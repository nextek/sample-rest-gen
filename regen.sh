#!/bin/bash


compilesvc() {
    set +x
    local DIR=$1
    cd $DIR
    mvn package > /dev/null
    if [ $? -eq 0 ]
    then
        echo "Successfully compiled $DIR"
    else
        echo "Failed to compile $DIR"
        exit 1
    fi

    local CNT=$(mvn checkstyle:checkstyle | grep 'Unable to locate Source' | wc -l)
    if [ "$CNT" -eq 1 ]
    then
        echo "Passed - zero checkstyle errors"
    else
        echo "FAILURE - $CNT checkstyle errors"
        exit 1
    fi
}

gensvc() {
    set +x
    local DIR=$1
    local YAML=$2
    local SVCNAME=$3

    cd $DIR
    java -jar target/sample-rest-gen-1.0.0.one-jar.jar --swagger=$YAML --service_name=$SVCNAME --database couchbase -o out > /dev/null

    if [ $? -eq 0 ]
    then
        echo "Successfully generated svc $SVCNAME"
    else
        echo "Failed to generate svc $SVCNAME"
        exit 1
    fi
}


set -x

ROOTDIR=$1



cd $ROOTDIR/out/

CURDIR=`pwd`

if [ "$CURDIR" = "$ROOTDIR/out" ]; then
    echo "YES curdir is right"
else
    echo "NO curdir is wrong"
fi

rm -rf *-svc

cd $ROOTDIR

mvn package

gensvc $ROOTDIR yaml/StayServiceCore.yaml stay
gensvc $ROOTDIR yaml/CDSServiceCore.yaml cds
gensvc $ROOTDIR yaml/PropertyConfigService.yaml prop
gensvc $ROOTDIR yaml/GuestProfileAPI.yaml guestprofile
gensvc $ROOTDIR yaml/GuestHHonorsAPI.yaml guesthonors
gensvc $ROOTDIR yaml/GuestRefrenceImpl.yaml guestref
gensvc $ROOTDIR yaml/GuestServiceCore.yaml guest
gensvc $ROOTDIR yaml/DateComplex.yaml datecomplex
gensvc $ROOTDIR yaml/PropertyMessagesService-v1.yaml propmsg
gensvc $ROOTDIR yaml/AdditionalPropComplex.yaml propcomplex
gensvc $ROOTDIR yaml/CredsServiceCore.yaml creds

cd $ROOTDIR/out/

compilesvc $ROOTDIR/out/cds-svc
compilesvc $ROOTDIR/out/guesthonors-svc
compilesvc $ROOTDIR/out/guestprofile-svc
compilesvc $ROOTDIR/out/guestref-svc
compilesvc $ROOTDIR/out/guest-svc
compilesvc $ROOTDIR/out/prop-svc
compilesvc $ROOTDIR/out/stay-svc
compilesvc $ROOTDIR/out/datecomplex-svc
compilesvc $ROOTDIR/out/propmsg-svc
compilesvc $ROOTDIR/out/propcomplex-svc
compilesvc $ROOTDIR/out/creds-svc
