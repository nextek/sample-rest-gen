#!/usr/bin/perl

use strict;
use warnings;
use Carp;
use Cwd;
use Getopt::Long;

my %C = ();
my %yaml2svc_name = (
  'StayServiceCore.yaml'            => 'stay-domainx',
  'CDSServiceCore.yaml'             => 'cds-domainx',
  'PropertyConfigService.yaml'      => 'prop-config',
  'GuestProfileAPI.yaml'            => 'guestprofile',
  'GuestHHonorsAPI.yaml'            => 'guesthonors',
  'GuestDomainxCore.yaml'           => 'guest-domainx',
  'DateComplex.yaml'                => 'datecomplex',
  'PropertyMessagesService-v1.yaml' => 'prop-msg',
  'AdditionalPropComplex.yaml'      => 'propcomplex',
  'CredsServiceCore.yaml'           => 'creds',
  'CompReferenceImpl.yaml'          => 'comp-ref',
  'HHonorsEnrollAPI.yaml'           => 'hhonors-enroll',
  'PropAPI.yaml'                    => 'prop-api',
  'AvailAPI.yaml'                   => 'avail',
);

my %non_couchbase_svcs = (
  'creds' => 1,
  'hhonors-enroll' => 1,
  'avail' => 1,
  'comp-ref' => 1,
);

my $USAGE =<< "END_USAGE";
regen.pl [options]
- compile the generator, run it on some yaml files, and check compile them
----------------------------------------------------------------
Options:
 === REQUIRED ===
 === OPTIONAL ===
  --svc_to_gen=<svc> ... will generatoe only <svc_to_gen>
  --nocheckstyle ....... ignore checkstyle errors
  --verbose ............ show more output
  --help ............... usage displayed
END_USAGE

main();

sub main {
  my $result = GetOptions("svc_to_gen=s"  => \$C{svc_to_gen},
                          "nocheckstyle"  => \$C{nocheckstyle},
                          "v|verbose"     => \$C{verbose},
                          "help"          => \$C{help},
                        );
  if($C{help}) {
    print $USAGE;
    exit 0;
  }

  my $cur_dir = getcwd;
  my $root_dir = $cur_dir;
  my $yaml_dir = "$root_dir/yaml";
  my $out_dir  = "$root_dir/out";
  if(! -d $yaml_dir) {
    croak "expected yaml directory in current directory but not found";
  }
  if(! -d $out_dir) {
    croak "expected out directory in current directory but not found";
  }
  $C{root_dir} = $root_dir;
  $C{yaml_dir} = $yaml_dir;
  $C{out_dir}  = $out_dir;

  my $cleanup_cmd = "rm -rf $C{out_dir}/*-svc";
  msg_print("> $cleanup_cmd");
  run_cmd($cleanup_cmd, no_print => 1);

  my $jar = compile_generator();
  generate_services($jar);
  compile_services();
}

sub compile_generator {
  chdir($C{root_dir});
  my ($res, $exit_val) = run_cmd("mvn clean package", no_print => 1);
  if($exit_val != 0) {
    croak "failed to compile generator";
  }

  my $target = glob("target/*one-jar.jar");
  return $target;
}

sub generate_services {
  my ($jar) = @_;

  foreach my $yaml (sort keys %yaml2svc_name) {
    my $svc_name = $yaml2svc_name{$yaml};
    next  if($C{svc_to_gen} && $svc_name !~ /$C{svc_to_gen}/);

    generate_svc($yaml, $jar, $svc_name);
  }
}

sub compile_services {
  foreach my $yaml (sort keys %yaml2svc_name) {
    my $svc_name = $yaml2svc_name{$yaml};
    next  if($C{svc_to_gen} && $svc_name !~ /$C{svc_to_gen}/);

    compile_svc($yaml, $svc_name);
  }
}

sub generate_svc {
  my ($yaml, $jar, $svc_name) = @_;

  my $yaml_path = "$C{yaml_dir}/$yaml";
  chdir($C{root_dir});

  my $base_cmd = "java -jar $jar --swagger=$yaml_path --service_name=$svc_name -o out --maskversions";

  if(! $non_couchbase_svcs{$svc_name}) {
    $base_cmd .= " --database couchbase";
  }

  my ($res, $exit_val) = run_cmd("$base_cmd >/dev/null 2>/dev/null", no_print => (!$C{verbose}));
  if($exit_val != 0) {
    run_cmd($base_cmd, no_print => 0);
    msg_print("failed to generate service $svc_name");
    msg_print($res);
    die "failed";
  }
  msg_print("Successfully generated service for $svc_name");
}

sub compile_svc {
  my ($yaml, $svc_name) = @_;

  my $svc_subdir = "${svc_name}-svc";
  my $svc_dir = "$C{out_dir}/$svc_subdir";
  if(! -d $svc_dir) {
    mkdir($svc_dir);
  }
  if(! -d $svc_dir) {
    confess "can't find svc dir ($svc_dir)";
  }

  chdir($svc_dir);

  my ($res, $exit_val) = run_cmd("mvn package", no_print => 1);
  if($exit_val != 0) {
    msg_print("failed to compile service $svc_name in subdir $svc_subdir");
    my ($res, $exit_val) = run_cmd("mvn clean package", no_print => 0);
    msg_print($res);
    die "failed";
  }

  if($res =~ /There are (\d+) errors reported by/) {
    my $num_errors = $1;
    my $num_ignored_issues = 0;
    my $error_txt = '';
    if($res =~ /Starting audit(.*?)Audit done/s) {
      $error_txt = $1;
      $num_ignored_issues = getNumIgnoredIssues($1);
    }

    my $num_considered_errors = $num_errors - $num_ignored_issues;

    my $do_fail = 0;
    if(!$C{nocheckstyle} && $num_errors > 0) {
      msg_print('='x72);
      if($error_txt) {
        msg_print($error_txt);
      }
      msg_print('='x72);
      $do_fail = 1;
    }

    msg_print("[$svc_name] chks errors: $num_errors, ignored: $num_ignored_issues, final: $num_considered_errors");
    if($do_fail) {
      die "no checkstyle errors allowed";
    }
  }
  msg_print("Successfully compiled service for $svc_name");
}

sub getNumIgnoredIssues {
  my ($issue_txt) = @_;
  my @issues = split(/\n/, $issue_txt);
  my $ignore_cnt = 0;
  foreach my $issue (@issues) {
    #if($issue =~ /NPath Complexity/) {
    #  $ignore_cnt++;
    #}
  }
  return $ignore_cnt;
}

sub run_cmd {
  my ($cmd, %options) = @_;
  if(! $options{no_print}) {
    msg_print("> $cmd");
  }
  my $res = `$cmd`;
  my $exit_val = $? >> 8;

  return ($res, $exit_val);
}

sub msg_print {
  my ($line) = @_;
  chomp($line);
  print $line."\n";
}

