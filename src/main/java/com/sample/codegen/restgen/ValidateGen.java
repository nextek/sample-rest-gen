package com.sample.codegen.restgen;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.sample.codegen.Util;
import com.sample.codegen.config.Configuration;
import com.sample.codegen.restgen.modelgen.ModelClassSet;
import com.sample.codegen.restgen.servicegen.ServiceClassSet;
import com.sample.codegen.restgen.validationgen.ValidateClass;

import io.swagger.models.HttpMethod;
import io.swagger.models.Operation;
import io.swagger.models.Path;
import io.swagger.models.Swagger;

public class ValidateGen  {
  private Swagger             swagger;
  private Configuration       config;
  private ModelGen            modelGen;
  private Map<String, ValidateClass> validateClassMap = new HashMap<>();
  private List<ValidateClass> validateClasses         = new ArrayList<>();

  public ValidateGen(final Swagger swagger, final Configuration config, final ModelGen modelGen) {
    this.swagger  = swagger;
    this.config   = config;
    this.modelGen = modelGen;
    this.prepare();
  }

  protected void prepare() {
    createValidationClasses();
  }

  public ModelGen getModelGen() {
    return modelGen;
  }

  protected void createValidationClasses() {
    final Map<String, Path> pathsMap = swagger.getPaths();

    final int maxClassCnt = 15;
    final Map<String, ValidateClass> classMap = new HashMap<>();
    final Map<String, Integer> classCount = new HashMap<>();

    for(Map.Entry<String, Path> entry : pathsMap.entrySet()) {
      // System.out.println("  Key = " + entry.getKey() + ", Value = " + entry.getValue());

      final String pathString = entry.getKey();
      final Path   path       = entry.getValue();

      final Map<HttpMethod, Operation> operationMap = path.getOperationMap();

      for(Map.Entry<HttpMethod, Operation> entryB : operationMap.entrySet()) {
        final HttpMethod method     = entryB.getKey();
        final String methodName     = method.name();
        // System.out.println("    Method = " + methodName.toUpperCase());

        final ModelClassSet modelClassSet = getModelGen().getModelClassSet(pathString, methodName);
        final int numRequestClasses = modelClassSet.getNumRequestClasses();

        final String[] ret = makeValidatePkgClass(entry.getKey());
        final String pkgName   = ret[0];
        String className = ret[1];
        String classKey = pkgName + "." + className;

        int count = (classCount.get(classKey) == null) ? 0 : classCount.get(classKey);
        count += numRequestClasses;
        classCount.put(classKey, count);

        int instance = 1 + (count / maxClassCnt);
        className += getInstanceModifier(instance);
        classKey = pkgName + "." + className;

        ValidateClass validateClass = classMap.get(classKey);

        if(validateClass == null) {
          validateClass = new ValidateClass(pkgName, className);
          validateClasses.add(validateClass);
          classMap.put(classKey, validateClass);
        }

        final String key = pathString + ";" + methodName;
        validateClassMap.put(key, validateClass);
      }
    }

  }

  public void generate(final String outdir, final String pkgRoot) {
    for(ValidateClass validateClass : validateClasses) {
      validateClass.generate(outdir, pkgRoot);
    }
  }

  protected String[] makeValidatePkgClass(final String pathString) {
    final List<String> pkgParts = new ArrayList<String>();
    pkgParts.add("com");
    pkgParts.add("sample");
    pkgParts.add("soa");
    pkgParts.add(config.getSvcPkgToken());
    pkgParts.add("validation");

    final String resource = Util.getLastResource(pathString);

    String className = Util.ucFirst(Util.getLastResource(resource)) + "Validator";

    final String pkg = String.join(".", pkgParts);

    final String[] ret = new String[2];
    ret[0] = pkg;
    ret[1] = className;
    return ret;
  }

  private String getInstanceModifier(final int instance) {
    if(instance > 1) {
      return Character.toString((char) ('A' + instance - 1));
    }

    return "";
  }


  public ValidateClass getValidateClass(final String pathString, final String methodName) {
    final String key = pathString + ";" + methodName;
    final ValidateClass ret = validateClassMap.get(key);
    return ret;
  }

}
