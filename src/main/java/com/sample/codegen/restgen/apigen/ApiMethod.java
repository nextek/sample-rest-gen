package com.sample.codegen.restgen.apigen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sample.codegen.SwaggerUtil;
import com.sample.codegen.Util;
import com.sample.codegen.config.Configuration;
import com.sample.codegen.lang.java.AccessModifier;
import com.sample.codegen.lang.java.LClass;
import com.sample.codegen.lang.java.Method;
import com.sample.codegen.lang.java.Param;
import com.sample.codegen.lang.java.SimpleType;
import com.sample.codegen.lang.java.Type;
import com.sample.codegen.restgen.ModelGen;
import com.sample.codegen.restgen.ParamCat;
import com.sample.codegen.restgen.apigen.TxnContextClass;
import com.sample.codegen.restgen.modelgen.ModelClass;
import com.sample.codegen.restgen.modelgen.ModelClassSet;
import com.sample.codegen.restgen.modelgen.ModelField;
import com.sample.codegen.restgen.modelgen.RequestClass;
import com.sample.codegen.restgen.servicegen.ServiceClassSet;
import com.sample.codegen.restgen.servicegen.ServiceMethod;
import com.sample.codegen.restgen.validationgen.ValidateClass;
import com.sample.codegen.restgen.validationgen.ValidateMethod;

import io.swagger.models.HttpMethod;
import io.swagger.models.Operation;
import io.swagger.models.Path;
import io.swagger.models.Response;
import io.swagger.models.Swagger;
import io.swagger.models.properties.Property;

public class ApiMethod extends Method {
  private Swagger         swagger;
  private String          pathString;
  private HttpMethod      httpMethod;
  private ServiceClassSet serviceClassSet;
  private ValidateClass   validateClass;
  private ModelClassSet   modelClassSet;
  private String          classPathId;
  private String          methodId;
  private TxnContextClass txnContextClass;

  private List<ApiMethod> apiMethods = new ArrayList<>();

  private boolean         hasArrayResponse       = false;
  private boolean         hasLocationHeaderReply = false;
  private String          correspondingGetPathForPost; 

  private String httpHeadersFieldName = "httpHeaders";

  public static String getMethodId(final String pathString, final String methodName) {
    final String lastResource = Util.getLastResource(pathString);
    final String lastPathPart = Util.getLastPathPart(pathString);
    String methodId = Util.ucFirst(methodName);
    if(! lastPathPart.equals(lastResource)) {
      methodId = methodId + Util.ucFirst(lastPathPart);
    }
    return methodId;
  }

  public static String createMethodName(final String pathString, final String methodName) {
    String methodId = getMethodId(pathString, methodName);
    String funcName = "perform" + methodId;
    return funcName;
  }

  public ApiMethod(final Method baseMethod, final Swagger swagger, final String pathString,
                   final HttpMethod httpMethod, final ServiceClassSet serviceClassSet,
                   final ValidateClass validateClass, final ModelClassSet modelClassSet,
                   final String classPathId, final TxnContextClass txnContextClass) {
    super(baseMethod);
    this.swagger         = swagger;
    this.pathString      = pathString;
    this.httpMethod      = httpMethod;
    this.serviceClassSet = serviceClassSet;
    this.validateClass   = validateClass;
    this.modelClassSet   = modelClassSet;
    this.classPathId     = classPathId;
    this.txnContextClass = txnContextClass;

    prepareHasArrayResponse();
  }

  public Swagger         getSwagger()         { return swagger;         }
  public String          getPathString()      { return pathString;      }
  public HttpMethod      getHttpMethod()      { return httpMethod;      }
  public ServiceClassSet getServiceClassSet() { return serviceClassSet; }
  public ValidateClass   getValidateClass()   { return validateClass;   }
  public ModelClassSet   getModelClassSet()   { return modelClassSet;   }

  public String          getHttpMethodName() {
    return httpMethod.name();
  }

  public String getServiceObjFieldName() {
    return Util.lcFirstOnly(this.serviceClassSet.getBaseClass().getClassName());
  }
  public Type getServiceObjFieldType() {
    return this.serviceClassSet.getBaseClass().getType();
  }
  public String getServiceObjPkgType() {
    return getServiceClassSet().getBaseClass().getPkgName();
  }
  public LClass getServiceClass() {
    return getServiceClassSet().getBaseClass();
  }
  
  public Path getPath() {
    return getSwagger().getPath(getPathString());
  }

  public Operation getOperation() {
    final Path path = getPath();
    final Map<HttpMethod, Operation> operationsMap = path.getOperationMap();
    final Operation operation = operationsMap.get(getHttpMethod());
    return operation;
  }

  public boolean getHasArrayResponse() {
    return hasArrayResponse;
  }

  public String getHysterixTimedName() {
    final int MAX_TIMED_NAME_LEN = 25;

    Operation oper = this.getOperation();
    String operId = oper.getOperationId();
    if(operId != null) {
      String retVal = null;
      if(operId.length() > MAX_TIMED_NAME_LEN) {
        retVal = operId.substring(0, MAX_TIMED_NAME_LEN);
      } else {
        retVal = operId;
      }
      return operId;
    }

    String methodId = getIntuitiveMethodId(false);
    int methodIdLen = methodId.length();
    String timedName = null;
    String proposedName = methodId + classPathId;
    //System.err.println("proposedName: " + proposedName + ", len: " + proposedName.length());
    boolean isOverLen = (proposedName.length() > MAX_TIMED_NAME_LEN);
    if(isOverLen) {
      methodId = getIntuitiveMethodId(true);
      proposedName = methodId + classPathId;
    }

    if(proposedName.length() > MAX_TIMED_NAME_LEN) {
      int overNChars = proposedName.length() - MAX_TIMED_NAME_LEN;
      //System.err.println("  overNChars: " + overNChars);
      //System.err.println("  classPathId: " + classPathId);
      timedName = methodId + this.classPathId.substring(0, classPathId.length() - overNChars);
    } else {
      timedName = proposedName;
    }
    return timedName;
  }

  public static String getIntuitiveVerbName(HttpMethod meth, boolean hasQueryParams) {
    String retStr = "";
    switch(meth) {
    case GET:
      if(hasQueryParams) {
        retStr = "search";
      } else {
        retStr = "read";
      }
      break;
    case POST:
      retStr = "create";
      break;
    case PUT:
      retStr = "update";
      break;
    case DELETE:
      retStr = "delete";
      break;
    case PATCH:
      retStr = "edit";
      break;
    default:
      retStr = "verb";
      break;
    }
    return retStr;
  }

  public String getIntuitiveMethodId(boolean shortened) {
    Operation oper = getOperation();
    boolean hasQueryParams = SwaggerUtil.OperationC.hasParamsOfType(oper, "QueryParameter");

    String verbName = getIntuitiveVerbName(getHttpMethod(), hasQueryParams);

    final String lastResource = Util.getLastResource(pathString);
    final String lastPathPart = Util.getLastPathPart(pathString);
    String methodId = Util.ucFirst(verbName);
    if(! lastPathPart.equals(lastResource)) {
      methodId = methodId + "By";
      if(shortened) {
        int shortLen = 3;
        if(lastPathPart.length() < shortLen) {
          shortLen = lastPathPart.length();
        }
        methodId += Util.ucFirst(lastPathPart).substring(0, shortLen);
      } else {
        methodId += Util.ucFirst(lastPathPart);
      }
    }
    return methodId;
  }

  protected void setupApiMethod(ModelClassSet svcModelClassSet, ValidateMethod valMethod,
                                ServiceMethod svcMethod, String basePath) {
    setReturnType(new SimpleType("Response", "javax.ws.rs.core.Response"));
    setJavaDocReturnDesc("a serialized Response object");
    addImportRef("javax.ws.rs.core.Response.ResponseBuilder");

    boolean hasBodyArg = false;
    String nonBodyApiParam = null;
    for(ParamCat paramCat : ParamCat.getValues()) {
      LClass requestLclass;
      Type type = null;
      Boolean validateClassObjOnInput = null;

      if(paramCat == ParamCat.OTHER) {
        RequestClass requestClass = getModelClassSet().getRequestClass();
        requestLclass = (LClass) requestClass;
        if(requestClass != null) {
          validateClassObjOnInput = requestClass.getValidateClassObjOnInput();
          type                    = requestClass.getType();
        }
      } else {
        ModelClass requestBodyClass = getModelClassSet().getBodyRequestClass();
        requestLclass = (LClass) requestBodyClass;
        type          = getModelClassSet().getBodyRequestType();
        if(requestBodyClass != null) {
          validateClassObjOnInput = requestBodyClass.getValidateClassObjOnInput();
        }
      }
      if(requestLclass == null) {
        continue;
      }

      final String className = requestLclass.getClassName();
      final String paramName = Util.lcFirstOnly(Util.abbreviateIdentifier(className));

      String paramCatDesc;
      final List<String> paramAnnotations = new ArrayList<String>();
      final List<String> paramImportRefs  = new ArrayList<String>();
      if(validateClassObjOnInput) {
        paramAnnotations.add("@Valid()");
        paramImportRefs.add("javax.validation.Valid");
      }

      if(paramCat == ParamCat.BODY) {
        paramCatDesc = "body";
        hasBodyArg   = true;
      } else {
        paramCatDesc        = "non-body";
        paramAnnotations.add("@BeanParam()");
        paramImportRefs.add("javax.ws.rs.BeanParam");
        nonBodyApiParam     = paramName;
      }

      final String jdocParamDesc
        = String.format("This is the %s %s parameter", paramName, paramCatDesc);

      final Param param = new Param(paramName, type);
      param.setIsFinal(true);
      param.setLabel(paramCat.name());
      param.setJavaDocParamDesc(jdocParamDesc);
      param.addAnnotations(paramAnnotations);
      param.addImportRefs(paramImportRefs);
      addParam(param);
    }

    final Param param2 = new Param(httpHeadersFieldName, "HttpHeaders",
                                   "javax.ws.rs.core.HttpHeaders");
    param2.setIsFinal(true);
    param2.setLabel(httpHeadersFieldName);
    param2.setJavaDocParamDesc("The request headers");
    param2.addAnnotation("@Context()");
    param2.addImportRef("javax.ws.rs.core.Context");
    addParam(param2);

    final String bodyTxt = buildResponseMethodBody(nonBodyApiParam, svcModelClassSet,
                                                   valMethod, svcMethod);
    setBody(bodyTxt);

    String remainingPath = getPathString().replace(basePath, "");
    
    final List<String> annotations = new ArrayList<String>();

    final String methodAnnot = getHttpMethodName().toUpperCase();
    annotations.add("@" + getHttpMethodName().toUpperCase() + "()");
    addImportRef("javax.ws.rs." + methodAnnot);

    annotations.add("@Path(value = \"" + remainingPath + "\")");
    addImportRef("javax.ws.rs.Path");

    List<String> producesList = SwaggerUtil.OperationC.getProduces(getOperation(), getSwagger());
    List<String> prioritizedProducesList = getPrioritizedMimeList(producesList);
    String producesStr = String.join(", ", prioritizedProducesList);

    List<String> consumesList = SwaggerUtil.OperationC.getConsumes(getOperation(), getSwagger());
    List<String> prioritizedConsumesList = getPrioritizedMimeList(consumesList);
    String consumesStr = String.join(", ", prioritizedConsumesList);

    annotations.add("@Produces(value = { " + producesStr + " })");
    addImportRef("javax.ws.rs.Produces");

    if(hasBodyArg) {
      annotations.add("@Consumes(value = { " + consumesStr + " })");
      addImportRef("javax.ws.rs.Consumes");
    }

    String timedAnnotValue = null;
    String hysterixTimedName = this.getHysterixTimedName();
    if(hysterixTimedName != null) {
      timedAnnotValue = String.format("name=\"%s\"", hysterixTimedName);
    }
    annotations.add("@Timed(" + timedAnnotValue + ")");
    addImportRef("com.codahale.metrics.annotation.Timed");
    addAnnotations(annotations);

    addJavaDocDescLine(String.format("The REST endpoint for %s on %s.",
                                     getHttpMethodName(), getPathString()));
  }

  private List<String> getPrioritizedMimeList(List<String> mimeTypes) {
    List<String> returnList = new ArrayList<>();

    for(String mimeType : mimeTypes) {
      if(mimeType.matches(".*json.*")) {
        if(mimeTypes.size() > 1) {
          returnList.add("\"" + mimeType + "; qs=5\"");
        } else {
          returnList.add("\"" + mimeType + "\"");
        }
      }
    }

    for(String mimeType : mimeTypes) {
      if(mimeType.matches(".*json.*")) {
        continue;
      }
      returnList.add("\"" + mimeType + "\"");
    }
    return returnList;
  }

  private String buildResponseMethodBody(final String nonBodyApiParam,
                                         final ModelClassSet svcModelClassSet,
                                         final ValidateMethod valMethod,
                                         final ServiceMethod  svcMethod) {
    List<String> bodyParts = new ArrayList<>();
    // function calls are indented
    final int bodyMaxLineLen = getBodyMaxLineLen();
    final String txnContextVar = "txnContext";

    addImportRef(this.txnContextClass.getFullName());
    bodyParts.add(String.format("final %s %s = new %s(%s);", this.txnContextClass.getClassName(),
                                txnContextVar, this.txnContextClass.getClassName(),
                                httpHeadersFieldName));

    String valCall = valMethod.getFunctionCall(this.validateClass.getClassName(),
                                               this, bodyMaxLineLen, txnContextVar) + Util.nl();
    bodyParts.add(valCall);
    addImportRef(this.validateClass.getFullName());

    String svcCall = svcMethod.getFunctionCall("this." + getServiceObjFieldName(),
                                               this, bodyMaxLineLen, txnContextVar) + Util.nl();
    bodyParts.add(svcCall);
    addImportRefs(svcMethod.getReturnType().getAllImportRefs());

    String svcRetVarName = svcMethod.getReturnVarName();

    List<String> builderElems = new ArrayList<>();

    boolean returns200 = SwaggerUtil.OperationC.hasResponseCode(getOperation(), 200);
    boolean returns201 = SwaggerUtil.OperationC.hasResponseCode(getOperation(), 201);
    boolean returns202 = SwaggerUtil.OperationC.hasResponseCode(getOperation(), 202);
    boolean returns204 = SwaggerUtil.OperationC.hasResponseCode(getOperation(), 204);

    Response resp_for_200 = SwaggerUtil.OperationC.getResponse(getOperation(), 200);
    String schema_for_200 = null;
    if(resp_for_200 != null) {
      schema_for_200 = SwaggerUtil.ResponseC.getResponseSchema(resp_for_200);
    }

    String statusCall = null;

    if(returns200) {
      statusCall = "ok()";
      if(svcRetVarName != null && schema_for_200 != null) {
        builderElems.add("entity(" + svcRetVarName + ")");
      }
    } else if(returns201) {
      String locationVar = null;
      if(this.hasLocationHeaderReply) {
        if(this.correspondingGetPathForPost == null) {
          bodyParts.add("// No corresponding get path to return on location header was found"
                        + Util.nl() + "// for specifically created resource.");
        }
        locationVar = "locationUri";
        String locAssignCode = getLocationJavaCode(bodyMaxLineLen, locationVar, nonBodyApiParam,
                                                   svcMethod.getReturnVarName(),
                                                   getModelClassSet().getRequestClass(),
                                                   svcModelClassSet);
        bodyParts.add(locAssignCode);
      }

      if(locationVar == null) {
        // they either didn't specify a return location header or there is no corresponding
        // get to provide a path to the resource, so return a 201 status with no location
        // header
        statusCall = "status(Response.Status.CREATED)";
      } else {
        statusCall = "created(" + locationVar + ")";
      }
    } else if(returns202) {
      statusCall = "accepted()";
    } else if(returns204) {
      statusCall = "noContent()";
    }

    String builderVar = "builder";
    bodyParts.add(String.format("ResponseBuilder %s = Response.%s;",
                                builderVar, statusCall));
    bodyParts.add(String.format("%s = %s.updateResponse(%s);",
                                builderVar, txnContextVar, builderVar));

    if(builderElems.size() > 0) {
      bodyParts.add(String.format("%s = %s.%s;", builderVar, builderVar,
                                  String.join(".", builderElems)));
    }
    bodyParts.add(String.format("return %s.build();", builderVar));

    String code = String.join(Util.nl(), bodyParts);
    return code;
  }

  protected String getLocationJavaCode(int bodyMaxLineLen, String locationVar,
                                       String nonBodyApiParam,
                                       String svcRetVar,
                                       RequestClass nonBodyRequestClass,
                                       ModelClassSet svcModelClassSet) {
    String assignPrefix = String.format("final URI %s = URI.create(", locationVar);
    addImportRef("java.net.URI");
    String locIdUri;
    
    List<String> javaParts   = new ArrayList<>();
    if(this.correspondingGetPathForPost == null) {
      // Lee suggested that if we can't find a GET to use just give them the current post path
      locIdUri = getPathString();
    } else {
      locIdUri = this.correspondingGetPathForPost;
    }
    
    List<String> pathParts = new ArrayList<>();
    pathParts.addAll(Arrays.asList(locIdUri.split("/")));
    if(pathParts.size() > 0 && pathParts.get(0).length() == 0) {
      pathParts.remove(0);
    }

    int numPathParts = pathParts.size();

    List<String> curUrlParts = new ArrayList<>();
    for(int i = 0; i < numPathParts; i++) {
      String pathPart = pathParts.get(i);
      boolean isLast  = (i == (numPathParts - 1));
      //System.err.println("pathPart = " + pathPart);
      //System.err.println("isLast: " + isLast + ", numPathParts: " + numPathParts + ", i: " + i);
        
      if(! pathPart.matches(".*[{].*[}].*")) {
        curUrlParts.add(pathPart);
        //System.err.println("matches path arg. adding and continuing");
        continue;
      }

      String urlStrPart = "/" + String.join("/", curUrlParts) + "/";
      javaParts.add("\"" + urlStrPart + "\"");
      curUrlParts.clear();
        
      // a path param {}
      final String pathPartA = pathPart.replaceAll("[{}]", "");
      final String fieldName = pathPartA;
      if(isLast) {
        ModelClass respClass = svcModelClassSet.getResponseClass();
        ModelField keyFieldModelField = respClass.getModelField(fieldName);
        if(keyFieldModelField == null) {
          throw new RuntimeException("Couldn't find field " + fieldName + " in model "
                                     + respClass.getSchemaName() + ". Can't retrieve this field"
                                     + " to construct location header.");
        }
        String getterMethodName
          = respClass.getModelField(fieldName).getPrimaryGetterMethodName();
        String getCall = String.format("%s.%s()", svcRetVar, getterMethodName);
        javaParts.add(getCall);
      } else {
        //System.err.println("nonBodyRequestClass: " + nonBodyRequestClass);
        //System.err.println("fieldName: " + fieldName);
        //System.err.println("modelField: " + nonBodyRequestClass.getModelField(fieldName));
        ModelField keyFieldModelField = nonBodyRequestClass.getModelField(fieldName);
        if(keyFieldModelField == null) {
          throw new RuntimeException("Couldn't find field " + fieldName + " in model "
                                     + nonBodyRequestClass.getSchemaName()
                                     + ". Can't retrieve this field"
                                     + " to construct location header.");
        }

        String getterMethodName
          = nonBodyRequestClass.getModelField(fieldName).getPrimaryGetterMethodName();
        String getCall = String.format("%s.%s()", nonBodyApiParam, getterMethodName);
        javaParts.add(getCall);
      }
    }

    if(curUrlParts.size() > 0) {
      String urlStrPartB = "/" + String.join("/", curUrlParts);
      javaParts.add("\"" + urlStrPartB + "\"");
    }

    //System.err.println("bodyMaxLineLen: " + bodyMaxLineLen);
    final String successiveIndent = Util.getRepeatedStr(assignPrefix.length(), " ");
    String locJavaCode
      = Util.wrapJoinTokens(assignPrefix, bodyMaxLineLen, successiveIndent, " + ", ");",
                            Util.JoinTokenStyle.PREPEND, javaParts, false, true, true);
    return locJavaCode;
  }
  
  protected ModelClassSet getLocationResponse(ModelGen modelGen) {
    ModelClassSet svcModelClassSet = getModelClassSet();  // by default, use the model class

    final Response response201 = SwaggerUtil.OperationC.getResponse(getOperation(), 201);
    if(response201 != null) {
      final Map<String, Property> headers = response201.getHeaders();
      if(headers != null && headers.get("Location") != null) {
        this.hasLocationHeaderReply = true;
      }
    }

    final String httpMethodName = getHttpMethodName();
    if(! this.hasLocationHeaderReply || getHttpMethod() != HttpMethod.POST) {
      return svcModelClassSet;
    }

    final String getPathString
      = SwaggerUtil.SwaggerC.getCorrespondingGetPathForPost(getSwagger(), getPathString());

    if(getPathString == null) {
      System.err.println("Warning: we couldn't find a corresponding get in the swagger for "
                         + "the " + httpMethodName + " at " + getPathString()
                         + ". Consider updating "
                         + "the swagger to add a corresponding get for this endpoint.");
    } else {
      this.correspondingGetPathForPost = getPathString;

      final ModelClass getResponseClass
        = modelGen.getResponseClass(this.correspondingGetPathForPost, HttpMethod.GET.name());
      final ModelClass postResponseClass = getModelClassSet().getResponseClass();

      final String getClassName = getResponseClass.getClassName();

      if(postResponseClass != null) {
        final String postClassName = postResponseClass.getClassName();
      
        if(! getClassName.equals(postClassName)) {
          // highly unlikely we would hit this condition
          throw new RuntimeException("from location header we want service to return class "
                                   + getClassName + " but from response schema we want to return "
                                   + "class " + postClassName + ". We can't decide which to use");
        }
      }

      svcModelClassSet = svcModelClassSet.shallowClone();
      svcModelClassSet.setResponseClass(getResponseClass);
    }
    return svcModelClassSet;
  }

  protected void prepareHasArrayResponse() {
    final Operation operation = getOperation();
    if(SwaggerUtil.OperationC.doesHaveArrayResponse(operation)) {
      hasArrayResponse = true;
    }
  }


  
}
