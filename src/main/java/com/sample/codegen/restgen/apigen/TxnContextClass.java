package com.sample.codegen.restgen.apigen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sample.codegen.SwaggerUtil;
import com.sample.codegen.Util;
import com.sample.codegen.config.Configuration;
import com.sample.codegen.lang.java.AccessModifier;
import com.sample.codegen.lang.java.Field;
import com.sample.codegen.lang.java.LClass;
import com.sample.codegen.lang.java.Method;
import com.sample.codegen.lang.java.Param;

public class TxnContextClass extends LClass {  
  public TxnContextClass() {
    super(Configuration.getConfig().getSvcClassName() + "TxnContext");
    String pkgName = "com.sample.soa." + Configuration.getConfig().getSvcPkgToken()
                     + ".api.context";
    this.setPkgName(pkgName);

    prepare();
  }

  public void prepare() {
    prepareClass();
    prepareConstructor();
  }

  private void prepareClass() {
    this.addJavaDocDescLine("This class is a scratch area for service specific "
                            + "transaction context data.");
    this.addJavaDocDescLine("Feel free to add fields and methods as needed.");
    this.setJavaDocAuthor(Configuration.getConfig().getJavaDocAuthor());
    this.setJavaDocVersion(Configuration.getConfig().getJavaDocVersion());

    LClass txnContextClass = new LClass("TxnContext");
    txnContextClass.setPkgName("com.sample.soa.context");
    this.setParentClass(txnContextClass);
  }

  protected void prepareConstructor() {
    final Method nonDefaultConstr = createMethod(getClassName(), AccessModifier.PUBLIC);
    nonDefaultConstr.setReturnType(null);
    nonDefaultConstr.addJavaDocDescLine("Constructor.");
    nonDefaultConstr.setBody("super(requestHeaders);");

    final Param param = new Param("requestHeaders", "HttpHeaders", "javax.ws.rs.core.HttpHeaders");
    param.setIsFinal(true);
    param.setJavaDocParamDesc("The request headers.");
    nonDefaultConstr.addParam(param);

    addMethod(nonDefaultConstr);
  }

  public void generate(final String outdir, final String pkgRoot) {
    //System.err.println("generate a ApiClass");
    final LClass.GenerationOptions options = new LClass.GenerationOptions();
    options.ensureBlankLineBeforeFldAnnotations = true;
    options.verticallyLineUpFields              = false;
    final String classTxt = getClassTxt(options);
    final String outPath  = getOutPath(outdir, pkgRoot);
    Util.writeGeneratedFile(outPath, classTxt);
  }

}

