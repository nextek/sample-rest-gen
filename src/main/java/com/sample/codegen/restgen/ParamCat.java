package com.sample.codegen.restgen;

import java.util.Arrays;
import java.util.List;
import java.util.Collections;

public enum ParamCat {
  OTHER, BODY;

  public static List<ParamCat> getValues() {
    final List<ParamCat> retList = Arrays.asList(ParamCat.values());
    Collections.sort(retList);
    return retList;
  }

};


