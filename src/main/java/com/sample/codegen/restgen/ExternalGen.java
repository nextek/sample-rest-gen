package com.sample.codegen.restgen;

import java.util.ArrayList;
import java.util.List;

import com.sample.codegen.Util;
import com.sample.codegen.config.Configuration;
import com.sample.codegen.config.Configuration.HysterixInfo;
import com.sample.codegen.restgen.externalgen.ExternalClass;

public class ExternalGen  {
  private List<ExternalClass> externalClasses = new ArrayList<>();
  
  public ExternalGen() {
    if(Configuration.getConfig().isComposite()) {
      this.prepare();
    }
  }

  protected void prepare() {
    createExternalClasses();
  }

  protected void createExternalClasses() {
    List<HysterixInfo> l = Configuration.getConfig().getHysterixDependencies();
    for(HysterixInfo hysterixDep : l) {
      final String[] ret   = makeExternalPkgClass(hysterixDep);
      final String pkgName = ret[0];
      String className = ret[1];
      String classKey = pkgName + "." + className;
      ExternalClass externalClass = new ExternalClass(pkgName, className, hysterixDep);
      externalClasses.add(externalClass);
    }
  }

  public void generate(final String outdir, final String pkgRoot) {
    if(! Configuration.getConfig().isComposite()) {
      return;
    }

    for(ExternalClass externalClass : this.externalClasses) {
      final List<String> description = new ArrayList<String>();
      description.add("This package contains classes for external endpoints for "
                      + "composite services.");
      externalClass.generate(outdir, pkgRoot);
      PackageInfoGen.getInstance().add(externalClass.getPkgName(), pkgRoot, description);
    }

    List<HysterixInfo> l = Configuration.getConfig().getHysterixDependencies();
    for(HysterixInfo hysterixDep : l) {
      final List<String> description = new ArrayList<String>();
      description.add("This package contains model classes for external endpoint "
                      + hysterixDep.getResource() + ".");
      PackageInfoGen.getInstance().add(pkgRoot + ".external." + hysterixDep.getResource()
                                       + ".model", pkgRoot, description);
    }
  }

  protected String[] makeExternalPkgClass(final HysterixInfo hysterixDep) {
    final List<String> pkgParts = new ArrayList<String>();
    pkgParts.add("com");
    pkgParts.add("sample");
    pkgParts.add("soa");
    pkgParts.add(Configuration.getConfig().getSvcPkgToken());
    pkgParts.add("external");
    pkgParts.add(hysterixDep.getResource());

    String className = hysterixDep.getCommandName();

    final String pkg = String.join(".", pkgParts);

    final String[] ret = new String[2];
    ret[0] = pkg;
    ret[1] = className;
    return ret;
  }

}
