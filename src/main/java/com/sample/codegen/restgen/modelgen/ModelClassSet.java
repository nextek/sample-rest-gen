package com.sample.codegen.restgen.modelgen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sample.codegen.lang.java.LClass;
import com.sample.codegen.lang.java.Type;
import com.sample.codegen.restgen.ParamCat;

public class ModelClassSet {
  private RequestClass requestClass;
  private ModelClass   bodyRequestClass;
  private ModelClass   responseClass;
  private Type         bodyRequestType;
  
  public ModelClassSet() {
  }

  public ModelClassSet shallowClone() {
    ModelClassSet n = new ModelClassSet();
    n.requestClass     = this.getRequestClass();
    n.bodyRequestClass = this.getBodyRequestClass();
    n.responseClass    = this.getResponseClass();
    n.bodyRequestType  = this.getBodyRequestType();
    return n;
  }

  public void setRequestClass(final RequestClass requestClass) {
    this.requestClass = requestClass;
  }

  public void setBodyRequestClass(final ModelClass bodyRequestClass) {
    this.bodyRequestClass = bodyRequestClass;
  }

  public void setResponseClass(final ModelClass responseClass) {
    this.responseClass = responseClass;
  }

  public void setBodyRequestType(final Type bodyType) {
    this.bodyRequestType = bodyType;
  }
  
  public RequestClass getRequestClass() {
    return requestClass;
  }
  public ModelClass   getBodyRequestClass() {
    return bodyRequestClass;
  }
  public ModelClass   getResponseClass() {
    return responseClass;
  }
  public Type getBodyRequestType() {
    return bodyRequestType;
  }

  public int getNumRequestClasses() {
    int cnt = 0;
    if(requestClass != null) {
      cnt++;
    }
    if(bodyRequestClass != null) {
      cnt++;
    }
    return cnt;
  }

  public List<LClass> getRequestClassList() {
    final List<LClass> retList = new ArrayList<>();
    if(this.requestClass != null) {
      retList.add(requestClass);
    }
    if(this.bodyRequestClass != null) {
      retList.add(bodyRequestClass);
    }
    return retList;
  }
}

