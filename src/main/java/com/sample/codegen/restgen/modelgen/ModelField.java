package com.sample.codegen.restgen.modelgen;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.sample.codegen.SwaggerUtil;
import com.sample.codegen.Util;
import com.sample.codegen.lang.java.AccessModifier;
import com.sample.codegen.lang.java.CompositeType;
import com.sample.codegen.lang.java.Field;
import com.sample.codegen.lang.java.LClass;
import com.sample.codegen.lang.java.Method;
import com.sample.codegen.lang.java.Param;
import com.sample.codegen.lang.java.SimpleType;
import com.sample.codegen.lang.java.Type;
import com.sample.codegen.restgen.EnumGen;
import com.sample.codegen.restgen.ParamCat;
import com.sample.codegen.restgen.modelgen.ModelClass.ModelClassStyle;

import io.swagger.models.Swagger;
import io.swagger.models.parameters.BodyParameter;

// represents the generated meta-data for a field
public class ModelField {
  private LClass          thisClass;
  private String          fieldName;
  private String          paramType;
  private String          swaggerType;
  private String          swaggerFormat;
  private String          swaggerDesc;
  private boolean         isRequired;
  private boolean         isReadOnly;
  private LClass          javaClass;
  private String          itemsSwaggerType;
  private String          itemsSwaggerFormat;
  private LClass          itemsJavaClass;
  private ModelClassStyle modelClassStyle;
  private String          serviceName;
  private String          pattern;
  private Integer         minLength;
  private Integer         maxLength;
  private Integer         minItems;
  private Integer         maxItems;
  private String          customMsg;
  
  private Type            nonContainerType;
  private boolean         isClassType = false;
  private boolean         isContainer = false;
  private Type            declaredType;
  private String          fieldInitializer;
  private int             orderPriorityGroup;
  private String          fieldNote;
  private List<String>    customValidations;
  
  public ModelField(final String fieldName, final LClass thisClass, final String paramType,
                    final String swaggerType, final String swaggerFormat, final String swaggerDesc,
                    final boolean isRequired, final Boolean isReadOnly, final LClass javaClass,
                    final String itemsSwaggerType, final String itemsSwaggerFormat,
                    final LClass itemsJavaClass, final ModelClassStyle modelClassStyle,
                    final String serviceName, final String pattern,
                    final Integer minLen, final Integer maxLen, final String fieldNote,
                    final Integer minItems,final Integer maxItems, final String customMsg,
                    final List<String> customValidations) {
    this.fieldName          = Util.lcFirstOnly(fieldName);
    this.thisClass          = thisClass;
    this.paramType          = paramType;
    this.swaggerType        = swaggerType;
    this.swaggerFormat      = swaggerFormat;
    this.swaggerDesc        = swaggerDesc;
    this.isRequired         = isRequired;
    if(isReadOnly == null)  {
      this.isReadOnly       = false;;
    } else {
      this.isReadOnly       = isReadOnly;
    }
    this.javaClass          = javaClass;
    this.itemsSwaggerType   = itemsSwaggerType;
    this.itemsSwaggerFormat = itemsSwaggerFormat;
    this.itemsJavaClass     = itemsJavaClass;
    this.modelClassStyle    = modelClassStyle;
    this.serviceName        = serviceName;
    this.pattern            = pattern;
    this.minLength          = minLen;
    this.maxLength          = maxLen;
    this.fieldNote          = fieldNote;
    this.minItems           = minItems;
    this.maxItems           = maxItems;
    this.customMsg          = customMsg;
    this.customValidations  = customValidations;
  }

  public String  getFieldName() {
    return fieldName;
  }
  public LClass  getThisClass() {
    return thisClass;
  }
  public String  getParamType() {
    return paramType;
  }
  public Type    getDeclaredType() {
    return declaredType;
  }
  public boolean getIsClassType() {
    return isClassType;
  }
  public String  getSwaggerType() {
    return swaggerType;
  }
  public String  getSwaggerFormat() {
    return swaggerFormat;
  }
  public boolean getIsRequired() {
    return isRequired;
  }
  public boolean getIsReadOnly() {
    return isReadOnly;
  }
  public LClass  getJavaClass() {
    return javaClass;
  }
  public String  getItemsSwaggerType() {
    return itemsSwaggerType;
  }
  public String  getItemsSwaggerFormat() {
    return itemsSwaggerType;
  }
  public LClass  getItemsJavaClass() {
    return itemsJavaClass;
  }
  public boolean isRequestStyle() {
    return modelClassStyle == ModelClassStyle.REQUEST;
  }
  public boolean isResponseStyle() {
    return modelClassStyle == ModelClassStyle.RESPONSE;
  }
  public String getServiceName() {
    return serviceName;
  }
  public Type    getNonContainerType() {
    return nonContainerType;
  }
  public boolean getIsContainer() {
    return isContainer;
  }
  public String  getPattern() {
    return pattern;
  }

  public Type getDesiredContainerType() {
    if(getParamType().equals("query")) {
      return new SimpleType("Set", "java.util.Set");
    } else {
      return new SimpleType("List", "java.util.List");
    }
  }

  public void prepareDefault() {
    //System.err.println("fieldName: " + fieldName + ", swaggerType: " + swaggerType
    //                   + ", swaggerFormat: " + swaggerFormat
    //                   + ", itemsSwaggerType: " + itemsSwaggerType + ", itemsSwaggerFormat: "
    //                   + itemsSwaggerFormat);
    orderPriorityGroup    = 50;  // default priority

    if("array".equals(swaggerType)) {
      if(itemsJavaClass != null) {
        this.nonContainerType = getItemsJavaClass().getType();
        this.isClassType      = true;
        orderPriorityGroup    = 90;
      } else {
        this.nonContainerType = Util.getJavaType(getItemsSwaggerType(), getItemsSwaggerFormat());
        orderPriorityGroup    = 80;
      }
      this.declaredType = new CompositeType(getDesiredContainerType(), getNonContainerType());
      this.isContainer  = true;
    } else {
      if(javaClass != null) {
        this.nonContainerType = getJavaClass().getType();
        this.isClassType      = true;
        orderPriorityGroup    = 70;
      } else {
        //System.err.println("swaggerType: " + getSwaggerType());
        //System.err.println(", swaggerFormat: " + getSwaggerFormat());
        this.nonContainerType = Util.getJavaType(getSwaggerType(), getSwaggerFormat());
      }
      this.declaredType     = getNonContainerType();
    }
  }

  public void prepareWithDefinedType(Type type, Type nonContainerType, boolean isArray) {
    orderPriorityGroup    = 50;  // default priority
    this.declaredType     = type;
    this.nonContainerType = nonContainerType;
    
    if(isArray) {
      this.isClassType      = true;
      orderPriorityGroup    = 90;
    } else {
      this.isClassType      = true;
      orderPriorityGroup    = 70;
    }
  }
  
  public boolean isDateFormat(String swaggerFormatStr) {
    if(swaggerFormatStr != null &&
       (swaggerFormatStr.equals("date") || swaggerFormatStr.equals("date-time") ||
        swaggerFormatStr.equals("time"))) {
      return true;
    } else {
      return false;
    }
  }

  public Field getField(LClass parentClass) {
    final Field field = thisClass.createField(getFieldName(), AccessModifier.PRIVATE);
    field.setType(declaredType);
    if(getJavaClass() != null) {
      field.addImportRef(getJavaClass().getFullName());
    }
    if(getItemsJavaClass() != null) {
      field.addImportRef(getItemsJavaClass().getFullName());
    }
    field.setInitializerCode(this.fieldInitializer);
    field.setOrderPriorityGroup(orderPriorityGroup);

    if(this.fieldNote != null) {
      field.setJavaDoc(this.fieldNote);
    }
    
    final String fieldName = getFieldName();
    final String paramType = getParamType();
    boolean hasPattern =  (this.pattern != null && isRequestStyle());

    // NON-VALIDATION ANNOTATIONS
    String annotateName = null;
    if(paramType != null) {
      if("path".equals(paramType)) {
        field.addAnnotation(String.format("@PathParam(value = \"%s\")", fieldName));
        field.addImportRef("javax.ws.rs.PathParam");
      } else if("query".equals(paramType)) {
        field.addAnnotation(String.format("@QueryParam(value = \"%s\")", fieldName));
        field.addImportRef("javax.ws.rs.QueryParam");
      } else if("body".equals(paramType)) {
        if(getIsReadOnly()) {
          String annotExtra = "";
          if(isRequestStyle()) {
            annotExtra = ", access = JsonProperty.Access.READ_ONLY";
          }
          field.addAnnotation(
                    String.format("@JsonProperty(value = \"%s\"%s)", fieldName, annotExtra));
          field.addImportRef("com.fasterxml.jackson.annotation.JsonProperty");
        } else {
          field.addAnnotation(String.format("@JsonProperty(value = \"%s\")", fieldName));
          field.addImportRef("com.fasterxml.jackson.annotation.JsonProperty");
        }
      } else {
        throw new RuntimeException("annotation for paramType (" + paramType + ") not coded");
      }
    }

    boolean isString  = getNonContainerType().isString();
    Integer useMin = null;
    Integer useMax = null;

    if(isString && ! this.isContainer) {
      if(this.minLength != null) {
        useMin = this.minLength;
      }
      if(this.maxLength != null) {
        useMax = this.maxLength;
      }
    }

    if(this.isContainer) {
      if(this.minItems != null) {
        useMin = this.minItems;
      }
      if(this.maxItems != null) {
        useMax = this.maxItems;
      }
    }

    boolean hasEmptyCheckAnnot = false;  // exists an annotation with an empty check
    List<String> sizeTokens = new ArrayList<>();
    if(useMin != null) {
      String minStr = Integer.toString(useMin);
      if(useMin > 2) {
        String constName = Util.constantizeVar(fieldName) + "_MIN_SIZE";
        parentClass.addConstIntField(constName, useMin);
        minStr = constName;
      }
      sizeTokens.add("min=" + minStr);
      hasEmptyCheckAnnot = true;
    }
    if(useMax != null) {
      String maxStr = Integer.toString(useMax);
      if(useMax > 2) {
        String constName = Util.constantizeVar(fieldName) + "_MAX_SIZE";
        parentClass.addConstIntField(constName, useMax);
        maxStr = constName;
      }
      sizeTokens.add("max=" + maxStr);
    }
    if(sizeTokens.size() > 0) {
      String lenValStr = String.join(", ", sizeTokens);
      field.addAnnotation("@Size(" + lenValStr + ")");
      field.addImportRef("javax.validation.constraints.Size");
    }

    if(hasPattern) {
      String quotedPattern = this.pattern.replace("\\", "\\\\");

      List<String> patternTokens = new ArrayList<>();
      patternTokens.add(String.format("regexp = \"%s\"", quotedPattern));

      String patternWrapped
        = Util.wrapJoinTokens("@Pattern(", field.getMaxLineLen(), "         ", ", ", ")",
                              Util.JoinTokenStyle.APPEND, patternTokens, false, true, true);
      field.addAnnotation(patternWrapped);
      field.addImportRef("javax.validation.constraints.Pattern");
      hasEmptyCheckAnnot = true;
    }

    if(isRequestStyle() && getIsClassType() && ! getIsReadOnly()) {
      field.addAnnotation("@Valid()");
      field.addImportRef("javax.validation.Valid");
    }

    boolean hasDateAnnotation = false;
    if(isRequestStyle() && ! getIsReadOnly()) {
      if(this.swaggerFormat != null && this.swaggerFormat.equals("date")) {
        field.addAnnotation("@AssertDate()");
        field.addImportRef("com.sample.soa.constraints.AssertDate");
        hasEmptyCheckAnnot = true;
      } else if(this.swaggerFormat != null && this.swaggerFormat.equals("date-time")) {
        field.addAnnotation("@AssertDateTime()");
        field.addImportRef("com.sample.soa.constraints.AssertDateTime");
        hasEmptyCheckAnnot = true;
      } else if(this.swaggerFormat != null && this.swaggerFormat.equals("time")) {
        field.addAnnotation("@AssertTime()");
        field.addImportRef("com.sample.soa.constraints.AssertTime");
        hasEmptyCheckAnnot = true;
      } else if(this.itemsSwaggerFormat != null && this.itemsSwaggerFormat.equals("date")) {
        field.addImportRef("com.sample.soa.constraints.AssertDate");
        field.addAnnotation("@AssertDate()");
        field.addImportRef("com.sample.soa.constraints.AssertDate");
        hasEmptyCheckAnnot = true;
      } else if(this.itemsSwaggerFormat != null && this.itemsSwaggerFormat.equals("date-time")) {
        field.addAnnotation("@AssertDateTime()");
        field.addImportRef("com.sample.soa.constraints.AssertDateTime");
        hasEmptyCheckAnnot = true;
      } else if(this.itemsSwaggerFormat != null && this.itemsSwaggerFormat.equals("time")) {
        field.addAnnotation("@AssertTime()");
        field.addImportRef("com.sample.soa.constraints.AssertTime");
        hasEmptyCheckAnnot = true;
      }

      for(String customValidator : this.customValidations) {
        String annotation = Util.lookupCustomValidatorAnnotation(customValidator);
        if(annotation == null) {
          throw new RuntimeException("Warnings: unhandled custom validation passed: "
                                     + customValidator);
        }

        field.addAnnotation(String.format("@%s()", annotation));
        field.addImportRef(Util.lookupCustomValidatorImportRef(customValidator));
        hasEmptyCheckAnnot = true;
      }

      // VALIDATION ANNOTATIONS
      if(getIsRequired() && isRequestStyle()) {
        if(isString) {
          if(hasEmptyCheckAnnot) {
            // one of the other annotations has an empty check so we can just perform a
            // NotNull annotation
            field.addAnnotation("@NotNull()");
            field.addImportRef("javax.validation.constraints.NotNull");
          } else {
            field.addAnnotation("@NotEmpty()");
            // we need to use our own annotation for lists and sets of strings as the hibernate
            // NotEmpty annotation doesn't verify that values in the list have positive length
            field.addImportRef("com.sample.soa.constraints.NotEmpty");
          }
        } else {
          field.addAnnotation("@NotNull()");
          field.addImportRef("javax.validation.constraints.NotNull");
        }
      }

    }

    return field;
  }

  public String getPrimaryGetterMethodName() {
    String getterFuncName;
    if(getIsContainer()) {
      final String pluralField = Util.pluralizeNoun(fieldName);
      getterFuncName = "get" + Util.ucFirstOnly(pluralField);
    } else {
      getterFuncName = "get" + Util.ucFirstOnly(fieldName);
    }
    return getterFuncName;
  }

  public Method getPrimaryGetterMethod() {
    return getNonDateGetterMethod();
  }
  
  public Method getNonDateGetterMethod() {
    final String fieldName = getFieldName();
    final String paramType = getParamType();

    String getterFuncName;
    if(getIsContainer()) {
      final String pluralField = Util.pluralizeNoun(fieldName);
      getterFuncName = "get" + Util.ucFirstOnly(pluralField);
    } else {
      getterFuncName = "get" + Util.ucFirstOnly(fieldName);
    }

    final Method getter = getThisClass().createMethod(getPrimaryGetterMethodName(),
                                                      AccessModifier.PUBLIC);

    final String simpleBody = "return " + fieldName + ";";
    String getterBody = simpleBody;  // the default case
    
    if(paramType != null && ! "body".equals(paramType)) {
      final String queryBody     =   "return " + fieldName + ";";
      final String queryListBody =   "return " + fieldName + ";";
      
      if(getNonContainerType().isString()) {
        if(getIsContainer()) {
          getterBody = queryListBody;
        } else {
          getterBody = queryBody;
        }
      }
    }

    getter.setBody(getterBody);
    getter.addJavaDocDescLine("Returns the " + fieldName + " field.");
    getter.setReturnType(getDeclaredType());
    getter.setJavaDocReturnDesc("The " + fieldName + " field.");
    return getter;
  }

  public Type getJava8DateType() {
    Type java8type = null;
    if((this.swaggerFormat != null && this.swaggerFormat.equals("date")) ||
       (this.itemsSwaggerFormat != null && this.itemsSwaggerFormat.equals("date"))) {
      java8type = new SimpleType("LocalDate", null);
    } else if((this.swaggerFormat != null && this.swaggerFormat.equals("date-time")) ||
              (this.itemsSwaggerFormat != null && this.itemsSwaggerFormat.equals("date-time"))) {
      java8type = new SimpleType("ZonedDateTime", null);
    } else if((this.swaggerFormat != null && this.swaggerFormat.equals("time")) ||
              (this.itemsSwaggerFormat != null && this.itemsSwaggerFormat.equals("time"))) {
      java8type = new SimpleType("LocalTime", null);
    }
    return java8type;
  }

  public String getZonedDateTimeGetter(String java8DateType) {
    String ret = null;
    if(java8DateType.equals("LocalDate")) {
      ret = "toLocalDate";
    } else if(java8DateType.equals("ZonedDateTime")) {
      ret = "toLocalDateTime";
    } else if(java8DateType.equals("LocalTime")) {
      ret = "toLocalTime";
    }
    return ret;
  }

  public String getDateTimeFormatter(Type java8DateType) {
    String ret = null;
    String typeName = java8DateType.getTypeName();
    if(typeName.equals("LocalDate")) {
      ret = "DateTimeFormatter.ISO_LOCAL_DATE";
    } else if(typeName.equals("ZonedDateTime")) {
      ret = "DateTimeFormatter.ISO_OFFSET_DATE_TIME";
    } else if(typeName.equals("LocalTime")) {
      ret = "DateTimeFormatter.ISO_LOCAL_TIME";
    }
    return ret;
  }

  private class GetterReturn {
    public List<String> bodyLines = new ArrayList<String>();
    public String returnCodeChunk;
  };
  
  protected GetterReturn formulateSingleDateGetterCode(Method getterMeth,
                                                       boolean isStringRetType,
                                                       String dateFieldName) {
    Type java8DateType = getJava8DateType();
    GetterReturn ret = new GetterReturn();
    
    if(java8DateType.getTypeName().equals("ZonedDateTime")) {
      List<String> bodyLines = new ArrayList<>();
      ret.bodyLines.add(String.format("final ZonedDateTime zdt = ZonedDateTime.parse(%s);",
                                  dateFieldName));
      if(isStringRetType) {
        ret.bodyLines.add("final ZonedDateTime zdtUtc = zdt.withZoneSameInstant(ZoneOffset.UTC);");
        getterMeth.addImportRef("java.time.ZoneOffset");
        String formatter = getDateTimeFormatter(java8DateType);
        ret.returnCodeChunk = String.format("zdtUtc.format(%s)", formatter);
      } else {
        ret.returnCodeChunk = "zdt.withZoneSameInstant(ZoneOffset.UTC)";
        getterMeth.addImportRef("java.time.ZoneOffset");
      }
    } else {
      List<String> lineTokens = new ArrayList<>();
      String var = null;
      if(java8DateType.getTypeName().equals("LocalDate")) {
        var = "ld";
      } else if(java8DateType.getTypeName().equals("LocalTime")) {
        var = "lt";
      }
      String prefix = String.format("final %s %s = %s.parse(", java8DateType.getTypeName(),
                                    var, java8DateType.getTypeName());
      lineTokens.add(dateFieldName);
      lineTokens.add(getDateTimeFormatter(java8DateType) + ");");
      final String successiveIndent = Util.getRepeatedStr(prefix.length(), " ");
      ret.bodyLines
        = Util.wrapJoinTokensLn(prefix, getterMeth.getBodyMaxLineLen(), successiveIndent, ", ", "",
                                Util.JoinTokenStyle.APPEND, lineTokens, false, true, true);
      if(isStringRetType) {
        String formatter = getDateTimeFormatter(java8DateType);
        ret.returnCodeChunk = String.format("%s.format(%s)", var, formatter);
      } else {
        ret.returnCodeChunk = var;
      }
    }

    return ret;
  }
  
  public Method getDateGetterMethod(boolean isStringRetType) {
    final String fieldName = getFieldName();
    final String paramType = getParamType();

    Type java8DateType = getJava8DateType();
    final Method getter = getThisClass().createMethod(getPrimaryGetterMethodName(),
                                                      AccessModifier.PUBLIC);
    getter.addImportRef(String.format("java.time.%s", java8DateType.getTypeName()));
    getter.addImportRef("java.time.format.DateTimeFormatter");

    Type retType = null;
    String body    = null;
    List<String> bodyLines = new ArrayList<>();

    Type nonContainerRetType = null;
    if(isStringRetType) {
      nonContainerRetType = SimpleType.getStringType();
    } else {
      nonContainerRetType = java8DateType;
    }

    if(getIsContainer()) {
      getter.addImportRef("java.util.List");
      getter.addImportRef("java.util.ArrayList");
      retType = new CompositeType(SimpleType.getListType(), nonContainerRetType);

      String retVar = "retList";
      bodyLines.add(String.format("final %s %s = new ArrayList<>();", retType, retVar));

      String singleVar = fieldName + "Item";
      bodyLines.add(String.format("for(%s %s : %s) {", getNonContainerType().getTypeName(),
                                  singleVar, fieldName));
      bodyLines.add(String.format("  if(%s != null) {", singleVar));
      GetterReturn ret = formulateSingleDateGetterCode(getter, isStringRetType, singleVar);
      bodyLines.addAll(Util.indentLines(4, ret.bodyLines));;
      bodyLines.add(String.format("    %s.add(%s);", retVar, ret.returnCodeChunk));
      bodyLines.add("  }");
      bodyLines.add("}");
      bodyLines.add(String.format("return %s;", retVar));
    } else {
      retType = nonContainerRetType;
      GetterReturn ret = formulateSingleDateGetterCode(getter, isStringRetType, fieldName);
      bodyLines.add(String.format("if(%s == null) {", fieldName));
      bodyLines.add("  return null;");
      bodyLines.add("}");
      bodyLines.addAll(ret.bodyLines);
      bodyLines.add(String.format("return %s;", ret.returnCodeChunk));
    }
    body = String.join(Util.nl(), bodyLines);
    
    getter.setBody(body);
    getter.addJavaDocDescLine("Returns the " + fieldName + " field.");
    getter.setReturnType(retType);
    getter.setJavaDocReturnDesc("The " + fieldName + " field.");
    return getter;
  }

  public List<Method> getAdditionalGetterMethods() {
    List<Method> retList = new ArrayList<>();
    return retList;
  }

  public List<Method> getGetterMethods() {
    List<Method> retList = new ArrayList<>();
    retList.add(getPrimaryGetterMethod());
    retList.addAll(getAdditionalGetterMethods());
    return retList;
  }

  public String getPrimarySetterMethodName() {
    final String setterFuncName;
    if(getIsContainer()) {
      final String pluralField = Util.pluralizeNoun(fieldName);
      setterFuncName = "set" + Util.ucFirstOnly(pluralField);
    } else {
      setterFuncName = "set" + Util.ucFirstOnly(fieldName);
    }
    return setterFuncName;
  }

  public Method getPrimarySetterMethod() {
    return getNonDateSetterMethodUtil(getPrimarySetterMethodName());
  }

  public Method getNonDateSetterMethodWithSuffix(String methodSuffix) {
    final String fieldName = getFieldName();

    String setterFuncName;
    if(getIsContainer()) {
      final String pluralField = Util.pluralizeNoun(fieldName);
      setterFuncName = "set" + Util.ucFirstOnly(pluralField);
    } else {
      setterFuncName = "set" + Util.ucFirstOnly(fieldName);
    }
    if(methodSuffix != null) {
      setterFuncName += methodSuffix;
    }
    return getNonDateSetterMethodUtil(setterFuncName);
  }

  public Method getNonDateSetterMethodUtil(String methodName) {
    final Method setter = getThisClass().createMethod(methodName, AccessModifier.PUBLIC);

    final Param arg1 = new Param(fieldName, getDeclaredType());
    arg1.setIsFinal(true);
    setter.addParam(arg1);

    List<String> assignTokens = new ArrayList<>();
    assignTokens.add("this." + fieldName);
    assignTokens.add(fieldName + ";");
    
    final String body = Util.wrapJoinTokens("", setter.getBodyMaxLineLen(), "  ", " = ", "",
                                            Util.JoinTokenStyle.PREPEND, assignTokens, false,
                                            true, true);
    setter.setBody(body);
    setter.addJavaDocDescLine("Sets the " + fieldName + " field.");
    setter.setReturnType(SimpleType.getVoidType());
    return setter;
  }

  public Method getDateSetterMethod() {
    final String fieldName = getFieldName();

    String setterFuncName;
    if(getIsContainer()) {
      final String pluralField = Util.pluralizeNoun(fieldName);
      setterFuncName = "set" + Util.ucFirstOnly(pluralField);
    } else {
      setterFuncName = "set" + Util.ucFirstOnly(fieldName);
    }

    Type java8DateType = getJava8DateType();
    setterFuncName += "From" + java8DateType.getTypeName();
    
    final Method setter = getThisClass().createMethod(setterFuncName, AccessModifier.PUBLIC);

    setter.addImportRef(String.format("java.time.%s", java8DateType.getTypeName()));
    setter.addImportRef("java.time.ZonedDateTime");
    setter.addImportRef("java.time.ZoneOffset");

    String param1Type = null;
    if(! getIsContainer()) {
      param1Type = java8DateType.getTypeName();
    } else {
      param1Type = String.format("List<%s>", java8DateType.getTypeName());
    }
    final Param arg1 = new Param(fieldName, param1Type, null);
    arg1.setIsFinal(true);
    setter.addParam(arg1);

    List<String> bodyLines = new ArrayList<>();

    if(! getIsContainer()) {
      if(java8DateType.getTypeName().equals("LocalTime")) {
        setter.addImportRef("java.time.format.DateTimeFormatter");
        bodyLines.add(String.format(
                    "final String s = %s.format(DateTimeFormatter.ISO_LOCAL_TIME);",
                    fieldName));
        bodyLines.add(String.format("this.%s = s;", fieldName));
      } else if(java8DateType.getTypeName().equals("LocalDate")) {
        setter.addImportRef("java.time.format.DateTimeFormatter");
        bodyLines.add(String.format(
                              "final String s = %s.format(DateTimeFormatter.ISO_LOCAL_DATE);",
                              fieldName));
        bodyLines.add(String.format("this.%s = s;", fieldName));
        setter.addImportRef("java.time.LocalTime");
      } else if(java8DateType.getTypeName().equals("ZonedDateTime")) {
        bodyLines.add(String.format(
                            "final ZonedDateTime zdt = %s.withZoneSameInstant(ZoneOffset.UTC);",
                            fieldName));
        bodyLines.add(String.format("this.%s = zdt.toString();", fieldName));
      }
    } else {
      String singleVar = fieldName + "Item";
      bodyLines.add(String.format("for(%s %s : %s) {", java8DateType.getTypeName(),
                                  singleVar, fieldName));
      if(java8DateType.getTypeName().equals("LocalTime")) {
        setter.addImportRef("java.time.format.DateTimeFormatter");
        bodyLines.add(String.format(
                      "  final String s = %s.format(DateTimeFormatter.ISO_LOCAL_TIME);",
                      singleVar));
        bodyLines.add(String.format("  this.%s.add(s);", fieldName));
      } else if(java8DateType.getTypeName().equals("LocalDate"))  {
        setter.addImportRef("java.time.format.DateTimeFormatter");
        bodyLines.add(String.format(
                              "  final String s = %s.format(DateTimeFormatter.ISO_LOCAL_DATE);",
                              singleVar));
        bodyLines.add(String.format("  this.%s.add(s);", fieldName));
      } else if(java8DateType.getTypeName().equals("ZonedDateTime")) {
        bodyLines.add(String.format(
                            "  final ZonedDateTime zdt = %s.withZoneSameInstant(ZoneOffset.UTC);",
                            singleVar));
        bodyLines.add(String.format("  this.%s.add(zdt.toString());", fieldName));
      }
      bodyLines.add("}");
    }
    String body = String.join(Util.nl(), bodyLines);

    setter.setBody(body);
    setter.addJavaDocDescLine("Sets the " + fieldName + " field.");
    setter.setReturnType(SimpleType.getVoidType());
    return setter;
  }

  public List<Method> getAdditionalSetterMethods() {
    List<Method> retList = new ArrayList<>();
    if(! isRequestStyle()) {
      if(isDateFormat(this.swaggerFormat) ||
         isDateFormat(this.itemsSwaggerFormat)) {
        retList.add(getDateSetterMethod());
      }
    }
    return retList;
  }

  public List<Method> getSetterMethods() {
    List<Method> retList = new ArrayList<>();
    retList.add(getPrimarySetterMethod());
    return retList;
  }
}

