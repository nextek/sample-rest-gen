package com.sample.codegen.restgen.modelgen;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sample.codegen.SwaggerUtil;
import com.sample.codegen.Util;
import com.sample.codegen.lang.java.LClass;

import io.swagger.models.Model;
import io.swagger.models.Swagger;
import io.swagger.models.parameters.BodyParameter;

abstract public class SwaggerFieldClass extends LClass {
  private Swagger swagger;
  private String  serviceName;
  private String  schemaName;
  private Model   swaggerModel;
  private Map<String, ModelField> modelFieldsMap = new HashMap<String, ModelField>();

  private int maxFieldLen         = 0;
  private int maxDeclaredLen      = 0;
  private int maxClassDeclaredLen = 0;

  public SwaggerFieldClass(final Swagger swagger, final String serviceName, final String pkgName,
                           final String className, final String schemaName,
                           final Model swaggerModel) {
    super(className);
    this.setPkgName(pkgName);

    this.swagger      = swagger;
    this.serviceName  = serviceName;
    this.schemaName   = schemaName;
    this.swaggerModel = swaggerModel;
  }

  public Swagger getSwagger() {
    return swagger;
  }
  public String getSchemaName() {
    return schemaName;
  }
  public Model getSwaggerModel() {
    return swaggerModel;
  }

  public void saveModelField(final ModelField mf) {
    modelFieldsMap.put(mf.getFieldName(), mf);
  }

  public ModelField getModelField(final String fieldName) {
    return modelFieldsMap.get(fieldName);
  }

  public List<String> getFieldNames() {
    final List<String> ret = new ArrayList<String>();
    ret.addAll(modelFieldsMap.keySet());
    return ret;
  }

  public void generate(final String outdir, final String pkgRoot) {
    //System.err.println("generate a ModelClass");
    final LClass.GenerationOptions options = new LClass.GenerationOptions();
    options.ensureBlankLineBeforeFldAnnotations = true;
    options.verticallyLineUpFields              = true;
    final String classTxt = getClassTxt(options);
    final String outPath  = getOutPath(outdir, pkgRoot);
    Util.writeGeneratedFile(outPath, classTxt);
  }

  public String getServiceName(){
    return serviceName;
  }

  abstract public Boolean getValidateClassObjOnInput();
}

