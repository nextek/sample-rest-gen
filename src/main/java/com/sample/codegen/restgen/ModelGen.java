package com.sample.codegen.restgen;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import com.sample.codegen.SwaggerUtil;
import com.sample.codegen.Util;
import com.sample.codegen.config.Configuration;
import com.sample.codegen.lang.java.CompositeType;
import com.sample.codegen.lang.java.SimpleType;
import com.sample.codegen.lang.java.Type;
import com.sample.codegen.restgen.modelgen.ModelClass;
import com.sample.codegen.restgen.modelgen.ModelClassSet;
import com.sample.codegen.restgen.modelgen.RequestClass;
import com.sample.codegen.restgen.modelgen.ModelClass.ModelClassStyle;

import io.swagger.models.ArrayModel;
import io.swagger.models.HttpMethod;
import io.swagger.models.Model;
import io.swagger.models.ModelImpl;
import io.swagger.models.Operation;
import io.swagger.models.Path;
import io.swagger.models.RefModel;
import io.swagger.models.Response;
import io.swagger.models.Swagger;
import io.swagger.models.parameters.BodyParameter;
import io.swagger.models.parameters.Parameter;
import io.swagger.models.properties.ArrayProperty;
import io.swagger.models.properties.Property;
import io.swagger.models.properties.RefProperty;

public class ModelGen {
  private Swagger       swagger;
  private Configuration config;

  private Map<String, String> pathMeth2RequestBodySchemaName = new HashMap<>();
  private Map<String, String> pathMeth2RequestBodyContainer  = new HashMap<>();

  private Map<String, ModelClass> schema2RequestBodyClass    = new HashMap<>();
  private Map<String, ModelClass> schema2ResponseClass       = new HashMap<>();

  private List<RequestClass>  requestClasses  = new ArrayList<RequestClass>();
  private List<ModelClass> requestBodyClasses = new ArrayList<ModelClass>();
  private List<ModelClass> responseClasses    = new ArrayList<ModelClass>();

  private Map<String, Map<String, String>> pathMethRetcode2ResponseSchemaName
    = new HashMap<String, Map<String, String>>();

  public ModelGen(final Swagger swagger, final Configuration configuration) {
    this.swagger = swagger;
    this.config  = configuration;

    init();
    prepare();
  }

  public Swagger            getSwagger() {
    return swagger;
  }
  public Configuration      getConfig() {
    return config;
  }
  public List<RequestClass> getRequestClasses() {
    return requestClasses;
  }
  public List<ModelClass>   getBodyRequestClasses() {
    return requestBodyClasses;
  }
  public List<ModelClass>   getResponseClasses() {
    return responseClasses;
  }

  protected void init() {
    createRequestClasses();
    createResponseClasses();
  }

  protected void prepare() {
    prepareRequestClasses();
    prepareResponseClasses();
  }

  protected void createRequestClasses() {
    final Map<String, Path> pathsMap = swagger.getPaths();
    final Map<String, Model> schema2ModelImpl = new HashMap<>();

    //System.err.println("_create_request_classes");

    for(Map.Entry<String, Path> entry : pathsMap.entrySet()) {
      //System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());

      final String pathString = entry.getKey();
      final Path   path       = entry.getValue();

      final Map<HttpMethod, Operation> operationMap = path.getOperationMap();

      OPERATION_LOOP:
      for(Map.Entry<HttpMethod, Operation> entryB : operationMap.entrySet()) {
        final HttpMethod method     = entryB.getKey();
        final String methodName     = method.name();
        final Operation  operation  = entryB.getValue();
        final Map<String, Object> vendorExtList = operation.getVendorExtensions();

        if(vendorExtList.get("x-gen-ignore") != null) {
          continue OPERATION_LOOP;
        }

        //System.err.println("pathString: " + pathString + ", methodName: " + methodName);

        final List<ParamCat> paramCategories = new ArrayList<ParamCat>();

        final boolean hasBodyParams
          = SwaggerUtil.OperationC.hasParamsOfType(operation, "BodyParameter");
        final boolean hasOtherParams
          = SwaggerUtil.OperationC.hasParamsNotOfType(operation, "BodyParameter");

        if(hasBodyParams) {
          paramCategories.add(ParamCat.BODY);
        }

        if(hasOtherParams) {
          paramCategories.add(ParamCat.OTHER);
        }

        PARAM_CAT_LOOP:
        for(ParamCat paramCategory : paramCategories) {
          String schemaName = null;
          String containerType = null;

          if(paramCategory == ParamCat.BODY) {
            final List<Parameter> bodyParams
              = SwaggerUtil.OperationC.getParamsOfType(operation, "BodyParameter");

            final BodyParameter bodyParam = (BodyParameter) bodyParams.get(0);
            Model schema = bodyParam.getSchema();
            if(schema instanceof ArrayModel) {
              final ArrayModel arrayModel = (ArrayModel) schema;
              Property prop = arrayModel.getItems();
              if(prop instanceof RefProperty) {
                final RefProperty refProp = (RefProperty) prop;
                schemaName   = refProp.getSimpleRef();
              }
              containerType = "Array";
            } else if(schema instanceof RefModel) {
              final RefModel refSchema = (RefModel) schema;
              schemaName               = refSchema.getSimpleRef();
            } else if(schema instanceof ModelImpl) {
              String lastPathPart = Util.getLastPathPart(pathString);
              schemaName = lastPathPart + Util.ucFirst(methodName);
              schema2ModelImpl.put(schemaName, schema);
            } else {
              throw new RuntimeException("unsupported schema type " + schema.getClass().getName());
            }

            final String key = pathString + ";" + methodName;
            pathMeth2RequestBodySchemaName.put(key, schemaName);
            pathMeth2RequestBodyContainer.put(key, containerType);
          } else if(paramCategory == ParamCat.OTHER) {
            final String[] ret = makeRequestPkgClass(pathString, methodName, paramCategory,
                                                     schemaName);
            final String pkgName   = ret[0];
            final String className = ret[1];

            final RequestClass requestClass
              = new RequestClass(swagger, config.getServiceName(), pkgName, className,
                                 schemaName, pathString, method);
            requestClasses.add(requestClass);
          }
        }
      }
    }

    final Map<String, Boolean> bodySchemaNamesMap = new HashMap<String, Boolean>();
    final List<String> schemaNameList = new ArrayList<>();
    schemaNameList.addAll(pathMeth2RequestBodySchemaName.values());

    for(int i = 0; i < schemaNameList.size(); i++) {
      final String schemaName = schemaNameList.get(i);
      final Model model = SwaggerUtil.SwaggerC.getModel(getSwagger(), schemaName);
      if(model != null) {
        bodySchemaNamesMap.put(schemaName, true);
        final List<String> childModels
          = SwaggerUtil.ModelC.getReferencedSchemas(model, getSwagger());

        for(String childSchemaName : childModels) {
          bodySchemaNamesMap.put(childSchemaName, true);
          schemaNameList.add(childSchemaName);
        }
      } else {
        bodySchemaNamesMap.put(schemaName, true);
      }
    }

    for(String schemaName : bodySchemaNamesMap.keySet()) {
      final String className   = Util.ucFirstOnly(schemaName) + "Request";
      final String pkgName     = "com.sample.soa." + config.getSvcPkgToken()
                               + ".model.request.body";

      final Model modelSpec = schema2ModelImpl.get(schemaName);

      final ModelClass bodyRequestClass
        = new ModelClass(getSwagger(), config.getServiceName(), pkgName, className, schemaName,
                         ModelClassStyle.REQUEST, modelSpec);


      //System.err.println("  created Body RequestClass " + pkgName + " / " + className);

      schema2RequestBodyClass.put(schemaName, bodyRequestClass);
      requestBodyClasses.add(bodyRequestClass);
    }
  }

  protected void createResponseClasses() {
    final Map<String, Path> pathsMap = swagger.getPaths();

    //System.err.println("_create_request_classes");

    final List<String> responseSchemaNames = new ArrayList<String>();

    for(Map.Entry<String, Path> entry : pathsMap.entrySet()) {
      //System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());

      final String pathString = entry.getKey();
      final Path   path       = entry.getValue();

      final Map<HttpMethod, Operation> operationMap = path.getOperationMap();

      OPERATION_LOOP:
      for(Map.Entry<HttpMethod, Operation> entryB : operationMap.entrySet()) {
        final HttpMethod method     = entryB.getKey();
        final String methodName     = method.name();
        final Operation  operation  = entryB.getValue();
        final Map<String, Object> vendorExtList = operation.getVendorExtensions();

        if(vendorExtList.get("x-gen-ignore") != null) {
          continue OPERATION_LOOP;
        }

        final Map<String, Response> responsesMap = operation.getResponses();

        RESPONSE_LOOP:
        for(Map.Entry<String, Response> entryC : responsesMap.entrySet()) {
          final String retCode    = entryC.getKey();
          final Response response = entryC.getValue();
          if(response == null) {
            continue RESPONSE_LOOP;
          }
          Property schema   = response.getSchema();

          if(schema == null) {
            // this is a normal condition if an endpoint doesn't return a model/schema
            //System.err.println("NULL Schema RESP: path: " + pathString + ", method: "
            //                   + methodName + ", response " + retCode);
            continue RESPONSE_LOOP;
          }

          //System.err.println("NULL Schema RESP: path: " + pathString + ", method: "
          //                   + methodName + ", response " + retCode + ", schema: " + schema);

          if(schema instanceof ArrayProperty) {
            final ArrayProperty arrayProp = (ArrayProperty) schema;
            schema = arrayProp.getItems();
          }

          if(schema instanceof RefProperty) {
            final RefProperty refProp = (RefProperty) schema;
            final String schemaName   = refProp.getSimpleRef();
            //System.err.println("      schemaName: " + schemaName);
            responseSchemaNames.add(schemaName);

            final String key = pathString + ";" + methodName;
            Map<String, String> retCodeMap;

            if(pathMethRetcode2ResponseSchemaName.containsKey(key)){
              retCodeMap = pathMethRetcode2ResponseSchemaName.get(key);
            } else {
              retCodeMap = new HashMap<String, String>();
              pathMethRetcode2ResponseSchemaName.put(key, retCodeMap);
            }

            retCodeMap.put(retCode, schemaName);
          } else {
            throw new RuntimeException("don't know how to handle schema of type "
                                       + schema.getClass());
          }
        }
      }
    }

    final Map<String, Boolean> allSchemaNamesMap = new HashMap<String, Boolean>();
    for(String schemaName : responseSchemaNames) {
      final Model model = SwaggerUtil.SwaggerC.getModel(swagger, schemaName);
      allSchemaNamesMap.put(schemaName, true);
      final List<String> childModels = SwaggerUtil.ModelC.getReferencedSchemas(model, swagger);

      for(String childSchemaName : childModels) {
        allSchemaNamesMap.put(childSchemaName, true);
      }
    }

    final List<String> allSchemaNames = new ArrayList<String>();
    allSchemaNames.addAll(allSchemaNamesMap.keySet());
    ALL_SCHEMA_LOOP:
    for(String schemaName : allSchemaNames) {
      if("ErrorDetail".equals(schemaName) || "ErrorNotification".equals(schemaName)) {
        // skip this schema since they have this model class in soa-commons
        continue ALL_SCHEMA_LOOP;
      }

      final String className   = Util.ucFirstOnly(schemaName) + "Response";
      final String pkgName     = "com.sample.soa." + config.getSvcPkgToken() + ".model.response";

      final ModelClass responseClass = new ModelClass(swagger, getConfig().getServiceName(),
                                                      pkgName, className, schemaName,
                                                      ModelClassStyle.RESPONSE, null);

      responseClasses.add(responseClass);
      schema2ResponseClass.put(schemaName, responseClass);
    }

  }

  protected void prepareRequestClasses() {
    for(RequestClass requestClass : getRequestClasses()) {
      requestClass.prepare();
    }

    final Function<String, ModelClass> schema2BodyModelClassFunc
      = (str -> this.getRequestBodyClassFromSchemaName(str));

    for(ModelClass bodyRequestClass : getBodyRequestClasses()) {
      bodyRequestClass.prepare(schema2BodyModelClassFunc);
    }
  }

  protected void prepareResponseClasses() {
    final Function<String, ModelClass> schema2modelClassFunc
      = (str -> this.getResponseClassFromSchemaName(str));

    for(ModelClass responseClass : getResponseClasses()) {
      responseClass.prepare(schema2modelClassFunc);
    }
  }

  // returns array of [ pkgName, className ]
  protected String[] makeRequestPkgClass(final String pathString, final String methodName,
                                         final ParamCat paramCategory, final String schemaName) {
    //System.err.println("pathString: " + pathString + ", schema: " + schemaName);
    final String[] pathPartsRaw = pathString.split("/");
    final List<String> pathPartsOrig = Util.getPathPartsList(pathString);
    final List<String> pathParts     = new ArrayList<String>();

    for(String part : pathPartsRaw) {
      if(part.isEmpty()) {
        continue;
      }
      if(part.matches(".*[{}].*")) {
        continue;
      }
      final String newPart = part.toLowerCase();
      pathParts.add(newPart);
    }

    final List<String> pkgParts = new ArrayList<String>();
    pkgParts.add("com");
    pkgParts.add("sample");
    pkgParts.add("soa");
    pkgParts.add(config.getSvcPkgToken());
    pkgParts.add("model");
    pkgParts.add("request");

    String classNm;
    if(paramCategory == ParamCat.BODY) {
      if(schemaName == null) {
        throw new RuntimeException("no schema name defined (" + config.getServiceName() + ", "
                             + pathString + ", " + methodName + ", " + paramCategory.name() + ")");
      }
      classNm = Util.ucFirstOnly(schemaName) + "BodyParams";
    } else {
      int pathPartsOrigSize = pathPartsOrig.size();
      List<String> nameParts = new ArrayList<>();

      nameParts.add(methodName.toLowerCase());
      if(pathPartsOrigSize == 1) {
        nameParts.add(Util.ucFirst(pathPartsOrig.get(0)));
      } else {
        String twoFromPart = pathPartsOrig.get(pathPartsOrigSize - 2);
        nameParts.add(Util.ucFirst(twoFromPart.replaceAll("[{}]", "")));
                      
        String oneFromPart = pathPartsOrig.get(pathPartsOrigSize - 1);
        nameParts.add(Util.ucFirst(oneFromPart.replaceAll("[{}]", "")));
      }
      nameParts.add("Params");
      String classNmA = String.join("", nameParts);
      classNm = Util.ucFirstOnly(classNmA);
      pkgParts.addAll(pathParts);
    }

    final String pkg = String.join(".", pkgParts);

    // System.err.println("pkg: " + pkg);
    // System.err.println("class: " + classNm);

    final String[] ret = new String[2];
    ret[0] = pkg;
    ret[1] = classNm;
    return ret;
  }

  public ModelClassSet getModelClassSet(final String pathString, final String methodName) {
    final ModelClassSet modelClassSet = new ModelClassSet();

    final RequestClass requestClass = getRequestClass(pathString, methodName);
    if(requestClass != null) {
      modelClassSet.setRequestClass(requestClass);
    }

    final ModelClass requestBodyClass = getRequestBodyClass(pathString, methodName);
    if(requestBodyClass != null) {
      modelClassSet.setBodyRequestClass(requestBodyClass);
    }

    final ModelClass responseClass = getResponseClass(pathString, methodName);
    modelClassSet.setResponseClass(responseClass);

    final Type bodyType = getRequestBodyType(pathString, methodName);
    if(bodyType != null) {
      modelClassSet.setBodyRequestType(bodyType);
    }

    return modelClassSet;
  }

  public ModelClass getRequestBodyClass(final String pathString, final String methodName) {
    final String key = pathString + ";" + methodName;
    final String schemaName = pathMeth2RequestBodySchemaName.get(key);
    if(schemaName == null) {
      return null;
    }
    return getRequestBodyClassFromSchemaName(schemaName);
  }

  public Type getRequestBodyType(final String pathString, final String methodName) {
    final String key = pathString + ";" + methodName;
    final String schemaName = pathMeth2RequestBodySchemaName.get(key);
    if(schemaName == null) {
      return null;
    }
    ModelClass bodyClass = getRequestBodyClassFromSchemaName(schemaName);
    String containerType = pathMeth2RequestBodyContainer.get(key);
    Type retType; 

    if(containerType != null && containerType.equals("Array")) {
      retType = new CompositeType(new SimpleType("List", "java.util.List"),
                                  new SimpleType(bodyClass));
    } else {
      retType = bodyClass.getType();
    }

    return retType;
  }

  public RequestClass getRequestClass(final String pathString, final String methodName) {
    // for non-body param types
    for(RequestClass requestClass : getRequestClasses()) {
      if(requestClass.getPathString().equals(pathString)
         && requestClass.getMethodName().equals(methodName)) {
        return requestClass;
      }
    }

    return null;
  }

  public ModelClass getResponseClass(final String pathString, final String methodName) {
    final String key = pathString + ";" + methodName;
    final Map<String, String> retCodeMap = pathMethRetcode2ResponseSchemaName.get(key);
    if(retCodeMap == null) {
      return null;
    }

    String wantSchemaName = null;
    for(Map.Entry<String, String> entry : retCodeMap.entrySet()) {
      final String retCode    = entry.getKey();
      final String schemaName = entry.getValue();
      int retCodeInt;
      try {
        retCodeInt = Integer.parseInt(retCode);
      } catch(NumberFormatException e) {
        retCodeInt = 0;
      }
      if(retCodeInt >= 200 && retCodeInt <= 299) {
        wantSchemaName = schemaName;
      }
    }

    if(wantSchemaName == null) {
      return null;
    }

    return getResponseClassFromSchemaName(wantSchemaName);
  }

  public ModelClass getRequestBodyClassFromSchemaName(final String schemaName) {
    return schema2RequestBodyClass.get(schemaName);
  }

  public ModelClass getResponseClassFromSchemaName(final String schemaName) {
    return schema2ResponseClass.get(schemaName);
  }

  public void generate(final String outdir, final String pkgRoot) {
    Map<String, Boolean> pkgsSeen = new HashMap<>();

    for(RequestClass requestClass : getRequestClasses()) {
      requestClass.generate(outdir, pkgRoot);
      String pkgName = requestClass.getPkgName();
      
      if(pkgsSeen.get(pkgName) == null) {
        final List<String> desc = new ArrayList<>();
        desc.add("This package contains classes representing requests in the model domain that "
                 + "are passed as");
        desc.add("non-body parameters at URL path " + requestClass.getPathString() + ".");
        PackageInfoGen.getInstance().add(pkgName, pkgRoot, desc);
        pkgsSeen.put(pkgName, true);
      }
    }

    for(ModelClass bodyRequestClass : getBodyRequestClasses()) {
      bodyRequestClass.generate(outdir, pkgRoot);
      String pkgName = bodyRequestClass.getPkgName();

      if(pkgsSeen.get(pkgName) == null) {
        final List<String> desc = new ArrayList<>();
        desc.add("This package contains classes representing requests in the model domain that");
        desc.add("are passed as body parameters.");
        PackageInfoGen.getInstance().add(pkgName, pkgRoot, desc);
        pkgsSeen.put(pkgName, true);
      }
    }

    for(ModelClass responseClass : getResponseClasses()) {
      responseClass.generate(outdir, pkgRoot);
      String pkgName = responseClass.getPkgName();

      if(pkgsSeen.get(pkgName) == null) {
        final List<String> desc = new ArrayList<>();
        desc.add("This package contains classes representing responses in the model domain.");
        PackageInfoGen.getInstance().add(pkgName, pkgRoot, desc);
        pkgsSeen.put(pkgName, true);
      }
    }
  }
}

