package com.sample.codegen.restgen;

import java.util.ArrayList;
import java.util.List;

import com.sample.codegen.Util;
import com.sample.codegen.config.Configuration;
import com.sample.codegen.lang.java.LClass;
import com.sample.codegen.restgen.enumgen.EnumConstClass;

import io.swagger.models.Swagger;

public class EnumGen {
  private static EnumGen instance;

  private List<EnumConstClass> enums;
  private Swagger swagger;
  private Configuration config;

  private EnumGen() {
    enums = new ArrayList<>();
  }

  public static EnumGen getInstance() {
    if(instance == null){
      instance = new EnumGen();
    }
    return instance;
  }

  public void init(Swagger s, Configuration config){
    this.swagger = s;
    this.config = config;
  }

  public void add(EnumConstClass e){
    enums.add(e);
  }

  public EnumConstClass create(String name, List<String> values) {
    String className = this.convertName(name);

    EnumConstClass e = this.find(className);

    if(e != null){
      if(! e.getEnumValues().equals(values)) {
        throw new RuntimeException("You have two instances of enum named " + name + " that have "
                                   + "enum values lists that don't match");
      }
      return e;
    }

    final String enumPkg = "com.sample.soa." + config.getSvcPkgToken()
                         + ".model.constants";

    e = new EnumConstClass(enumPkg, className, name, values);
    e.setJavaDocVersion(this.config.getJavaDocVersion());
    e.setJavaDocAuthor(this.config.getJavaDocAuthor());
    e.addJavaDocDescLine("Enum representing an enum model parameter named '" + name + "'.");

    this.add(e);

    return e;
  }

  public EnumConstClass find(String name) {
    String className = this.convertName(name);
    for(EnumConstClass e : this.enums){
      if(e.getClassName().equals(className)){
        return e;
      }
    }
    return null;
  }

  public void generate(final String outdir, final String pkgRoot){
    for(EnumConstClass e : this.enums){
      e.generate(outdir, pkgRoot);
    }

    if(this.enums.size() > 0){
      final List<String> description = new ArrayList<String>();

      description.add("This package contains enums representing enum type parameters in "
                    + "request models.");
      PackageInfoGen.getInstance().add("com.sample.soa." + config.getSvcPkgToken() +
                                       ".model.constants", pkgRoot, description);
    }
  }


  //---------------------------
  //       GENERATOR METHODS
  //---------------------------
  private String convertName(String name) {
    return Util.ucFirstOnly(name);
  }

  // STATIC INFO METHODS
  public static String getEnumClassName(String name){
    return Util.ucFirstOnly(name);
  }
}
