package com.sample.codegen.restgen.staticgen;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sample.codegen.config.Configuration;

public class StaticHelper {
  private static String getFullUrl(String nexusUrl, String ppBasePath, String ppGroupIdPath,
                                   String ppArtifactId) {
    List<String> tokens = new ArrayList<>();
    tokens.add(nexusUrl.replaceFirst("/$", ""));
    tokens.add(ppBasePath.replaceFirst("^/", "").replaceFirst("/$", ""));
    tokens.add(ppGroupIdPath.replaceFirst("^/", "").replaceFirst("/$", ""));
    tokens.add(ppArtifactId.replaceFirst("^/", "").replaceFirst("/$", ""));
    tokens.add("maven-metadata.xml");
    String fullUrl = String.join("/", tokens);
    return fullUrl;
  }

  public static String getParentVersion(String nexusUrl, String ppBasePath, String ppGroupIdPath,
                                        String ppArtifactId) {
    String fullNexusUrlStr = getFullUrl(nexusUrl, ppBasePath, ppGroupIdPath, ppArtifactId);
    //System.err.println("fullNexusUrlStr: (" + fullNexusUrlStr + ")");
    try {
      URL fullNexusUrl = new URL(fullNexusUrlStr);
      URLConnection connection = fullNexusUrl.openConnection();

      int contentLength = connection.getContentLength();
      InputStream raw = connection.getInputStream();

      BufferedInputStream in = new BufferedInputStream(raw);
      byte[] buffer = new byte[contentLength]; //read it in one fell swoop
      int offset = 0;
      while (offset < contentLength) {
        int bytesRead = in.read(buffer, offset, buffer.length - offset);
        if (bytesRead == -1)
          break;
        offset += bytesRead;
      }
      in.close();

      if (offset != contentLength) {
        throw new IOException("Only read " + offset + " bytes; Expected " + contentLength
                              + " bytes");
      }

      final String data = new String(buffer);
      final Pattern pattern = Pattern.compile("<release>(.*)</release>");
      final Matcher matcher = pattern.matcher(data);

      if(matcher.find()){
        return matcher.group(1);
      } else{
        throw new RuntimeException("Could not find version info in the nexus response.");
      }
    } catch(MalformedURLException e){
      System.err.println("Could not load url '" + fullNexusUrlStr + "'");
      throw new RuntimeException("Could not load url '" + fullNexusUrlStr + "'", e);
    } catch (IOException e) {
      System.err.println("Could not load url '" + fullNexusUrlStr + "'");
      throw new RuntimeException("Could not load url '" + fullNexusUrlStr + "'", e);
    }
  }
}
