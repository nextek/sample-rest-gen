package com.sample.codegen.restgen.testgen;

import java.util.ArrayList;
import java.util.List;

import com.sample.codegen.SwaggerUtil;
import com.sample.codegen.Util;
import com.sample.codegen.config.Configuration;
import com.sample.codegen.lang.java.AccessModifier;
import com.sample.codegen.lang.java.Field;
import com.sample.codegen.lang.java.LClass;
import com.sample.codegen.lang.java.Method;
import com.sample.codegen.lang.java.SimpleType;
import com.sample.codegen.lang.java.Type;
import com.sample.codegen.restgen.ModelGen;
import com.sample.codegen.restgen.TestGen;
import com.sample.codegen.restgen.modelgen.ModelClass;

import io.swagger.models.HttpMethod;
import io.swagger.models.Operation;
import io.swagger.models.Path;
import io.swagger.models.Swagger;

public class TestResourceFunctionClass extends LClass {
  private Swagger swagger;
  private String packageName;
  private String className;
  private String anchor;
  private ModelGen modelGen;
  private String serviceName;
  private TestGen testGen;
  private DataClass dataClass;

  private List<MethodPathTuple> methodPathPairs = new ArrayList<>();

  public TestResourceFunctionClass(final Swagger swagger, final String packageName,
                                   final String className, final String anchor,
                                   final ModelGen modelGen, final String serviceName,
                                   final TestGen testGen, final DataClass dataClass) {
    super(className);
    this.swagger = swagger;
    this.packageName = packageName;
    this.className = className;
    this.anchor = anchor;
    this.modelGen = modelGen;
    this.serviceName = serviceName;
    this.testGen = testGen;
    this.dataClass = dataClass;

    prepare();
  }

  private void prepare(){
    this.setPkgName(this.packageName);
  }

  public void build(){
    // FIELDS
    final Field serverField = new Field("server", AccessModifier.PRIVATE, 98, 2);
    serverField.setType(new SimpleType("EmbeddedWebApplicationContext", null));
    serverField.addAnnotation("@Autowired");

    final Field portField = new Field("port", AccessModifier.PRIVATE, 98, 2);
    portField.setType(SimpleType.getIntPrimitiveType());
    portField.addAnnotation("@Value('${local.server.port}')");

    this.addField(serverField);
    this.addField(portField);


    // METHODS
    final Method setupMethod = new Method("setUp", AccessModifier.PRIVATE);
    setupMethod.setReturnType(SimpleType.getVoidType());
    setupMethod.addAnnotation("@Before");
    setupMethod.setBody("RestAssured.port = port;");

    this.addMethod(setupMethod);

    this.addMethod(buildPostSupportMethod());
    this.addMethod(buildGetSupportMethod());

    if(getMethodActualPath(HttpMethod.GET, false) != null) {
      final Method getSupportMethod = buildGetQuerySupportMethod();
      this.addMethod(getSupportMethod);
    }
    if(getMethodActualPath(HttpMethod.DELETE) != null){
      final Method deleteSupportMethod = buildDeleteQuerySupportMethod();
      this.addMethod(deleteSupportMethod);
    }

    final Method createParentMethod = buildCreateParentMethod();
    this.addMethod(createParentMethod);

    final Method createParentAndSelfMethod = buildCreateParentAndSelfMethod();
    this.addMethod(createParentAndSelfMethod);

    final Method createPostModelRecFullMethod = buildCreatePostModelRecFullMethod();
    this.addMethod(createPostModelRecFullMethod);

    final Method createPostModelRecRequiredMethod = buildCreatePostModelRecRequiredMethod();
    this.addMethod(createPostModelRecRequiredMethod);

    final List<Method> testMethods = makeTests();
    testMethods.forEach(m -> this.addMethod(m));

    // CLASS ATTRIBUTES
    String classServiceName = Util.ucFirstOnly(this.serviceName);
    classServiceName = classServiceName.replaceAll("[^a-zA-Z_]", "");

    final String mainAppClass = classServiceName + "Application";

    this.addImportRef("com.sample.soa." + Configuration.getConfig().getSvcPkgToken()
                      + ".appconfig." + mainAppClass);
    this.addImportRef("org.junit.runner.RunWith");
    this.addImportRef("org.junit.Assert.assertNull:static");
    this.addImportRef("org.junit.Assert.assertNotNull:static");
    this.addImportRef("org.junit.Assert.assertEquals:static");
    this.addImportRef("org.springframework.boot.test.SpringApplicationConfiguration");
    this.addImportRef("org.springframework.boot.test.WebIntegrationTest");
    this.addImportRef("org.springframework.context.annotation.Profile");
    this.addImportRef("org.springframework.beans.factory.annotation.Autowired");
    this.addImportRef("org.junit.Before");
    this.addImportRef("org.junit.Test");
    this.addImportRef("org.springframework.beans.factory.annotation.Value");
    this.addImportRef("org.springframework.test.context.junit4.SpringJUnit4ClassRunner");
    this.addImportRef("org.springframework.boot.context.embedded.EmbeddedWebApplicationContext");
    this.addImportRef("com.jayway.restassured.RestAssured");
    this.addImportRef("com.jayway.restassured.response.Response");

    this.addAnnotation("@RunWith(SpringJUnit4ClassRunner.class)");
    this.addAnnotation("@SpringApplicationConfiguration(classes = [" + mainAppClass + ".class])");
    this.addAnnotation("@WebIntegrationTest('server.port:0')");
    this.addAnnotation("@Profile('LOCAL')");
  }

  private List<Method> makeTests() {
    // TODO
    return new ArrayList<Method>();
  }

  private Method buildPostSupportMethod() {
    final String postPath = getMethodActualPath(HttpMethod.POST);
    ModelClass modelClass = getModelClass();
    boolean hasReturnHeader = false;

    if(postPath != null) {

    }

    // TODO: returning placeholder for now
    final Method placeholder = new Method("postSupport", AccessModifier.PRIVATE);
    //placeholder.setReturnType(this.modelGen.getModelClassSet(postPath, ));
    placeholder.setReturnType(SimpleType.getVoidType());

    return placeholder;
  }

  private Method buildGetSupportMethod() {
    // TODO: returning placeholder for now
    final Method placeholder = new Method("getSelfSupport", AccessModifier.PRIVATE);
    //placeholder.setReturnType(this.modelGen.getModelClassSet(postPath, ));
    placeholder.setReturnType(SimpleType.getVoidType());

    return placeholder;
  }

  private Method buildGetQuerySupportMethod(){
    // TODO: returning placeholder for now
    final Method placeholder = new Method("getQuerySupport", AccessModifier.PRIVATE);
    //placeholder.setReturnType(this.modelGen.getModelClassSet(postPath, ));
    placeholder.setReturnType(SimpleType.getVoidType());

    return placeholder;
  }

  private Method buildDeleteQuerySupportMethod(){
    // TODO: returning placeholder for now
    final Method placeholder = new Method("deleteQuerySupport", AccessModifier.PRIVATE);
    //placeholder.setReturnType(this.modelGen.getModelClassSet(postPath, ));
    placeholder.setReturnType(SimpleType.getVoidType());

    return placeholder;
  }

  private Method buildCreateParentMethod() {
    // TODO: returning placeholder for now
    final Method placeholder = new Method("createParentResource", AccessModifier.PRIVATE);
    //placeholder.setReturnType(this.modelGen.getModelClassSet(postPath, ));
    placeholder.setReturnType(SimpleType.getVoidType());

    return placeholder;
  }

  private Method buildCreateParentAndSelfMethod() {
    // TODO: returning placeholder for now
    final Method placeholder = new Method("createSelfAndParentResource", AccessModifier.PRIVATE);
    placeholder.setReturnType(SimpleType.getMapType());

    return placeholder;
  }

  private Method buildCreatePostModelRecFullMethod() {
    // TODO: returning placeholder for now
    final Method placeholder = new Method("createPostModelRecFull", AccessModifier.PRIVATE);
    placeholder.setReturnType(SimpleType.getVoidType());

    return placeholder;
  }

  private Method buildCreatePostModelRecRequiredMethod() {
    // TODO: returning placeholder for now
    final Method placeholder = new Method("createPostModelRecRequired", AccessModifier.PRIVATE);
    placeholder.setReturnType(SimpleType.getVoidType());

    return placeholder;
  }

  private ModelClass getModelClass(){
    final String getPath = this.getMethodActualPath(HttpMethod.GET, true);
    // TODO
    return null;
  }

  private String getMethodActualPath(final HttpMethod method){
    return this.getMethodActualPath(method, false);
  }

  private String getMethodActualPath(final HttpMethod method, final boolean wantSingleGetRef){

    final ArrayList<MethodPathTuple> found = new ArrayList<>();
    this.methodPathPairs.forEach(pair -> {
      if(pair.getMethod() == method){
        found.add(pair);
      }
    });

    if(found.isEmpty()) {
      return null;
    }

    if(found.size() == 1 && wantSingleGetRef) {
      return found.get(0).getPath();
    }

    if(found.size() > 1 && wantSingleGetRef) {
      throw new IllegalArgumentException("Wanted single reference but multiple matched.");
    }

    String returnPath = null;

    for(MethodPathTuple pair : found){
      final Path path = this.swagger.getPath(pair.getPath());
      final Operation op = path.getOperationMap().get(pair.getMethod());

      final String[] pathParts = pair.getPath().split("/\\//");
      final String lastPathPart = pathParts[pathParts.length-1];
      final boolean isLastPathPartId = lastPathPart.matches("/[{}]/");

      final int paramCount = op.getParameters().size();
      final boolean isArrayResponse = SwaggerUtil.OperationC.doesHaveArrayResponse(op);

      if(wantSingleGetRef && isLastPathPartId && !isArrayResponse){
        returnPath = pair.getPath();
      }else if(wantSingleGetRef && paramCount == 0 && !isArrayResponse) {
        returnPath = pair.getPath();
      }

      if(!wantSingleGetRef && (paramCount > 0 || isArrayResponse)){
        returnPath = pair.getPath();
      }
    }

    return returnPath;
  }

  public void addMethodPath(final HttpMethod method, final String path){
    this.methodPathPairs.add(new MethodPathTuple(path, method));
  }

  public Swagger getSwagger() {
    return swagger;
  }

  public String getPackageName() {
    return packageName;
  }

  @Override()
  public String getClassName() {
    return className;
  }

  public String getAnchor() {
    return anchor;
  }

  public ModelGen getModelGen() {
    return modelGen;
  }

  public String getServiceName() {
    return serviceName;
  }

  public TestGen getTestGen() {
    return testGen;
  }

  public DataClass getDataClass() {
    return dataClass;
  }

  private class MethodPathTuple {
    private String path;
    private HttpMethod method;

    public MethodPathTuple(final String path, final HttpMethod method) {
      this.path = path;
      this.method = method;
    }

    public String getPath() {
      return path;
    }

    public HttpMethod getMethod() {
      return method;
    }
  }
}
