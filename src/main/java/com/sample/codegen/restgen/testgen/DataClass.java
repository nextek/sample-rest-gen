package com.sample.codegen.restgen.testgen;

import com.sample.codegen.lang.java.LClass;
import com.sample.codegen.restgen.ModelGen;
import com.sample.codegen.restgen.TestGen;

import io.swagger.models.Swagger;

public class DataClass extends LClass {
  private Swagger swagger;
  private String packageName;
  private String className;
  private String anchor;
  private ModelGen modelGen;
  private String serviceName;
  private TestGen testGen;

  public DataClass(final Swagger swagger, final String packageName, final String className,
                   final String anchor, final ModelGen modelGen, final String serviceName,
                   final TestGen testGen) {
    super(className);
    this.swagger = swagger;
    this.packageName = packageName;
    this.className = className;
    this.anchor = anchor;
    this.modelGen = modelGen;
    this.serviceName = serviceName;
    this.testGen = testGen;

    prepare();
  }

  public void prepare(){
    this.setPkgName(this.packageName);
  }

  public void build(){

  }

  public String getPackageName() {
    return packageName;
  }

  public String getClassName() {
    return className;
  }

  public String getAnchor() {
    return anchor;
  }

  public ModelGen getModelGen() {
    return modelGen;
  }

  public String getServiceName() {
    return serviceName;
  }

  public TestGen getTestGen() {
    return testGen;
  }

  public Swagger getSwagger() {
    return swagger;
  }
}
