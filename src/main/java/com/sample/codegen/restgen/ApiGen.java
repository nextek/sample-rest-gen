package com.sample.codegen.restgen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.sample.codegen.SwaggerUtil;
import com.sample.codegen.Util;
import com.sample.codegen.config.Configuration;
import com.sample.codegen.restgen.apigen.ApiClass;
import com.sample.codegen.restgen.apigen.TxnContextClass;
import com.sample.codegen.restgen.modelgen.ModelClass;
import com.sample.codegen.restgen.modelgen.ModelClassSet;
import com.sample.codegen.restgen.modelgen.RequestClass;
import com.sample.codegen.restgen.servicegen.ServiceClassSet;
import com.sample.codegen.restgen.validationgen.ValidateClass;

import io.swagger.models.HttpMethod;
import io.swagger.models.Operation;
import io.swagger.models.Path;
import io.swagger.models.Swagger;

public class ApiGen {
  private Swagger         swagger;
  private Configuration   config;
  private ModelGen        modelGen;
  private ServiceGen      serviceGen;
  private ValidateGen     validateGen;
  private TxnContextClass txnContextClass;
  private List<ApiClass>        apiClasses    = new ArrayList<ApiClass>();
  private Map<String, ApiClass> path2ApiClass = new HashMap<>();

  public ApiGen(final Swagger swagger, final Configuration configuration, final ModelGen modelGen,
                final ServiceGen serviceGen, final ValidateGen validateGen) {
    this.swagger     = swagger;
    this.config      = configuration;
    this.modelGen    = modelGen;
    this.serviceGen  = serviceGen;
    this.validateGen = validateGen;
    this.txnContextClass = new TxnContextClass();
    prepare();
  }

  public Swagger        getSwagger() {
    return swagger;
  }
  public Configuration  getConfig() {
    return config;
  }
  public ModelGen       getModelGen() {
    return modelGen;
  }
  public ServiceGen     getServiceGen()  {
    return serviceGen;
  }
  public ValidateGen    getValidateGen() {
    return validateGen;
  }
  public List<ApiClass> getApiClasses() {
    return this.apiClasses;
  }

  protected void prepare() {
    createApiClasses();
    for(ApiClass apiClass : getApiClasses()) {
      apiClass.prepare();
    }
  }

  protected ApiClass getApiClassAtPath(String pathString) {
    final String classPathId = NamingUtil.getClass(pathString);
    final String pkgName   = "com.sample.soa." + config.getSvcPkgToken() + ".api";
    final String className = classPathId + "Api";
    final String fullName = pkgName + "." + className;
    
    final ApiClass cl = this.path2ApiClass.get(fullName);
    if(cl != null) {
      return cl;
    }

    String basePath = Util.getBasePath(pathString);
    
    //System.err.println("creating ApiClass");
    final ApiClass apiClass = new ApiClass(getSwagger(), pkgName, className, getModelGen(),
                                           basePath, classPathId, txnContextClass);
                                             
    this.apiClasses.add(apiClass);
    this.path2ApiClass.put(fullName, apiClass);
    return apiClass;
  }
  
  protected void createApiClasses() {
    final Map<String, Path> pathsMap = swagger.getPaths();

    //System.err.println("createApiClasses");

    PATH_LOOP:
    for(Map.Entry<String, Path> entry : pathsMap.entrySet()) {
      //System.out.println("  Key = " + entry.getKey() + ", Value = " + entry.getValue());

      final String pathString = entry.getKey();
      final Path   path       = entry.getValue();
      final Map<String, Object> pathVendorExtList = path.getVendorExtensions();

      if(pathVendorExtList.get("x-gen-ignore") != null) {
        System.out.println("Ignoring path as instructed : " + pathString);
        continue PATH_LOOP;
      }

      final ServiceClassSet serviceClassSet = getServiceGen().getServiceClassSetByPath(pathString);

      final Map<HttpMethod, Operation> operationMap = path.getOperationMap();
      final TreeMap<HttpMethod, Operation> sortedOperationMap = new TreeMap<>(
         new Comparator<HttpMethod>() {
           @Override
           public int compare(HttpMethod o1, HttpMethod o2) {
             return o1.name().compareTo(o2.name());
           }
         });
      sortedOperationMap.putAll(operationMap);
      
      OPERATION_LOOP:
      for(Map.Entry<HttpMethod, Operation> entryB : sortedOperationMap.entrySet()) {
        final HttpMethod method     = entryB.getKey();
        final String methodName     = method.name();
        final Operation  operation  = entryB.getValue();
        final Map<String, Object> operVendorExtList = operation.getVendorExtensions();

        if(operVendorExtList.get("x-gen-ignore") != null) {
          System.out.println("Ignoring operation as instructed : " + pathString + " "
                             + methodName);
          continue OPERATION_LOOP;
        }
        //System.err.println("      operation " + methodName);

        final ModelClassSet modelClassSet = getModelGen().getModelClassSet(pathString, methodName);
        final ValidateClass validateClass = getValidateGen().getValidateClass(pathString,
                                                                              methodName);
        if(validateClass == null) {
          throw new RuntimeException("couldn't find validateClass for pathString: " + pathString
                                     + ", methodName: " + methodName);
        }

        ApiClass apiClass = this.getApiClassAtPath(pathString);
        apiClass.addApiMethod(pathString, method, serviceClassSet, validateClass, modelClassSet);
      }
    }
    
    Comparator<ApiClass> classCompare =
      new Comparator<ApiClass>() {
        public int compare(final ApiClass a, final ApiClass b) {
          return a.getFullName().compareTo(b.getFullName());
        }
      };
    Collections.sort(this.apiClasses, classCompare);
  }

  public void generate(final String outdir, final String pkgRoot) {
    //System.err.println("Generating api output code to dir " + outdir);

    final Map<String, ApiClass> pkg2classMap = new HashMap<String, ApiClass>();
    for(ApiClass apiClass : getApiClasses()) {
      //System.err.println("generate a requestClass");
      apiClass.generate(outdir, pkgRoot);
      pkg2classMap.put(apiClass.getPkgName(), apiClass);
    }

    pkg2classMap.forEach((pkgName, apiClass) -> {
        final List<String> description = new ArrayList<String>();
        description.add("This package contains classes representing REST requests at the");
        description.add("URL path " + apiClass.getBasePath() + ".");
        PackageInfoGen.getInstance().add(pkgName, pkgRoot, description);
      });

    this.txnContextClass.generate(outdir, pkgRoot);

    final List<String> description = new ArrayList<String>();
    description.add("This package contains classes related to context data.");
    PackageInfoGen.getInstance().add(pkgRoot + ".api.context", pkgRoot, description);
  }

}

