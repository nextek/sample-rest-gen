package com.sample.codegen.restgen.externalgen;

import java.util.ArrayList;
import java.util.List;

import com.sample.codegen.Util;
import com.sample.codegen.config.Configuration;
import com.sample.codegen.config.Configuration.HysterixInfo;
import com.sample.codegen.lang.java.AccessModifier;
import com.sample.codegen.lang.java.Field;
import com.sample.codegen.lang.java.LClass;
import com.sample.codegen.lang.java.Method;
import com.sample.codegen.lang.java.SimpleType;

public class ExternalClass extends LClass {
  private HysterixInfo hysterixInfo;

  public ExternalClass(final String packageName, final String className,
                       final HysterixInfo hysterixInfo) {
    super(className);
    this.setPkgName(packageName);
    this.hysterixInfo = hysterixInfo;
    init();
  }

  private void init() {
    this.setJavaDocAuthor(Configuration.getConfig().getJavaDocAuthor());
    this.setJavaDocVersion(Configuration.getConfig().getJavaDocVersion());
    this.addJavaDocDescLine("Location of custom or more complex input message validation.");

    LClass parentClass = new LClass("TenacityCommand<String>");
    parentClass.setPkgName("com.yammer.tenacity.core");
    this.setParentClass(parentClass);

    this.addImportRef("com.yammer.tenacity.core.TenacityCommand");
    this.addImportRef("org.glassfish.jersey.client.JerseyClientBuilder");
    this.addImportRef("com.sample.soa." + Configuration.getConfig().getSvcPkgToken()
                      + ".config.ExternalDependencyKeys");
    this.addImportRef("com.sample.soa.hystrix.SOAHystrixClientFilter");
    this.addImportRef("com.yammer.tenacity.core.http.TenacityJerseyClientBuilder");

    String commandName = hysterixInfo.getCommandName();

    final Method constr = this.createMethod(this.getClassName(), AccessModifier.PUBLIC);
    String constrBody = "super(ExternalDependencyKeys." + commandName + ");" + Util.nl()
                      + "final Client client = new JerseyClientBuilder().build();" + Util.nl()
                      + "this.tenacityClient = TenacityJerseyClientBuilder" + Util.nl()
                      + "  .builder(ExternalDependencyKeys." + commandName + ")" + Util.nl()
                      + "  .build(client).register(new SOAHystrixClientFilter());";
    constr.addJavaDocDescLine("Constructor.");
    constr.setBody(constrBody);
    constr.setReturnType(null);
    this.addMethod(constr);

    final Field tenClientFld = this.createField("tenacityClient", AccessModifier.PRIVATE);
    tenClientFld.setType(new SimpleType("Client", "javax.ws.rs.client.Client"));
    this.addField(tenClientFld);

    final Method runMeth = this.createMethod("run", AccessModifier.PROTECTED);
    runMeth.addAnnotation("@Override()");
    runMeth.addException("Exception", "possible exception");
    runMeth.addJavaDocDescLine("Makes the call to the external endpoint.");
    runMeth.setReturnType(SimpleType.getStringType());
    runMeth.setJavaDocReturnDesc("a result String.");

    String runBody =
        "// Developer has to implement the logic here to make the external call." + Util.nl()
      + "// Use tenacityClient." + Util.nl()
      + "// Example:"            + Util.nl()
      + "// Response response = tenacityClient.target(\"config-file-end-point\").request.get();"
      + Util.nl()
      + "return null;";
    runMeth.setBody(runBody);
    this.addMethod(runMeth);

    final Method getFallbackMeth = this.createMethod("getFallback", AccessModifier.PUBLIC);
    getFallbackMeth.addAnnotation("@Override()");
    getFallbackMeth.setReturnType(SimpleType.getStringType());
    getFallbackMeth.addJavaDocDescLine("Fallback handling in case main run method fails.");
    getFallbackMeth.setJavaDocReturnDesc("an error message string.");
    String fallbackBody =
      "// Developer has to implement this with the logic/behavior of the call failure due to"
      + Util.nl()
      + "// timeout or other reason.  This method is invoked in the moment of failure of the run()"
      + Util.nl()
      + "// method." + Util.nl()
      + "// Example: a null could be returned. Or perhaps a String saying that the \"data is not"
      + Util.nl()
      + "// available at this time" + Util.nl()
      + "// return \"Points information is not available at this time.\"" + Util.nl()
      + Util.nl()
      + "return null;";

    getFallbackMeth.setBody(fallbackBody);
    this.addMethod(getFallbackMeth);
  }

  public void generate(final String outdir, final String pkgRoot) {
    //System.err.println("generate a ValidateClass");
    final LClass.GenerationOptions options = new LClass.GenerationOptions();
    options.ensureBlankLineBeforeFldAnnotations = true;
    options.verticallyLineUpFields              = true;
    //disabling sort. The order should be based on the endpoints and not alphabetical.
    options.sortMethods                         = false;
    final String classTxt = getClassTxt(options);
    final String outPath  = getOutPath(outdir, pkgRoot);
    Util.writeGeneratedFile(outPath, classTxt);
  }
}
