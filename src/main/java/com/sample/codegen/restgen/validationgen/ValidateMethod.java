package com.sample.codegen.restgen.validationgen;

import java.util.ArrayList;
import java.util.List;

import com.sample.codegen.Util;
import com.sample.codegen.lang.java.LClass;
import com.sample.codegen.lang.java.Method;
import com.sample.codegen.lang.java.Param;
import com.sample.codegen.lang.java.SimpleType;
import com.sample.codegen.lang.java.Type;
import com.sample.codegen.restgen.ParamCat;
import com.sample.codegen.restgen.apigen.TxnContextClass;
import com.sample.codegen.restgen.modelgen.ModelClassSet;
import com.sample.codegen.restgen.modelgen.RequestClass;

public class ValidateMethod extends Method {
  private String        pathString;
  private String        operMethodName;
  private ModelClassSet modelClassSet;
  private TxnContextClass txnContextClass;

  public ValidateMethod(final Method baseMethod, final String pathString,
                        final String operMethodName, final ModelClassSet modelClassSet,
                        final TxnContextClass txnContextClass) {
    super(baseMethod);
    this.pathString     = pathString;
    this.operMethodName = operMethodName;
    this.modelClassSet  = modelClassSet;
    this.txnContextClass = txnContextClass;

    init();
  }

  public String        getPathString() {
    return pathString;
  }
  public String        getOperMethodName() {
    return operMethodName;
  }
  public ModelClassSet getModelClassSet()  {
    return modelClassSet;
  }

  public void init() {
    // nothing to do yet
    //System.err.println("validateMethod.setBody");
    this.setIsStatic(true);
    this.setReturnType(SimpleType.getVoidType());
    this.addJavaDocDescLine("Performs message validation for " + this.getOperMethodName());
    this.addJavaDocDescLine(" on " + getPathString() + ".");
    this.setBody("// put any needed custom validation here");

    LClass otherRequestClass = getModelClassSet().getRequestClass();
    if(otherRequestClass != null) {
      final String paramName
        = Util.lcFirstOnly(Util.abbreviateIdentifier(otherRequestClass.getClassName()));
      final Param param = new Param(paramName, new SimpleType(otherRequestClass));
      param.setIsFinal(true);
      this.addParam(param);
    }

    LClass bodyClass = getModelClassSet().getBodyRequestClass();
    if(bodyClass != null) {
      Type bodyType = getModelClassSet().getBodyRequestType();
      final String paramName
        = Util.lcFirstOnly(Util.abbreviateIdentifier(bodyClass.getClassName()));
      final Param param = new Param(paramName, bodyType);
      param.setIsFinal(true);
      this.addParam(param);
    }

    final Param contextParam = new Param("txnContext", new SimpleType(this.txnContextClass));
    contextParam.setIsFinal(true);
    contextParam.setLabel("context");
    this.addParam(contextParam);
  }

  public static String createMethodName(final String pathString, final String operMethodName) {
    final String lastResource = Util.getLastResource(pathString);
    final String lastPathPart = Util.getLastPathPart(pathString);
    String funcName = "assertValid" + Util.ucFirst(operMethodName)
                                    + Util.ucFirst(lastResource);
    if(! lastPathPart.equals(lastResource)) {
      funcName = funcName + Util.ucFirst(lastPathPart);
    }
    return funcName;
  }

  public String getFunctionCall(final String selfName, final Method getResponseMethod,
                                final int maxLineLen, final String txnContextVar) {

    final List<String> arguments = new ArrayList<String>();

    for(ParamCat paramCat : ParamCat.getValues()) {
      final Param param = getResponseMethod.getParamByLabel(paramCat.name());
      if(param == null) {
        continue;  // endpoint must not use this type of param cat
      }
      arguments.add(param.getParamName());
    }
    arguments.add(txnContextVar);

    return super.getFunctionCall(arguments, selfName, null, null, null, maxLineLen);
  }

}

