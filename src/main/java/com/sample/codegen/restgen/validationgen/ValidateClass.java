package com.sample.codegen.restgen.validationgen;

import com.sample.codegen.Util;
import com.sample.codegen.config.Configuration;
import com.sample.codegen.lang.java.AccessModifier;
import com.sample.codegen.lang.java.Field;
import com.sample.codegen.lang.java.LClass;
import com.sample.codegen.lang.java.Method;
import com.sample.codegen.restgen.apigen.TxnContextClass;
import com.sample.codegen.restgen.modelgen.ModelClassSet;

public class ValidateClass extends LClass {
  public ValidateClass(final String packageName, final String className) {
    super(className);
    this.setPkgName(packageName);
    init();
  }

  private void init() {
    //this.addImportRef("com.sample.soa.common.exception.ValidationException");
    this.setIsFinaLClass(true);
    this.setJavaDocAuthor(Configuration.getConfig().getJavaDocAuthor());
    this.setJavaDocVersion(Configuration.getConfig().getJavaDocVersion());
    this.addJavaDocDescLine("Location of custom or more complex input message validation.");

    final Method privateConst = new Method(this.getClassName(), AccessModifier.PRIVATE, 100, 2);
    privateConst.addJavaDocDescLine("Constructor.");
    privateConst.setBody("");
    privateConst.setReturnType(null);
    this.addMethod(privateConst);
  }

  public ValidateMethod createMethod(final String pathString, final String operMethodName,
                                     final ModelClassSet modelClassSet,
                                     final TxnContextClass txnContextClass) {
    final String methodName = ValidateMethod.createMethodName(pathString, operMethodName);
    final Method baseValidateMethod = createMethod(methodName, AccessModifier.PUBLIC);
    final ValidateMethod validateMethod = new ValidateMethod(baseValidateMethod, pathString,
                                                             operMethodName, modelClassSet,
                                                             txnContextClass);
    return validateMethod;
  }

  public void generate(final String outdir, final String pkgRoot) {
    if(this.getNumNonConstructorMethods() == 0) {
      // no reason to generate service class with no methods
      // this can happen when endpoints define x-gen-ignore vendor extension
      return;
    }

    //System.err.println("generate a ValidateClass");
    final LClass.GenerationOptions options = new LClass.GenerationOptions();
    options.ensureBlankLineBeforeFldAnnotations = true;
    options.verticallyLineUpFields              = true;
    //disabling sort. The order should be based on the endpoints and not alphabetical.
    options.sortMethods                         = false;
    final String classTxt = getClassTxt(options);
    final String outPath  = getOutPath(outdir, pkgRoot);
    Util.writeGeneratedFile(outPath, classTxt);
  }
}
