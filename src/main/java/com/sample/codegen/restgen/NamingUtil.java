package com.sample.codegen.restgen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sample.codegen.Util;

public class NamingUtil {
  private static final int START_PATH_TOKEN_CNT = 1;
  private static Map<String, PathTuple> stringPathTupleMap = new HashMap<>();

  public static void registerPath(String path) {
    final String realPath = getRealPath(path);
    if(stringPathTupleMap.containsKey(realPath)){
      return; //don't double add
    }

    PathTuple tuple = new PathTuple();
    tuple.path = path;
    tuple.classString = getOptomisticName(path, 0);
    tuple.originalClassString = getOptomisticName(path, 0);

    stringPathTupleMap.put(realPath, tuple);

    process();
  }

  public static String getClass(String path) {
    final String realPath = getRealPath(path);

    if(stringPathTupleMap.containsKey(realPath)) {
      return stringPathTupleMap.get(realPath).classString;
    }else{
      throw new IllegalArgumentException("Path '" + path + "' does not exist, or has not been registered.");
    }
  }


  private static void process() {
    for(String path : stringPathTupleMap.keySet()){
      PathTuple tuple = stringPathTupleMap.get(path);
      // this is where java REALLY needs LINQ
      List<PathTuple> matchingTuples = selectByClassname(tuple.classString);

      // if we find more than 1 we need to fall back to less optimistic naming
      if(matchingTuples.size() > 1) {
        for(PathTuple matchTuple : matchingTuples){
          matchTuple.collisions++;
          try {
            matchTuple.classString = getOptomisticName(matchTuple.path, matchTuple.collisions);
          }catch(ArrayIndexOutOfBoundsException ex) {
            matchTuple.classString = getFallBackName(matchTuple.path, matchTuple.collisions);
          }
        }
      }
    }
  }

  private static List<PathTuple> selectByClassname(String classString) {
    ArrayList<PathTuple> matches = new ArrayList<>(1);


    for(String path : stringPathTupleMap.keySet()) {
      PathTuple tuple = stringPathTupleMap.get(path);

      if(tuple.classString.equals(classString)){
        matches.add(tuple);
      }
    }

    return matches;
  }


  private static String getRealPath(String path) {
    String[] pathPartsRaw = path.split("/");

    if(pathPartsRaw[pathPartsRaw.length - 1].matches(".*[{}].*")){
      pathPartsRaw = Arrays.copyOf(pathPartsRaw, pathPartsRaw.length - 1);
    }

    return String.join("/", pathPartsRaw);
  }


  private static String getOptomisticName(String path, int collisions){
    final String[] pathPartsRaw = path.split("/");
    final List<String> pathParts = new ArrayList<>();

    for(String part : pathPartsRaw) {
      if(part.length() == 0) {
        continue;
      }
      if(part.matches(".*[{}].*")) {
        continue;
      }
      pathParts.add(part.toLowerCase());
    }

    int pathPartsSize = pathParts.size();
    List<String> nameParts = new ArrayList<>();

    if(pathPartsSize == 1) {
      nameParts.add(Util.ucFirst(pathParts.get(0)));
    } else {
      for(int i = START_PATH_TOKEN_CNT + collisions; i > 0; i--){
        nameParts.add(Util.ucFirst(pathParts.get(pathPartsSize - i)));
      }
    }

    return String.join("", nameParts);
  }

  private static String getFallBackName(String path, int collisions){
    final String[] pathPartsRaw = path.split("/");
    final List<String> pathParts = new ArrayList<>();

    for(String part : pathPartsRaw) {
      if(part.length() == 0) {
        continue;
      }
      if(part.matches(".*[{}].*")) {
        pathParts.add(part.toLowerCase().replace("{", "").replace("}", ""));
      }
      pathParts.add(part.toLowerCase());
    }

    int pathPartsSize = pathParts.size();
    List<String> nameParts = new ArrayList<>();

    if(pathPartsSize == 1) {
      nameParts.add(Util.ucFirst(pathParts.get(0)));
    } else {
      for(int i = START_PATH_TOKEN_CNT + collisions; i > 0; i--){
        nameParts.add(Util.ucFirst(pathParts.get(pathPartsSize - i)));
      }
    }

    return String.join("", nameParts);
  }

  private static class PathTuple {
    String path;
    String classString;
    String originalClassString;
    int collisions = 0;
  }
}
