package com.sample.codegen.restgen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sample.codegen.Util;
import com.sample.codegen.config.Configuration;
import com.sample.codegen.lang.java.LClass;
import com.sample.codegen.restgen.testgen.DataClass;
import com.sample.codegen.restgen.testgen.TestResourceFunctionClass;

import io.swagger.models.HttpMethod;
import io.swagger.models.Operation;
import io.swagger.models.Path;
import io.swagger.models.Swagger;

public class TestGen {
  private Swagger swagger;
  private Configuration config;

  private ApiGen apiGen;
  private ModelGen modelGen;
  private ServiceGen serviceGen;
  private ValidateGen validateGen;

  private List<LClass> resourceClasses = new ArrayList<>();
  private List<LClass> dataClasses = new ArrayList<>();

  public TestGen(final Swagger swagger, final Configuration config, final ApiGen apiGen,
                 final ModelGen modelGen, final ServiceGen serviceGen,
                 final ValidateGen validateGen) {
    this.swagger = swagger;
    this.config = config;
    this.apiGen = apiGen;
    this.modelGen = modelGen;
    this.serviceGen = serviceGen;
    this.validateGen = validateGen;

    this.prepare();
  }

  private void prepare() {
    this.createResourceClasses();
  }

  private void createResourceClasses() {
    final HashMap<String, List<String>> anchor2pathMap = new HashMap<>();
    this.swagger.getPaths().forEach((path, pathObj) -> {
        final String anchor = this.getAnchor(path);

        if(anchor2pathMap.containsKey(anchor)){
          anchor2pathMap.get(anchor).add(path);
        }else{
          final ArrayList<String> paths = new ArrayList<>();
          paths.add(path);

          anchor2pathMap.put(anchor, paths);
        }
      });

    anchor2pathMap.forEach((anchor, paths) ->{
        final String packageName = getResourcePackageName(anchor);
        final String className = getResourceClassName(anchor);

        final DataClass dataClass = new DataClass(this.swagger, packageName, className, anchor,
                                                  modelGen, config.getServiceName(), this);
        dataClasses.add(dataClass);

        final TestResourceFunctionClass resourceFunctionClass
          = new TestResourceFunctionClass(this.swagger, packageName, className, anchor, modelGen,
                                          config.getServiceName(), this, dataClass);
        resourceClasses.add(resourceFunctionClass);

        paths.forEach(pathString -> {
            final Path path = swagger.getPath(pathString);
            final Map<HttpMethod, Operation> ops = path.getOperationMap();

            ops.forEach((method, operation) -> {
                resourceFunctionClass.addMethodPath(method, pathString);
              });
          });
      });
  }

  private List<String> getFilteredPathParts(final String path){
    final String[] parts = path.split("/");
    final ArrayList<String> filtered = new ArrayList<>(parts.length);

    for(String part : parts){
      if(part.isEmpty()) {
        continue;
      }

      final String value = part.replaceAll("[{}]", "").toLowerCase();
      filtered.add(value);
    }
    return filtered;
  }

  private String getResourceClassName(final String anchor) {
    final List<String> pathParts = getFilteredPathParts(anchor);
    final String last = Util.ucFirst(pathParts.get(pathParts.size() - 1));

    return last + "ResourceFunctionalTest";
  }

  private String getResourcePackageName(final String anchor) {
    final List<String> pathParts = getFilteredPathParts(anchor);
    final String last = String.join(".", pathParts.toArray(new String[]{}));

    return "com.sample.soa." + this.config.getServiceName() + ".resource." + last;
  }

  /**
   * Removes the last element of the path if it's a variable. Otherwise return the path
   * @param  path path
   * @return the anchor string
   */
  private String getAnchor(final String path) {
    String[] parts = path.split("/");
    if(parts[parts.length-1].matches("/[{}]/")) {
      final String[] tmp = new String[parts.length-1];
      for(int i = 0; i < tmp.length; i++){
        tmp[i] = parts[i];
      }
      parts = tmp;

      // call it recursively incase there are multiple variables on the end of the path
      return getAnchor(String.join("/", parts));
    }
    return String.join("/", parts);
  }

  public List<LClass> getDataClasses() {
    return dataClasses;
  }

  public List<LClass> getResourceClasses() {
    return resourceClasses;
  }

  public void generate(final String outdir, final String pkgRoot) {
    final LClass.GenerationOptions options = new LClass.GenerationOptions();
    options.ensureBlankLineBeforeFldAnnotations = true;
    options.verticallyLineUpFields              = true;

    this.dataClasses.forEach(dataClass -> {
      Util.writeGeneratedFile(dataClass.getOutPath(outdir, pkgRoot),
                                 dataClass.getClassTxt(options));
    });

    this.resourceClasses.forEach(resourceClass -> {
      Util.writeGeneratedFile(resourceClass.getOutPath(outdir, pkgRoot),
                                 resourceClass.getClassTxt(options));
    });
  }
}
