package com.sample.codegen.restgen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sample.codegen.Util;

public class PackageInfoGen {
  private static PackageInfoGen instance;

  List<PackageInfo> packageInfos;

  private PackageInfoGen() {
    packageInfos = new ArrayList<>();
  }

  public void add(final String packageName, final String packageRoot,
                  final List<String> descriptionLines) {
    packageInfos.add(new PackageInfo(packageName, packageRoot, descriptionLines));
  }

  public static PackageInfoGen getInstance(){
    if(PackageInfoGen.instance == null){
      PackageInfoGen.instance = new PackageInfoGen();
    }

    return PackageInfoGen.instance;
  }

  public void generate(final String outdir){
    Map<String, Boolean> packageNamesDone = new HashMap<>();

    for(PackageInfo pi : this.packageInfos) {
      Boolean hasDonePackage = packageNamesDone.get(pi.getPackageName());
      if(hasDonePackage != null && hasDonePackage == true) {
        continue;
      }

      final StringBuilder output = new StringBuilder();
      final String subdir = Util.pkg2resourceSubdir(pi.getPackageName(), pi.getPackageRoot());
      final String classDir = outdir + "/" + subdir;
      output.append("/**" + Util.nl() + " * ");
      output.append(String.join(Util.nl() + " * ", pi.getDescriptionLines()));
      output.append(Util.nl() + " */" + Util.nl() + "package ");
      output.append(pi.getPackageName());
      output.append(";");

      Util.writeGeneratedFile(classDir + "/package-info.java", output.toString());
      packageNamesDone.put(pi.getPackageName(), true);
    }
  }

  private class PackageInfo{
    private String packageName;
    private String packageRoot;
    private List<String> descriptionLines;

    public PackageInfo(final String packageName, final String packageRoot,
                       final List<String> descriptionLines) {
      this.packageName = packageName;
      this.packageRoot = packageRoot;
      this.descriptionLines = descriptionLines;
    }

    public String getPackageName() {
      return packageName;
    }

    public String getPackageRoot() {
      return packageRoot;
    }

    public List<String> getDescriptionLines() {
      return descriptionLines;
    }
  }
}
