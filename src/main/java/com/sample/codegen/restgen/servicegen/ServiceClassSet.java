package com.sample.codegen.restgen.servicegen;

import com.sample.codegen.Util;
import com.sample.codegen.config.Configuration;
import com.sample.codegen.lang.java.AccessModifier;
import com.sample.codegen.lang.java.Field;
import com.sample.codegen.lang.java.LClass;
import com.sample.codegen.lang.java.Method;
import com.sample.codegen.restgen.NamingUtil;
import com.sample.codegen.restgen.apigen.TxnContextClass;
import com.sample.codegen.restgen.modelgen.ModelClassSet;

import io.swagger.models.HttpMethod;

public class ServiceClassSet {
  private String   resourceName;
  private String[] paths;

  private LClass   baseClass;
  private LClass   implClass;

  public ServiceClassSet(final String resourceName, final String[] paths) {
    this.resourceName = resourceName;
    this.paths        = paths;

    prepare();
  }

  public String getResourceName() {
    return resourceName;
  }

  private void prepare() {
    final String basePackageName
      = String.format("com.sample.soa.%s.service", Configuration.getConfig().getSvcPkgToken());
    final String baseClassName = NamingUtil.getClass(paths[0]) + "Service";
    final String implPackageName = basePackageName + ".impl";
    final String implClassName = baseClassName + "Impl";

    this.baseClass = new LClass(baseClassName);
    this.implClass = new LClass(implClassName);

    this.baseClass.setPkgName(basePackageName);
    this.baseClass.setIsInterface(true);
    this.baseClass.setJavaDocAuthor(Configuration.getConfig().getJavaDocAuthor());
    this.baseClass.setJavaDocVersion(Configuration.getConfig().getJavaDocVersion());
    this.baseClass.addJavaDocDescLine("Contract for the " + Util.ucFirst(this.resourceName)
                                    + " service.");

    this.implClass.setPkgName(implPackageName);
    this.implClass.setOfInterface(baseClass);
    this.implClass.setJavaDocAuthor(Configuration.getConfig().getJavaDocAuthor());
    this.implClass.setJavaDocVersion(Configuration.getConfig().getJavaDocVersion());
    this.implClass.addJavaDocDescLine("Implementation of the " + Util.ucFirst(this.resourceName)
                                    + " service responsible for business logic.");
    this.implClass.addAnnotation("@Service()");
    this.implClass.addImportRef("org.springframework.stereotype.Service");
  }

  public ServiceMethod createMethod(final HttpMethod httpMethod, final ModelClassSet modelClassSet,
                                    final boolean isArrayResponse,
                                    final TxnContextClass txnContextClass) {
    final ServiceMethod svcMethod = new ServiceMethod(this.resourceName, httpMethod,
                                                      modelClassSet, isArrayResponse,
                                                      txnContextClass);
    return svcMethod;
  }

  public void addMethod(final Method method){
    this.implClass.addMethod(method);
    this.baseClass.addMethod(new Method(method));
  }

  public boolean isMatchingPathString(final String pathString) {
    for(String curPathString : paths) {
      if(curPathString.equals(pathString)) {
        return true;
      }
    }
    return false;
  }

  public void generate(final String outdir, final String pkgRoot) {
    if(this.baseClass.getNumNonConstructorMethods() == 0) {
      // no reason to generate service class with no methods
      // this can happen when endpoints define x-gen-ignore vendor extension
      return;
    }

    if(this.baseClass.getNumMethods() == 1) {
      this.baseClass.addAnnotation("@FunctionalInterface()");
    }

    final LClass.GenerationOptions options = new LClass.GenerationOptions();
    options.ensureBlankLineBeforeFldAnnotations = true;
    options.verticallyLineUpFields              = true;
    
    final String classTxtA = this.baseClass.getClassTxt(options);
    final String outPathA  = this.baseClass.getOutPath(outdir, pkgRoot);
    Util.writeGeneratedFile(outPathA, classTxtA);

    final String classTxtB = this.implClass.getClassTxt(options);
    final String outPathB  = this.implClass.getOutPath(outdir, pkgRoot);
    Util.writeGeneratedFile(outPathB, classTxtB);
  }

  public LClass getBaseClass() {
    return baseClass;
  }

  public LClass getImplClass() {
    return implClass;
  }
}
