package com.sample.codegen.restgen.servicegen;

import java.util.ArrayList;
import java.util.List;

import com.sample.codegen.Util;
import com.sample.codegen.lang.java.AccessModifier;
import com.sample.codegen.lang.java.CompositeType;
import com.sample.codegen.lang.java.Method;
import com.sample.codegen.lang.java.Param;
import com.sample.codegen.lang.java.SimpleType;
import com.sample.codegen.lang.java.Type;
import com.sample.codegen.restgen.ParamCat;
import com.sample.codegen.restgen.apigen.TxnContextClass;
import com.sample.codegen.restgen.modelgen.ModelClass;
import com.sample.codegen.restgen.modelgen.ModelClassSet;
import com.sample.codegen.restgen.modelgen.RequestClass;

import io.swagger.models.HttpMethod;

public class ServiceMethod extends Method {
  private static final String FIND_TOKEN   = "find";
  private static final String GET_TOKEN    = "get";
  private static final String UPDATE_TOKEN = "update";
  private static final String PATCH_TOKEN  = "patch";
  private static final String CREATE_TOKEN = "create";
  private static final String DELETE_TOKEN = "delete";
  
  private String resource;
  private HttpMethod opMethod;
  private ModelClassSet modelClassSet;
  private boolean isArrayResponse;
  private TxnContextClass txnContextClass;

  private String returnVarName;

  // ParamStyle is being dropped off because only "model" is being used.
  public ServiceMethod(final String resourceName, final HttpMethod httpMethod,
                       final ModelClassSet modelClassSet, final boolean isArrayResponse,
                       final TxnContextClass txnContextClass) {
    super(ServiceMethod.getBaseMethod(resourceName, httpMethod, isArrayResponse));

    this.resource = resourceName;
    this.opMethod = httpMethod;
    this.modelClassSet = modelClassSet;
    this.isArrayResponse = isArrayResponse;
    this.txnContextClass = txnContextClass;

    prepare(resourceName, httpMethod, modelClassSet, isArrayResponse);
  }

  private static Method getBaseMethod(final String resourceName, final HttpMethod httpMethod,
                                      final boolean isArrayResponse) {
    String functionName;

    if(isArrayResponse && httpMethod == HttpMethod.GET) {
      functionName = FIND_TOKEN;
    }else if(httpMethod == HttpMethod.GET) {
      functionName = GET_TOKEN;
    }else if(httpMethod == HttpMethod.PUT) {
      functionName = UPDATE_TOKEN;
    }else if(httpMethod == HttpMethod.PATCH) {
      functionName = PATCH_TOKEN;
    }else if(httpMethod == HttpMethod.POST) {
      functionName = CREATE_TOKEN;
    }else if(httpMethod == HttpMethod.DELETE) {
      functionName = DELETE_TOKEN;
    }else{
      throw new IllegalArgumentException("unexpected operation method name of '"
                                         + httpMethod + "'");
    }

    final Method base = new Method(functionName, AccessModifier.PUBLIC, 98, 2);

    return base;
  }

  private void prepare(final String resourceName, final HttpMethod httpMethod,
                       final ModelClassSet modelClassSet, final boolean isArrayResponse) {
    final ModelClass responseClass = modelClassSet.getResponseClass();

    final List<Param> params = new ArrayList<>();
    final RequestClass otherRequestClass = modelClassSet.getRequestClass();
    final ModelClass   bodyRequestClass  = modelClassSet.getBodyRequestClass();
    final Type         bodyRequestType   = modelClassSet.getBodyRequestType();
    
    String bodyRequestPackage;
    String bodyRequestClassName;
    String bodyRequestVarName;
    String otherRequestPackage;
    String otherRequestClassName;
    String otherRequestVarName;

    if(otherRequestClass != null){
      otherRequestClassName = otherRequestClass.getClassName();
      otherRequestVarName = Util.lcFirstOnly(otherRequestClassName);
      final Param p = new Param(otherRequestVarName, new SimpleType(otherRequestClass));
      p.setIsFinal(true);
      p.setLabel("other");
      params.add(p);
    }
    if(bodyRequestType != null){
      bodyRequestClassName = bodyRequestClass.getClassName();
      bodyRequestVarName = Util.lcFirstOnly(bodyRequestClassName);
      final Param p = new Param(bodyRequestVarName, bodyRequestType);
      p.setIsFinal(true);
      p.setLabel("body");
      params.add(p);
    }

    final Param contextParam = new Param("txnContext", new SimpleType(this.txnContextClass));
    contextParam.setIsFinal(true);
    contextParam.setLabel("context");
    params.add(contextParam);

    String functionName;
    Type returnType;
    String returnDescJavaDoc = "";
    String responsePackage   = "";
    String responseClassName = "";

    if(responseClass != null) {
      responsePackage   = responseClass.getPkgName();
      responseClassName = responseClass.getClassName();
      returnType        = responseClass.getType();
      returnDescJavaDoc = "a " + responseClassName + " object";
    } else {
      returnType = SimpleType.getVoidType();
    }

    String newReturnType = responseClassName;
    if(isArrayResponse && httpMethod == HttpMethod.GET) {
      functionName  = FIND_TOKEN;
      returnType    = new CompositeType(SimpleType.getListType(),
                                        responseClass.getType());
      newReturnType = "ArrayList<>";
      returnDescJavaDoc = "a list of " + responseClassName + " objects";
      addImportRef("java.util.ArrayList");
    }else if(httpMethod == HttpMethod.GET) {
      functionName = GET_TOKEN;
    }else if(httpMethod == HttpMethod.PUT) {
      functionName = UPDATE_TOKEN;
    }else if(httpMethod == HttpMethod.PATCH) {
      functionName = PATCH_TOKEN;
    }else if(httpMethod == HttpMethod.POST) {
      functionName = CREATE_TOKEN;
    }else if(httpMethod == HttpMethod.DELETE) {
      functionName = DELETE_TOKEN;
    }else{
      throw new IllegalArgumentException("unexpected operation method name of '"
                                         + httpMethod + "'");
    }

    String body;
    if(responseClass == null) {
      body = "// implement this";
    }else{
      body = "// implement this"                                + Util.nl()
           + "return new " + newReturnType + "();"              + Util.nl();
    }

    this.setBody(body);
    this.setReturnType(returnType);
    this.setJavaDocReturnDesc(returnDescJavaDoc);

    this.addAnnotation("@Override()");

    params.forEach((p) -> this.addParam(p));

    this.addJavaDocDescLine("Performs a " + functionName + " operation on resource "
                                + Util.getLastResource(resourceName) + ".");

    if(responseClass != null){
      this.addImportRef(responsePackage + "." + responseClassName);
    }

    if(responseClass != null){
      if(isArrayResponse) {
        this.returnVarName = Util.lcFirstOnly(Util.pluralizeNoun(responseClass.getClassName()));
      }else{
        this.returnVarName = Util.lcFirstOnly(responseClass.getClassName());
      }
    }
  }

  //@Override different signature, same name
  public String getFunctionCall(final String selfName, final Method resourceFunction,
                                final int maxLineLen, final String txnContextVar) {
    final List<String> arguments = new ArrayList<>();
    final ModelClass responseClass = modelClassSet.getResponseClass();

    for(ParamCat paramCat : ParamCat.getValues()){
      final Param p = resourceFunction.getParamByLabel(paramCat.name());
      if(p == null) {
        continue;  // endpoint must not use this type of param cat
      }
      arguments.add(p.getParamName());
    }
    arguments.add(txnContextVar);

    return super.getFunctionCall(arguments, selfName, this.getReturnType(), "final",
                                 this.returnVarName, maxLineLen);
  }

  public String getReturnVarName() {
    return returnVarName;
  }
}
