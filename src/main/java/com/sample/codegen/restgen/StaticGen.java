package com.sample.codegen.restgen;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import com.sample.codegen.Util;
import com.sample.codegen.config.Configuration;
import com.sample.codegen.config.Configuration.HysterixInfo;

import io.swagger.models.Info;
import io.swagger.models.Swagger;

public class StaticGen {
  public enum TmplId {
    CLASS_SVC_APLICATION,
    CLASS_MARIADB_CONFIG_TMPL,    CLASS_COUCHB_CONFIG_TMPL,
    CLASS_APP_CONFIG_TMPL,        CLASS_BUNDLE_CONFIG_TMPL,
    CLASS_EXT_DEP_KEYS_TMPL,
    CLASS_TEST_EXC_TMPL,
    CLASS_ERROR_TYPE_TMPL,        CLASS_ERROR_REG_TMPL,
    CONFIG_APP_YAML_TMPL,         CONFIG_TEST_APP_YAML_TMPL,
    CONFIG_POM_XML_TMPL,          CONFIG_LOGBACK_XML_TMPL,
    PINFO_MAIN_TMPL,              PINFO_CONFIG_TMPL,         PINFO_ENTITY_TMPL,
    PINFO_MODEL_TMPL,             PINFO_REPOSITORY_TMPL,     PINFO_API_TMPL,
    PINFO_SERVICE_TMPL,           PINFO_SERVICE_IMPL_TMPL,   PINFO_VALIDATION_TMPL,
    PINFO_ERRORS_TMPL,            PINFO_ERRORS_CONST_TMPL,   PINFO_ERRORS_EXCEPT_TMPL,
    APP_YAML_TMPL,                APP_YAML2_TMPL,
  };

  private static Map<TmplId, Template> templateMap;
  private static MustacheFactory mustacheFactory = new DefaultMustacheFactory();
  private Swagger       swagger;
  private Configuration configuration;

  static {
    templateMap = new HashMap<>();

    templateMap.put(TmplId.CLASS_SVC_APLICATION,
                    new Template("mustache/static/classes/svc_application.mustache"));

    templateMap.put(TmplId.CLASS_MARIADB_CONFIG_TMPL,
                    new Template("mustache/static/classes/config_mariadb.mustache"));

    templateMap.put(TmplId.CLASS_COUCHB_CONFIG_TMPL,
                    new Template("mustache/static/classes/config_couchbase.mustache"));

    templateMap.put(TmplId.CLASS_APP_CONFIG_TMPL,
                    new Template("mustache/static/classes/app_config.mustache"));
    templateMap.put(TmplId.CLASS_BUNDLE_CONFIG_TMPL,
                    new Template("mustache/static/classes/external_bundle_factory.mustache"));
    templateMap.put(TmplId.CLASS_EXT_DEP_KEYS_TMPL,
                    new Template("mustache/static/classes/ext_depend_keys.mustache"));

    templateMap.put(TmplId.CLASS_TEST_EXC_TMPL,
                    new Template("mustache/static/classes/test_exception.mustache"));
    templateMap.put(TmplId.CLASS_ERROR_TYPE_TMPL,
                    new Template("mustache/static/classes/const_app_err_type.mustache"));
    templateMap.put(TmplId.CLASS_ERROR_REG_TMPL,
                    new Template("mustache/static/classes/error_registration.mustache"));

    templateMap.put(TmplId.CONFIG_POM_XML_TMPL,
                    new Template("mustache/static/config/pom.xml.mustache"));
    templateMap.put(TmplId.CONFIG_LOGBACK_XML_TMPL,
                    new Template("mustache/static/config/logback.xml.mustache"));

    templateMap.put(TmplId.APP_YAML_TMPL,
                    new Template("mustache/static/config/app.yaml.mustache"));
    templateMap.put(TmplId.APP_YAML2_TMPL,
                    new Template("mustache/static/config/app.test.yaml.mustache"));

    templateMap.put(TmplId.PINFO_MAIN_TMPL,
                    new Template("mustache/static/package_info/main.mustache"));
    templateMap.put(TmplId.PINFO_CONFIG_TMPL,
                    new Template("mustache/static/package_info/config.mustache"));
    templateMap.put(TmplId.PINFO_MODEL_TMPL,
                    new Template("mustache/static/package_info/model.mustache"));
    templateMap.put(TmplId.PINFO_REPOSITORY_TMPL,
                    new Template("mustache/static/package_info/repository.mustache"));
    templateMap.put(TmplId.PINFO_API_TMPL,
                    new Template("mustache/static/package_info/api.mustache"));
    templateMap.put(TmplId.PINFO_SERVICE_TMPL,
                    new Template("mustache/static/package_info/service.mustache"));
    templateMap.put(TmplId.PINFO_SERVICE_IMPL_TMPL,
                    new Template("mustache/static/package_info/service.impl.mustache"));
    templateMap.put(TmplId.PINFO_VALIDATION_TMPL,
                    new Template("mustache/static/package_info/validation.mustache"));
    templateMap.put(TmplId.PINFO_ERRORS_TMPL,
                    new Template("mustache/static/package_info/errors.mustache"));
    templateMap.put(TmplId.PINFO_ERRORS_CONST_TMPL,
                    new Template("mustache/static/package_info/errors_const.mustache"));
    templateMap.put(TmplId.PINFO_ERRORS_EXCEPT_TMPL,
                    new Template("mustache/static/package_info/errors_except.mustache"));
  }

  public StaticGen(final Swagger swagger, final Configuration configuration) {
    this.swagger       = swagger;
    this.configuration = configuration;
  }

  public Template getTemplate(final TmplId id) {
    return this.templateMap.get(id);
  }

  protected Map<String, Object> createVariableMap() {
    final String serviceName      = this.configuration.getServiceName();
    final String capServiceName   = Util.ucFirstOnly(serviceName);
    final String packageName      = this.configuration.getConfig().getSvcPkgToken();
    final String classServiceName = this.configuration.getConfig().getSvcClassName();
    final String identifierServiceName = Util.lcFirstOnly(classServiceName);
    final String capIdServiceName   = Util.ucFirstOnly(identifierServiceName);
    final boolean doCheckstyle    = this.configuration.isCheckstylePom();
    final String checkstylePath   = this.configuration.getCheckstylePath();
    final String configClassName  = classServiceName + "Config";

    String errorContext = "";
    final Info info = this.swagger.getInfo();
    if(info != null) {
      final Map<String, Object> getVendorExtensions = info.getVendorExtensions();
      if(getVendorExtensions != null) {
        Object errorContextObj = getVendorExtensions.get("x-error-context");
        if(errorContextObj != null) {
          errorContext = (String)errorContextObj;
        }
      }
    }

    boolean doCouchbase = false;
    if(this.configuration.getDatabase() == Configuration.Database.COUCHBASE) {
      doCouchbase = true;
    }
    boolean doDatabase = false;
    if(this.configuration.getDatabase() != null) {
      doDatabase = true;
    }

    Configuration config = Configuration.getConfig();

    final Map<String, Object> variables = new HashMap<>();
    variables.put("service_name", serviceName);
    variables.put("uc_service_name", capServiceName);
    variables.put("pkg_service_name", packageName);
    variables.put("identifier_service_name", identifierServiceName);
    variables.put("uc_identifier_service_name", capIdServiceName);
    variables.put("class_service_name", classServiceName);
    variables.put("couchbase_pom", doCouchbase);
    variables.put("do_couchbase", doCouchbase);
    variables.put("do_database", doDatabase);
    variables.put("xml_path", checkstylePath);
    variables.put("config_class_name", configClassName);
    variables.put("author", config.getJavaDocAuthor());
    variables.put("version", config.getJavaDocVersion());
    variables.put("parent_version", config.getParentPomVersion());
    variables.put("error_context", errorContext);
    variables.put("is_composite", config.isComposite());

    String swaggerVersion = "";
    String restgenVersion = "";
    if(config.getSwaggerVersion() != null) {
      if(config.getMaskVersions()) {
        swaggerVersion = "***";
      } else {
        swaggerVersion = config.getSwaggerVersion();
      }
    }
    if(config.getReleaseVersion() != null) {
      if(config.getMaskVersions()) {
        restgenVersion = "***";
      } else {
        restgenVersion = config.getReleaseVersion();
      }
    }

    variables.put("swagger_version", swaggerVersion);
    variables.put("restgen_version", restgenVersion);

    String externalFactoryCodeFragment = createExternalFactoryCodeFragment();
    variables.put("external_factory_code_fragment", externalFactoryCodeFragment);

    String hysterix_code = "";
    if(config.isComposite()) {
      List<HysterixInfo> l = config.getHysterixDependencies();
      List<String> lines = new ArrayList<>();
      for(int i = 0; i < l.size(); i++) {
        boolean isLast = (i == l.size() - 1);
        HysterixInfo item = l.get(i);
        lines.add("/**");
        lines.add(" * Enum value for '" + item.getCommandName() + "'.");
        lines.add(" */");
        String line = "  " + item.getCommandName();
        if(! isLast) {
          line += ",";
        }
        lines.add(line);
      }
      variables.put("hysterix_list_code", String.join(Util.nl(), lines));
    }

    if(configuration.getDatabase() != null) {
      switch(configuration.getDatabase()) {
      case COUCHBASE:
        variables.put("db_hostname_var",          "${CB_HOSTNAME}");
        variables.put("db_datastore_var",         "${CB_BUCKETNAME}");
        variables.put("db_username_var",          "${CB_USERNAME}");
        variables.put("db_password_var",          "${CB_BUCKETPASSWORD}");
        variables.put("db_replicacount_var",      "${CB_REPLICACOUNT}");
        variables.put("db_ssl_enabled_var",       "${CB_SSL_ENABLED}");
        variables.put("db_ssl_keystore_file",     "${CB_SSL_KEYSTORE_FILE}");
        variables.put("db_ssl_keystore_password", "${CB_SSL_KEYSTORE_PASSWORD}");
        break;
      case MARIADB:
        variables.put("db_hostname_var",  "${MYSQL_HOSTNAME}");
        variables.put("db_datastore_var", "${MYSQL_DATASOURCE}");
        variables.put("db_username_var",  "${MYSQL_USERNAME}");
        variables.put("db_password_var",  "${MYSQL_PASSWORD}");
        break;
      default:
        throw new RuntimeException("unexpected database");
      }
    } else {
      variables.put("db_hostname_var",  "undefined");
      variables.put("db_datastore_var", "undefined");
      variables.put("db_username_var",  "undefined");
      variables.put("db_password_var",  "undefined");
      variables.put("db_replicacount_var", "undefined");
    }

    // file ./[SVC]Application.java will have checkstyle errors if these import lines aren't
    // sorted correctly
    final List<String> soaImports = new ArrayList<>();
    soaImports.add("import com.sample.soa.config.SOAApplication;");
    soaImports.add("import com.sample.soa.config.ErrorContext;");
    soaImports.add(String.format("import com.sample.soa.%s.config.%s;",
                                    packageName, configClassName));
    soaImports.add(String.format("import com.sample.soa.%s.errors.ErrorsRegistration;",
                                 packageName));
    if(config.isComposite()) {
      soaImports.add("import com.yammer.tenacity.core.bundle.TenacityBundleConfigurationFactory;");
      soaImports.add(String.format("import com.sample.soa.%s.config."
                                   + "ExternalDependencyBundleConfigurationFactory;",
                                   packageName));
    }
    Collections.sort(soaImports);

    variables.put("soa_import_sec", String.join(Util.nl(), soaImports));

    String appConfigImportTxt = "";
    if(config.isComposite() || doCouchbase) {
      appConfigImportTxt = "import javax.validation.constraints.NotNull;" + Util.nl()
        + Util.nl()
        + "import com.fasterxml.jackson.annotation.JsonProperty;" + Util.nl();
    }
    variables.put("app_config_import_sec", appConfigImportTxt);

    // file ./[SVC]Application.java will have checkstyle errors if these import lines aren't
    // sorted correctly
    final List<String> errorsImports = new ArrayList<>();
    errorsImports.add("import com.sample.soa.errors.constants.ErrorCode;");
    errorsImports.add("import com.sample.soa.errors.exceptions.AppException;");
    errorsImports.add(String.format("import com.sample.soa.%s.errors.constants.AppErrorType;",
                                    packageName));
    Collections.sort(errorsImports);

    variables.put("errors_import_sec", String.join(Util.nl(), errorsImports));

    // file ./ErrorRegistration.java will have checkstyle errors if these import lines aren't
    // sorted correctly
    final List<String> errorRegImports = new ArrayList<>();
    errorRegImports.add("import com.sample.soa.errors.constants.ErrorCode;");
    errorRegImports.add("import com.sample.soa.errors.constants.NotificationTypes;");
    errorRegImports.add(String.format("import com.sample.soa.%s.errors.constants.AppErrorType;",
                                    packageName));
    Collections.sort(errorRegImports);
    variables.put("error_reg_import_sec", String.join(Util.nl(), errorRegImports));
    
    return variables;
  }

  protected String createExternalFactoryCodeFragment() {
    List<HysterixInfo> l = Configuration.getConfig().getHysterixDependencies();
    List<String> lines = new ArrayList<>();
    for(HysterixInfo hysterixInfo : l) {
      String commandName = hysterixInfo.getCommandName();
      String line = String.format("    builder.put(ExternalDependencyKeys.%s,",
                                  commandName);
      lines.add(line);
      String line2 = "      configuration.getTenacityConfig());";
      lines.add(line2);
    }
    String txt = String.join("\n", lines);
    return txt;
  }

  protected Map<TmplId, String> getOutputMap(final String outDirArg,
                                             final Map<String, Object> variableMap) {
    final String outDir = outDirArg + "/";
    final String srcDir = outDir + "src/main/java/com/sample/soa/"
                          + variableMap.get("pkg_service_name") + "/";

    final Map<TmplId, String> outputMap = new HashMap<>();
    outputMap.put(TmplId.CLASS_SVC_APLICATION,    srcDir + String.format("%sApplication.java",
                                                           variableMap.get("class_service_name")));

    outputMap.put(TmplId.CLASS_APP_CONFIG_TMPL, srcDir + String.format("config/%s.java",
                                                           variableMap.get("config_class_name")));
    if(Configuration.getConfig().isComposite()) {
      outputMap.put(TmplId.CLASS_BUNDLE_CONFIG_TMPL,
                    srcDir + "/config/ExternalDependencyBundleConfigurationFactory.java");
      outputMap.put(TmplId.CLASS_EXT_DEP_KEYS_TMPL,
                    srcDir + "/config/ExternalDependencyKeys.java");
    }
    outputMap.put(TmplId.CLASS_TEST_EXC_TMPL, srcDir + "errors/exceptions/TestException.java");
    outputMap.put(TmplId.CLASS_ERROR_TYPE_TMPL, srcDir + "errors/constants/AppErrorType.java");
    outputMap.put(TmplId.CLASS_ERROR_REG_TMPL, srcDir + "errors/ErrorsRegistration.java");

    outputMap.put(TmplId.CONFIG_POM_XML_TMPL,     outDir + "pom.xml");
    outputMap.put(TmplId.APP_YAML_TMPL,           outDir + "/src/main/resources/app.yaml");
    outputMap.put(TmplId.APP_YAML2_TMPL,          outDir + "/src/test/resources/app.yaml");

    outputMap.put(TmplId.PINFO_MAIN_TMPL,         srcDir + "/package-info.java");
    outputMap.put(TmplId.PINFO_CONFIG_TMPL,       srcDir + "config/package-info.java");
    outputMap.put(TmplId.PINFO_MODEL_TMPL,        srcDir + "model/package-info.java");
    outputMap.put(TmplId.PINFO_REPOSITORY_TMPL,   srcDir + "repository/package-info.java");
    outputMap.put(TmplId.PINFO_API_TMPL,          srcDir + "api/package-info.java");
    outputMap.put(TmplId.PINFO_SERVICE_TMPL,      srcDir + "service/package-info.java");
    outputMap.put(TmplId.PINFO_SERVICE_IMPL_TMPL, srcDir + "service/impl/package-info.java");
    outputMap.put(TmplId.PINFO_VALIDATION_TMPL,   srcDir + "validation/package-info.java");
    outputMap.put(TmplId.PINFO_ERRORS_TMPL,       srcDir + "errors/package-info.java");
    outputMap.put(TmplId.PINFO_ERRORS_CONST_TMPL, srcDir + "errors/constants/package-info.java");
    outputMap.put(TmplId.PINFO_ERRORS_EXCEPT_TMPL,srcDir + "errors/exceptions/package-info.java");

    if(configuration.getDatabase() != null) {
      switch(configuration.getDatabase()) {
      case COUCHBASE:
        outputMap.put(TmplId.CLASS_COUCHB_CONFIG_TMPL, srcDir + "config/Config.java");
        break;
      case MARIADB:
        outputMap.put(TmplId.CLASS_MARIADB_CONFIG_TMPL, srcDir + "config/Config.java");
        break;
      default:
        throw new RuntimeException("unexpected database");
      }
    }
    return outputMap;
  }

  public void generate(final String outDir) {
    final Map<String, Object> variableMap = createVariableMap();
    final Map<TmplId, String> outputMap   = getOutputMap(outDir, variableMap);

    for(TmplId id : outputMap.keySet()) {
      final Template tmpl  = getTemplate(id);
      final String outPath = outputMap.get(id);
      final String outTxt  = tmpl.getInterpolated(variableMap);
      Util.writeGeneratedFile(outPath, outTxt);
    }
  }

  public static class Template {
    private String apiPath;
    private Mustache mustache;

    public Template(final String apiPath) {
      this.apiPath = apiPath;
      this.mustache = mustacheFactory.compile(apiPath);
    }

    public String getApiPath() {
      return apiPath;
    }

    public Mustache getMustache(){
      return mustache;
    }

    public String getInterpolated(final Map<String, Object> variables) {
      final Writer writer = new StringWriter();
      try {
        this.mustache.execute(writer, variables);
        writer.flush();
      } catch(IOException e) {
        throw new RuntimeException(e);
      }
      return writer.toString();
    }

  }
}
