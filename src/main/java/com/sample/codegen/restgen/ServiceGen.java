package com.sample.codegen.restgen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sample.codegen.Util;
import com.sample.codegen.config.Configuration;
import com.sample.codegen.restgen.servicegen.ServiceClassSet;

import io.swagger.models.Path;
import io.swagger.models.Swagger;

public class ServiceGen {
  private Swagger swagger;
  private Configuration config;
  private List<ServiceClassSet> classSets;

  public ServiceGen(final Swagger swagger, final Configuration config) {
    this.swagger = swagger;
    this.config = config;
    this.classSets = new ArrayList<>();

    this.prepare();
  }

  private void prepare() {
    final Map<String, Path> map = swagger.getPaths();
    final HashMap<String, ArrayList<String>> serviceMap = new HashMap<>();

    map.forEach((key, path) -> {
        final String resource = Util.getLastResource(key);

        if(serviceMap.containsKey(resource)){
          serviceMap.get(resource).add(key);
        }else{
          final ArrayList<String> resourcePathStrings = new ArrayList<String>(1);
          resourcePathStrings.add(key);

          serviceMap.put(resource, resourcePathStrings);
        }
      });

    serviceMap.forEach((resource, paths) -> {
        final ServiceClassSet classSet
          = new ServiceClassSet(resource, paths.toArray((new String[]{})));
        this.classSets.add(classSet);
      });
  }

  public ServiceClassSet getServiceClassSetByPath(final String pathString) {
    for(ServiceClassSet classSet : classSets) {
      if(classSet.isMatchingPathString(pathString)) {
        return classSet;
      }
    }
    return null;
  }

  public void generate(final String outdir, final String pkgRoot) {
    classSets.forEach((set) -> {
        set.generate(outdir, pkgRoot);
      });
  }
}
