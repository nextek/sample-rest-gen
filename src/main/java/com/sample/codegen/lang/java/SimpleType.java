package com.sample.codegen.lang.java;

import java.util.ArrayList;
import java.util.List;

import com.sample.codegen.Util;

// java.util.List<java.util.String>

public class SimpleType extends Type {
  private static Type VOID_TYPE         = new SimpleType("void", null);
  private static Type BOOLEAN_PRIM_TYPE = new SimpleType("boolean", null);
  private static Type BOOLEAN_BOX_TYPE  = new SimpleType("Boolean", null);
  private static Type INT_PRIM_TYPE     = new SimpleType("int", null);
  private static Type INT_BOX_TYPE      = new SimpleType("Integer", null);
  private static Type LONG_PRIM_TYPE    = new SimpleType("long", null);
  private static Type LONG_BOX_TYPE     = new SimpleType("Long", null);
  private static Type FLOAT_BOX_TYPE    = new SimpleType("Float", null);
  private static Type DOUBLE_BOX_TYPE   = new SimpleType("Double", null);
  private static Type STRING_TYPE       = new SimpleType("String", null);
  private static Type BYTE_BOX_TYPE     = new SimpleType("Byte", null);
  private static Type LIST_TYPE         = new SimpleType("List", "java.util.List");
  private static Type MAP_TYPE          = new SimpleType("Map", "java.util.Map");
  private String  typeName;
  private String  fullName;

  public static Type getVoidType() {
    return VOID_TYPE;
  }
  public static Type getBooleanPrimitiveType() {
    return BOOLEAN_PRIM_TYPE;
  }
  public static Type getBooleanBoxType() {
    return BOOLEAN_BOX_TYPE;
  }
  public static Type getIntPrimitiveType() {
    return INT_PRIM_TYPE;
  }
  public static Type getIntegerBoxType() {
    return INT_BOX_TYPE;
  }
  public static Type getLongPrimitiveType() {
    return LONG_PRIM_TYPE;
  }
  public static Type getLongBoxType() {
    return LONG_BOX_TYPE;
  }
  public static Type getFloatBoxType() {
    return FLOAT_BOX_TYPE;
  }
  public static Type getDoubleBoxType() {
    return DOUBLE_BOX_TYPE;
  }
  public static Type getStringType() {
    return STRING_TYPE;
  }
  public static Type getByteBoxType() {
    return BYTE_BOX_TYPE;
  }
  public static Type getListType() {
    return LIST_TYPE;
  }
  public static Type getMapType() {
    return MAP_TYPE;
  }

  public SimpleType(LClass classType) {
    this.typeName = classType.getClassName();
    this.fullName = classType.getFullName();
    if(fullName != null) {
      Util.assertValidImportRef(this.fullName);
    }
  }

  public SimpleType(final String typeName, final String fullName) {
    // fullName can be null
    this.typeName = typeName;
    this.fullName = fullName;
    if(typeName == null) {
      throw new RuntimeException("typeName can't be null");
    }
    if(fullName != null) {
      Util.assertValidImportRef(fullName);
    }
  }

  public boolean isVoid() {
    return this.equals(getVoidType());
  }

  public boolean isString() {
    return this.equals(getStringType());
  }
  
  public String getTypeName() {
    return typeName;
  }

  public String getFullName() {
    return fullName;
  }

  public List<String> getAllImportRefs() {
    List<String> importRefs = new ArrayList<>();
    if(getFullName() != null) {
      importRefs.add(getFullName());
    }
    importRefs.addAll(getImportRefs());
    return importRefs;
  }
  public boolean isComposite() {
    return false;
  }
}

