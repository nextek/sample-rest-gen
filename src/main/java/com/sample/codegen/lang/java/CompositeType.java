package com.sample.codegen.lang.java;

import java.util.ArrayList;
import java.util.List;

import com.sample.codegen.Util;

// java.util.List<java.util.String>

public class CompositeType extends Type {
  private Type       containerType;
  private List<Type> composingTypes = new ArrayList<>();

  public CompositeType(Type containerType, Type firstComposingType) {
    setContainerType(containerType);
    addComposingType(firstComposingType);
  }

  public CompositeType(Type containerType, Type firstComposingType, Type secondComposingType) {
    setContainerType(containerType);
    addComposingType(firstComposingType);
    addComposingType(secondComposingType);
  }

  public Type getContainerType() {
    return containerType;
  }

  public List<Type> getComposingTypes() {
    return composingTypes;
  }

  public String getTypeName() {
    List<String> composingTypeNames = new ArrayList<>();
    for(Type composingType : this.composingTypes) {
      composingTypeNames.add(composingType.getTypeName());
    }
    String insideStr = String.join(", ", composingTypeNames);
    String typeName = String.format("%s<%s>", this.containerType.getTypeName(), insideStr);
    return typeName;
  }

  public String getFullName() {
    List<String> composingFullNames = new ArrayList<>();
    for(Type composingType : this.composingTypes) {
      String fullName = composingType.getFullName();
      if(fullName != null) {
        composingFullNames.add(fullName);
      }
    }
    String insideStr = String.join(", ", composingFullNames);
    String fullName = String.format("%s<%s>", this.containerType.getFullName(), insideStr);
    return fullName;
  }

  public List<String> getAllImportRefs() {
    final List<String> ret = new ArrayList<String>();
    ret.addAll(this.getImportRefs());

    List<Type> subTypes = new ArrayList<>();
    if(this.containerType != null) {
      subTypes.add(this.containerType);
    }
    subTypes.addAll(this.composingTypes);

    for(Type curType : subTypes) {
      ret.addAll(curType.getAllImportRefs());
    }

    return ret;
  }

  public void setContainerType(Type type) {
    if(containerType != null) {
      throw new RuntimeException("container type is already set");
    }
    containerType = type;
  }

  public void addComposingType(Type type) {
    composingTypes.add(type);
  }

  public boolean isVoid() {
    return false;
  }

  public boolean isString() {
    return false;
  }

  public boolean isComposite() {
    return true;
  }
}

