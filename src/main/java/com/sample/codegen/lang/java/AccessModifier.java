package com.sample.codegen.lang.java;

public enum AccessModifier {
  PUBLIC, PROTECTED, PRIVATE, NONE;

  public String keyword() {
    final String keyword = this.name();
    if(keyword.equals("DEFAULT")){
      return "";
    }
    return keyword.toLowerCase();
  }
};
