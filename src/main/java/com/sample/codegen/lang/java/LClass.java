package com.sample.codegen.lang.java;

import java.lang.RuntimeException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.sample.codegen.Util;

// LClass = language Class
// prefixed l is to distinguish from Java's java.lang.Class

public class LClass {
  static private final int ODD_PRIME_NUMS[]
    = { 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59,
        61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131,
        137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199 };

  static public class GenerationOptions {
    public int     maxLineLen                          = 100;
    public boolean separateFieldsByBlankLines          = false;
    public boolean ensureBlankLineBeforeFldAnnotations = false;
    public boolean verticallyLineUpFields              = false;
    public boolean verticallyLineUpFieldEquals         = false;
    public boolean sortFields                          = true;
    public boolean sortMethods                         = true;
  }

  private String  className;
  private String  pkgName;
  private boolean isInterface = false;
  private LClass  parentClass;
  private LClass  ofInterface;
  private boolean isFinaLClass;
  private String  headerDescBlock;   // a block of text just under the package line

  private List<Field>  fields      = new ArrayList<Field>();
  private List<Method> methods     = new ArrayList<Method>();
  private List<String> annotations = new ArrayList<String>();
  private List<String> importRefs  = new ArrayList<String>();

  // set either explicitJavaDoc
  // OR {javaDocDescLines, javaDocAuthor, and javaDocVersion}
  private String       explicitJavaDoc;
  private List<String> javaDocDescLines = new ArrayList<String>();
  private String       javaDocAuthor;
  private String       javaDocVersion;

  private int          maxLineLen = 100;
  private int          indentSize = 2;

  public LClass(final String className) {
    this.className = className;
  }

  public Type getType() {
    return new SimpleType(this);
  }
  
  public void setClassName(final String s) {
    className = s;
  }
  public void setPkgName(final String s) {
    if(s.matches(".*[A-Z].*")) {
      throw new RuntimeException("attmpting to set pkgName with capital letter: " + s);
    }
    pkgName = s;
  }
  public void setIsInterface(final boolean val) {
    isInterface = val;
  }
  public void setParentClass(final LClass classObj) {
    parentClass = classObj;
  }
  public void setOfInterface(final LClass interf) {
    ofInterface = interf;
  }
  public void setIsFinaLClass(final boolean b) {
    isFinaLClass = b;
  }
  public void addField(final Field f) {
    if(! isUniqField(f)) {
      throw new RuntimeException("Field " + f.getFieldName() + " not a uniq field");
    }
    fields.add(f);
  }
  public boolean isConstructor(final Method m) {
    return m.getMethodName().equals(getClassName());
  }

  public void addMethod(final Method m) {
    if(isConstructor(m)) {
      m.setOrderPriorityGroup(Method.CONSTRUCTOR_ORDER_PRIORITY);
    }

    m.setMaxLineLen(getMethodMaxLineLen());
    m.setIndentSize(getIndentSize());

    methods.add(m);
  }
  public void addAnnotation(final String a) {
    annotations.add(a);
  }
  public void setExplicitJavDoc(final String a) {
    explicitJavaDoc = a;
  }
  public void addJavaDocDescLine(final String a) {
    javaDocDescLines.add(a);
  }
  public void setJavaDocAuthor(final String s) {
    javaDocAuthor = s;
  }
  public void setJavaDocVersion(final String s) {
    javaDocVersion = s;
  }
  public void setMaxLineLen(final int l) {
    maxLineLen = l;
  }
  public void setIndentSize(final int s) {
    indentSize = s;
  }
  public void setHeaderDescBlock(final String s) {
    headerDescBlock = s;
  }

  public String  getClassName() {
    return className;
  }
  public String  getPkgName() {
    return pkgName;
  }
  public String  getFullName() {
    String classNameNoTmplArgs = getClassName().replaceAll("\\<.*", "");
    return pkgName + "." + classNameNoTmplArgs;
  }
  public boolean getIsInterface() {
    return isInterface;
  }
  public LClass  getParentClass() {
    return parentClass;
  }
  public LClass  getOfInterface() {
    return ofInterface;
  }
  public boolean getIsFinaLClass() {
    return isFinaLClass;
  }
  public int     getMaxLineLen() {
    return maxLineLen;
  }
  public int     getIndentSize() {
    return indentSize;
  }
  public String  getHeaderDescBlock() {
    return headerDescBlock;
  }

  public int     getMethodMaxLineLen() {
    return maxLineLen - indentSize;
  }

  public List<Field>  getFields() {
    return fields;
  }
  public Field getFieldOfName(String fieldName) {
    for(Field field : this.fields) {
      if(field.getFieldName().equals(fieldName)) {
        return field;
      }
    }
    return null;
  }
  
  public List<Method> getMethods() {
    return methods;
  }
  public int getNumMethods() {
    return methods.size();
  }
  public int getNumNonConstructorMethods() {
    int cnt = 0;
    for(Method m : getMethods()) {
      if(! isConstructor(m)) {
        cnt++;
      }
    }
    return cnt;
  }

  public List<String> getAnnotations() {
    return annotations;
  }
  public List<String> getImportRefs() {
    return importRefs;
  }

  public String getExplicitJavaDoc() {
    return explicitJavaDoc;
  }

  public Field createField(final String fieldName, final AccessModifier accessLevel) {
    final int fieldLineLen = getMaxLineLen() - getIndentSize();
    final Field newField = new Field(fieldName, accessLevel, fieldLineLen, getIndentSize());
    return newField;
  }

  public Method createMethod(final String methodName, final AccessModifier accessLevel) {
    final Method newMethod = new Method(methodName, accessLevel, getMethodMaxLineLen(),
                                        getIndentSize());
    return newMethod;
  }

  public void createEqualsMethod(boolean sortFields) {
    createEqualsMethodStyle4(sortFields);
  }

  public void createEqualsMethodStyle1(boolean sortFields) {
    final String className = getClassName();

    final Method equalsMethod = createMethod("equals", AccessModifier.PUBLIC);
    final String equalsJoinerToken = "      && ";
    final int maxEqualBlobLen = equalsMethod.getBodyMaxLineLen() - equalsJoinerToken.length();
 
    final List<String> equalsChunks = new ArrayList<String>();

    if(this.parentClass != null) {
      equalsChunks.add("super.equals(obj)");
    }

    List<Field> outputFields;
    if(sortFields) {
      outputFields = orderFields(getFields());
    } else {
      outputFields = getFields();
    }

    for(Field field : outputFields) {
      if(field.getIsStatic()) {
        continue;
      }
      final String fieldName = field.getFieldName();

      List<String> lineTokens = new ArrayList<>();
      lineTokens.add(fieldName);
      lineTokens.add("that." + fieldName);

      final String initialPart = "Objects.equals(";
      final int initialLen = initialPart.length();
      final String successiveIndent = Util.getRepeatedStr(initialLen, " ");

      final String blob
        = Util.wrapJoinTokens(initialPart, maxEqualBlobLen, successiveIndent, ", ", ")",
                              Util.JoinTokenStyle.APPEND, lineTokens, false, true, true);
      equalsChunks.add(blob);
    }

    final String equalsTxt = String.join(Util.nl() + equalsJoinerToken, equalsChunks);

    final String methodBody =
        "if (this == obj) {"                                      + Util.nl()
      + "  return true;"                                          + Util.nl()
      + "}"                                                       + Util.nl()
      + "if (obj == null || getClass() != obj.getClass()) {"      + Util.nl()
      + "  return false;"                                         + Util.nl()
      + "}"                                                       + Util.nl()
      + "final " + className + " that = (" + className + ") obj;" + Util.nl()
      + "return   " + equalsTxt + ";";

    equalsMethod.setReturnType(SimpleType.getBooleanPrimitiveType());
    final Param arg1 = new Param("obj", new SimpleType("Object", null));
    arg1.setIsFinal(true);
    equalsMethod.addParam(arg1);
    equalsMethod.setBody(methodBody);
    equalsMethod.setOrderPriorityGroup(85);
    equalsMethod.addAnnotation("@Override()");

    equalsMethod.addImportRef("java.util.Objects");  // used in method body

    addMethod(equalsMethod);
  }
  
  public void createEqualsMethodStyle2(boolean sortFields) {
    final String className = getClassName();

    final Method equalsMethod = createMethod("equals", AccessModifier.PUBLIC);
    final int GROUP_PRE_LEN = 24;
    final int maxEqualBlobLen = equalsMethod.getBodyMaxLineLen() - GROUP_PRE_LEN;

    final List<String> equalsChunks = new ArrayList<String>();

    List<String> tokens = new ArrayList<>();
    if(this.parentClass != null) {
      tokens.add("super.equals(obj)");
    }

    List<Field> outputFields;
    if(sortFields) {
      outputFields = orderFields(getFields());
    } else {
      outputFields = getFields();
    }

    for(Field field : outputFields) {
      if(field.getIsStatic()) {
        continue;
      }
      final String fieldName = field.getFieldName();
      
      List<String> lineTokens = new ArrayList<>();
      lineTokens.add(fieldName);
      lineTokens.add("that." + fieldName);

      final String initialPart = "Objects.equals(";
      final int initialLen = initialPart.length();
      final String successiveIndent = Util.getRepeatedStr(initialLen, " ");

      final String blob
        = Util.wrapJoinTokens(initialPart, maxEqualBlobLen, successiveIndent, ", ", ")",
                              Util.JoinTokenStyle.APPEND, lineTokens, false, true, true);
      tokens.add(blob);
    }

    String finalVar = "result";
    List<String> retLines
      = Util.getConditionalGroupedLines(0, equalsMethod.getBodyMaxLineLen(), " && ", tokens,
                                        finalVar);
    List<String> ret = retLines;
    String equalsTxt = String.join(Util.nl(), ret);

    final String methodBody =
        "if (this == obj) {"                                      + Util.nl()
      + "  return true;"                                          + Util.nl()
      + "}"                                                       + Util.nl()
      + "if (obj == null || getClass() != obj.getClass()) {"      + Util.nl()
      + "  return false;"                                         + Util.nl()
      + "}"                                                       + Util.nl()
      + "final " + className + " that = (" + className + ") obj;" + Util.nl()
      + equalsTxt                                                 + Util.nl()
      + "return   " + finalVar + ";";

    equalsMethod.setReturnType(SimpleType.getBooleanPrimitiveType());
    final Param arg1 = new Param("obj", "Object", null);
    arg1.setIsFinal(true);
    equalsMethod.addParam(arg1);
    equalsMethod.setBody(methodBody);
    equalsMethod.setOrderPriorityGroup(85);
    equalsMethod.addAnnotation("@Override()");

    equalsMethod.addImportRef("java.util.Objects");  // used in method body

    addMethod(equalsMethod);
  }

  public void createEqualsMethodStyle3(boolean sortFields) {
    final String className = getClassName();

    final Method equalsMethod = createMethod("equals", AccessModifier.PUBLIC);
    final int EXTRA_CHARS_LEN = 13;
    final int maxEqualBlobLen = equalsMethod.getBodyMaxLineLen() - EXTRA_CHARS_LEN;

    final List<String> equalsChunks = new ArrayList<String>();

    if(this.parentClass != null) {
      equalsChunks.add("super.equals(obj)");
    }

    List<Field> outputFields;
    if(sortFields) {
      outputFields = orderFields(getFields());
    } else {
      outputFields = getFields();
    }

    for(Field field : outputFields) {
      if(field.getIsStatic()) {
        continue;
      }
      final String fieldName = field.getFieldName();

      List<String> lineTokens = new ArrayList<>();
      lineTokens.add(fieldName);
      lineTokens.add("that." + fieldName);

      final String initialPart = "Objects.equals(";
      final int initialLen = initialPart.length();
      final String successiveIndent = Util.getRepeatedStr(initialLen, " ");

      final String blob
        = Util.wrapJoinTokens(initialPart, maxEqualBlobLen, successiveIndent, ", ", ")",
                              Util.JoinTokenStyle.APPEND, lineTokens, false, true, true);
      equalsChunks.add(blob);
    }

    List<String> cmpLines = new ArrayList<>();
    for(String equalsChunk : equalsChunks) {
      cmpLines.add("if(! " + equalsChunk + ") {");
      cmpLines.add("  isEquals = false;");
      cmpLines.add("}");
    }

    final String equalsTxt = String.join(Util.nl(), cmpLines);

    final String methodBody =
        "if (this == obj) {"                                      + Util.nl()
      + "  return true;"                                          + Util.nl()
      + "}"                                                       + Util.nl()
      + "if (obj == null || getClass() != obj.getClass()) {"      + Util.nl()
      + "  return false;"                                         + Util.nl()
      + "}"                                                       + Util.nl()
      + "final " + className + " that = (" + className + ") obj;" + Util.nl()
      + "boolean isEquals = true;"                                + Util.nl()
      + equalsTxt                                                 + Util.nl()
      + "return isEquals;";

    equalsMethod.setReturnType(SimpleType.getBooleanPrimitiveType());
    final Param arg1 = new Param("obj", "Object", null);
    arg1.setIsFinal(true);
    equalsMethod.addParam(arg1);
    equalsMethod.setBody(methodBody);
    equalsMethod.setOrderPriorityGroup(85);
    equalsMethod.addAnnotation("@Override()");

    equalsMethod.addImportRef("java.util.Objects");  // used in method body

    addMethod(equalsMethod);
  }

  public void createEqualsMethodStyle4(boolean sortFields) {
    final String className = getClassName();

    final Method equalsMethod = createMethod("equals", AccessModifier.PUBLIC);
    final int EXTRA_CHARS_LEN = 15;
    final int maxEqualBlobLen = equalsMethod.getBodyMaxLineLen() - EXTRA_CHARS_LEN;

    final List<String> equalsChunks = new ArrayList<String>();

    if(this.parentClass != null) {
      equalsChunks.add("    .appendSuper(super.equals(obj))");
    }

    List<Field> outputFields;
    if(sortFields) {
      outputFields = orderFields(getFields());
    } else {
      outputFields = getFields();
    }

    for(Field field : outputFields) {
      if(field.getIsStatic()) {
        continue;
      }
      final String fieldName = field.getFieldName();

      List<String> lineTokens = new ArrayList<>();
      lineTokens.add(fieldName);
      lineTokens.add("that." + fieldName + ")");

      final String initialPart = "    .append(";
      final int initialLen = initialPart.length();
      final String successiveIndent = Util.getRepeatedStr(initialLen, " ");

      final String blob
        = Util.wrapJoinTokens(initialPart, maxEqualBlobLen, successiveIndent, ", ", "",
                              Util.JoinTokenStyle.APPEND, lineTokens, false, true, false);
      equalsChunks.add(blob);
    }
    equalsChunks.add("    .isEquals();");

    final String equalsTxt = String.join(Util.nl(), equalsChunks);

    final String methodBody =
        "if (this == obj) {"                                      + Util.nl()
      + "  return true;"                                          + Util.nl()
      + "}"                                                       + Util.nl()
      + ""                                                        + Util.nl()
      + "if (obj == null || getClass() != obj.getClass()) {"      + Util.nl()
      + "  return false;"                                         + Util.nl()
      + "}"                                                       + Util.nl()
      + ""                                                        + Util.nl()
      + "final " + className + " that = (" + className + ") obj;" + Util.nl()
      + ""                                                        + Util.nl()
      + "return new EqualsBuilder()"                              + Util.nl()
      + equalsTxt;

    equalsMethod.setReturnType(SimpleType.getBooleanPrimitiveType());
    final Param arg1 = new Param("obj", "Object", null);
    arg1.setIsFinal(true);
    equalsMethod.addParam(arg1);
    equalsMethod.setBody(methodBody);
    equalsMethod.setOrderPriorityGroup(85);
    equalsMethod.addAnnotation("@Override()");
    equalsMethod.addJavaDocDescLine("Tests if another object is equal to this object.");
    equalsMethod.setJavaDocReturnDesc("true if objects are equals, otherwise false.");

    // used in method body
    equalsMethod.addImportRef("org.apache.commons.lang3.builder.EqualsBuilder");

    addMethod(equalsMethod);
  }

  public void createHashCodeMethod(boolean sortFields) {
    createHashCodeMethodStyle2(sortFields);
  }

  public void createHashCodeMethodStyle1(boolean sortFields) {
    final List<String> fieldNames = new ArrayList<String>();

    List<Field> outputFields;
    if(sortFields) {
      outputFields = orderFields(getFields());
    } else {
      outputFields = getFields();
    }

    for(Field field : outputFields) {
      if(field.getIsStatic()) {
        continue;
      }
      final String fieldName = field.getFieldName();
      fieldNames.add(fieldName);
    }
    final String successiveIndent = Util.getRepeatedStr(16, " ");
    final int methodBodyMaxLineLen = getMethodMaxLineLen() - getIndentSize();

    final String hashTxtA
      = Util.wrapJoinTokens("    return Objects.hash(", methodBodyMaxLineLen, successiveIndent,
                            ", ", ");", Util.JoinTokenStyle.APPEND, fieldNames, false, true, true);

    final Method hashCodeMethod = createMethod("hashCode", AccessModifier.PUBLIC);
    hashCodeMethod.setReturnType(SimpleType.getIntPrimitiveType());
    hashCodeMethod.setBody(hashTxtA);
    hashCodeMethod.setOrderPriorityGroup(85);
    hashCodeMethod.addAnnotation("@Override()");

    addMethod(hashCodeMethod);
  }

  public long getClassRandNum1() {
    long num = className.hashCode();
    for(Field f : getFields()) {
      num += f.getFieldName().hashCode();
    }
    return num;
  }

  public long getClassRandNum2() {
    long num = className.hashCode() / 3;
    int i = 0;
    for(Field f : getFields()) {
      if(i % 3 == 0) {
        num += f.getFieldName().hashCode();
      }
      i++;
    }
    return num;
  }
  
  public int pickOddPrime(long randNum) {
    if(randNum < 0) {
      randNum = -randNum;
    }
    int chosenIndex = (int)(randNum % (long)ODD_PRIME_NUMS.length);
    return ODD_PRIME_NUMS[chosenIndex];
  }
  
  public int pickInitialNonzeroOdd() {
    return pickOddPrime(getClassRandNum1());
  }

  public int pickMultiplierNonzeroOdd() {
    return pickOddPrime(getClassRandNum2());
  }

  public void createHashCodeMethodStyle2(boolean sortFields) {
    final List<String> hashChunks = new ArrayList<String>();

    String oddNum1Var = "INITIAL_NONZERO_ODD_NUM";
    int oddNum1 = pickInitialNonzeroOdd();
    addConstIntField(oddNum1Var, oddNum1);

    String oddNum2Var = "MULTIPLIER_NONZERO_ODD_NUM";
    int oddNum2 = pickMultiplierNonzeroOdd();
    addConstIntField(oddNum2Var, oddNum2);
    
    if(this.parentClass != null) {
      hashChunks.add("    .appendSuper(super.hashCode())");
    }

    List<Field> outputFields;
    if(sortFields) {
      outputFields = orderFields(getFields());
    } else {
      outputFields = getFields();
    }

    for(Field field : outputFields) {
      if(field.getIsStatic()) {
        continue;
      }
      final String fieldName = field.getFieldName();
      hashChunks.add("    .append(" + fieldName + ")");
    }
    hashChunks.add("    .toHashCode();");

    final String hashCodeTxt = String.join(Util.nl(), hashChunks);

    final String initialRetLine = String.format("return new HashCodeBuilder(%s, %s)",
                                                oddNum1Var, oddNum2Var);

    final String methodBody = initialRetLine + Util.nl()
                            + hashCodeTxt;

    final Method hashCodeMethod = createMethod("hashCode", AccessModifier.PUBLIC);
    hashCodeMethod.setReturnType(SimpleType.getIntPrimitiveType());
    hashCodeMethod.setBody(methodBody);
    hashCodeMethod.setOrderPriorityGroup(85);
    hashCodeMethod.addAnnotation("@Override()");
    hashCodeMethod.addJavaDocDescLine("Returns a hash-code representative of this object.");
    hashCodeMethod.setJavaDocReturnDesc("a hash-code representative of this object.");

    // used in method body
    hashCodeMethod.addImportRef("org.apache.commons.lang3.builder.HashCodeBuilder");

    addMethod(hashCodeMethod);
  }

  public void addConstIntField(String fieldName, Integer useInt) {
    final Field field = this.createField(fieldName, AccessModifier.PRIVATE);
    field.setType(SimpleType.getIntPrimitiveType());
    field.setIsStatic(true);
    field.setIsFinal(true);
    field.setInitializerCode(Integer.toString(useInt));
    this.addField(field);
  }

  public void createToStringMethod(boolean sortFields) {
    final List<String> chunks = new ArrayList<String>();
    int ctr = 0;

    List<Field> outputFields;
    if(sortFields) {
      outputFields = orderFields(getFields());
    } else {
      outputFields = getFields();
    }

    for(Field field : outputFields) {
      if(field.getIsStatic()) {
        continue;
      }
      final String fieldName = field.getFieldName();
      String desc;
      if(ctr == 0) {
        desc = String.format("\"%s [%s=\"", getClassName(), fieldName);
      } else {
        desc = String.format("\", %s=\"", fieldName);
      }
      chunks.add(desc);
      chunks.add(fieldName);
      ctr++;
    }
    chunks.add("\"]\";");

    final String firstLinePrefix = "return ";
    final int prefixLen      = firstLinePrefix.length();
    final int methodBodyMaxLineLen = getMethodMaxLineLen() - getIndentSize();
    final String successiveLinePrefix = Util.getRepeatedStr(prefixLen, " ");

    final String toStringBody = Util.wrapJoinTokens(firstLinePrefix, methodBodyMaxLineLen,
                                                    successiveLinePrefix,
                                                    " + ", "", Util.JoinTokenStyle.PREPEND,
                                                    chunks, true, true, true);
    final Method toStringMethod = createMethod("toString", AccessModifier.PUBLIC);
    toStringMethod.setReturnType(SimpleType.getStringType());
    toStringMethod.setBody(toStringBody);
    toStringMethod.setOrderPriorityGroup(85);
    toStringMethod.addAnnotation("@Override()");
    toStringMethod.addJavaDocDescLine("Returns a String representative of this object.");
    toStringMethod.setJavaDocReturnDesc("a String representative of this object.");

    addMethod(toStringMethod);
  }

  public void addImportRef(final String pkg) {
    Util.assertValidImportRef(pkg);
    importRefs.add(pkg);
  }

  public void addImportRefs(final List<String> list) {
    for(String r : list) {
      addImportRef(r);
    }
  }

  public void removeImportRef(final String pkg)  {
    final Iterator<String> it = importRefs.iterator();
    while (it.hasNext()) {
      final String elem = it.next();
      if(elem.equals(pkg)) {
        it.remove();
      }
    }
  }

  protected boolean isUniqField(final Field f) {
    // todo
    return true;
  }

  protected boolean isUniqMethod(final Method f) {
    // todo
    return true;
  }

  protected List<String> preGenerateAnalyze() {
    final List<String> simpleParamTypesNeedingFullRef = new ArrayList<String>();

    final Map<String, List<Method>> simpleSigMethodsMap = new HashMap<String, List<Method>>();

    for(Method curMethod : getMethods()) {
      final String simpleSig  = curMethod.getSimpleTypeSigString();
      List<Method> list = simpleSigMethodsMap.get(simpleSig);
      if(list == null) {
        list = new ArrayList<Method>();
        simpleSigMethodsMap.put(simpleSig, list);
      }
      list.add(curMethod);
    }

    final List<String> typesNeedingFullRef = new ArrayList<String>();

    ALL_METHOD_LOOP:
    for(Method curMethod : methods) {
      final String simpleSig        = curMethod.getSimpleTypeSigString();
      final List<Method> methodList = simpleSigMethodsMap.get(simpleSig);

      if(methodList.size() <= 1) {
        // we only care about methods which have a duplicate simple method sig
        continue ALL_METHOD_LOOP;
      }

      final String fullSig = curMethod.getFullTypeSigString();

      DUP_METHOD_LOOP:
      for(Method thisMethod : methodList) {
        final String thisFullSig = thisMethod.getFullTypeSigString();
        if(thisFullSig.equals(fullSig)) {
          continue DUP_METHOD_LOOP;
        }

        final List<String> commonTypes = curMethod.findCommonParamSimpleTypes(thisMethod);
        typesNeedingFullRef.addAll(commonTypes);
      }
    }

    final Map<String, Integer> typesNeedingFullRefMap = new HashMap<String, Integer>();
    for(String type : typesNeedingFullRef) {
      typesNeedingFullRefMap.put(type, 1);
    }

    simpleParamTypesNeedingFullRef.addAll(typesNeedingFullRefMap.keySet());
    return simpleParamTypesNeedingFullRef;
  }

  public String getClassTxt(final GenerationOptions op) {
    final List<String> simpleParamTypesNeedingFullRef = preGenerateAnalyze();
    final List<String> imports = new ArrayList<>();
    imports.addAll(getImportRefs());
    Util.assertValidImportRefs(imports);
    
    final String javaDoc = getJavaDocText();
    final String classLine = getClassLine(imports);
    Util.assertValidImportRefs(imports);
    final String fieldsDeclIndented = getFieldsText(op, imports);
    Util.assertValidImportRefs(imports);
    final String annotationsTxt = getAnnotationsText();
    final String methodTxt = getMethodText(op, simpleParamTypesNeedingFullRef, imports);
    Util.assertValidImportRefs(imports);
    final String importTxt = getImportTxt(imports, simpleParamTypesNeedingFullRef);
    Util.assertValidImportRefs(imports);

    //now combine everything

    final List<String> classParts = new ArrayList<String>();
    classParts.add("package " + getPkgName() + ";");
    classParts.add("");  // blank line after package

    if(this.headerDescBlock != null && this.headerDescBlock.length() > 0) {
      String blockTxt = "/**" + Util.nl();
      for(String line : this.headerDescBlock.split(Util.nl())) {
        blockTxt += " * " + line + Util.nl();
      }
      blockTxt += " */" + Util.nl();
      classParts.add(blockTxt);
    }

    if(importTxt != null && !importTxt.isEmpty()) {
      classParts.add(importTxt);
      classParts.add("");  // blank line after imports
    }

    if(javaDoc != null) {
      classParts.add(javaDoc);
    }

    if(annotationsTxt != null) {
      classParts.add(annotationsTxt);
    }

    classParts.add(classLine);

    if(fieldsDeclIndented != null) {
      classParts.add(fieldsDeclIndented);
      if(getMethods().size() > 0) {
        classParts.add("");   // blank line after fields
      }
    }

    if(methodTxt != null) {
      classParts.add(methodTxt);
    }

    classParts.add("}");

    final String txt = String.join(Util.nl(), classParts);
    return txt;
  }

  protected String getJavaDocText() {
    String javaDoc = getExplicitJavaDoc();
    if(javaDoc == null) {
      javaDoc = createClassJavaDoc();
    }
    return javaDoc;
  }

  protected String getAnnotationsText(){
    if(this.annotations.size() > 0) {
      return String.join(Util.nl(), getAnnotations());
    }
    return null;
  }

  protected String getMethodText(GenerationOptions op, List<String> simpleParamTypesNeedingFullRef,
                                 List<String> imports) {
    String methodTxt;
    List<Method> outputMethods;
    if(op.sortMethods) {
      outputMethods = orderMethods(getMethods());
    } else {
      outputMethods = getMethods();
    }

    final List<String> methodChunks = new ArrayList<String>();

    //System.out.println("Generating methods for " + this.getClassName());
    for(Method meth : outputMethods) {
      //System.out.println("\t" + meth.getMethodName());
      if(getIsInterface()) {
        final String decl = meth.getInterfaceDeclaration(simpleParamTypesNeedingFullRef);
        final String declInteded = Util.indentLines(getIndentSize(), decl);
        methodChunks.add(declInteded);
      } else {
        final String funcDef = meth.getDefinitionCode(simpleParamTypesNeedingFullRef, meth.getAccessLevel() == AccessModifier.NONE);
        final String funcDefTrimmed = funcDef.replaceFirst("\\s+$", "");
        final String funcDefIndented = Util.indentLines(getIndentSize(), funcDefTrimmed);
        methodChunks.add(funcDefIndented);
      }

      final List<String> methImports;
      if(getIsInterface()) {
        methImports = meth.getInterfaceImportRefs();
        Util.assertValidImportRefs(methImports);
      } else {
        methImports = meth.getAllImportRefs();
        Util.assertValidImportRefs(methImports);
      }
      imports.addAll(methImports);
    }

    methodTxt = String.join(Util.nl() + Util.nl(), methodChunks);
    return methodTxt;
  }

  protected String getFieldsText(GenerationOptions op, List<String> imports) {
    if(op.verticallyLineUpFieldEquals) {
      // Can't vertically line up field equals without vertically lining up fields
      op.verticallyLineUpFields = true;
    }

    List<Field> outputFields;
    if(op.sortFields) {
      outputFields = orderFields(getFields());
    } else {
      outputFields = getFields();
    }

    final List<String> fieldDeclLines = new ArrayList<String>();
    int lastPriority = 0;
    for(Field field : outputFields) {
      if(getIsInterface()) {
        continue;
      }

      final int orderPriorityGroup = field.getOrderPriorityGroup();

      boolean addBlankLine = false;
      if(field.getAnnotations().size() > 0 && op.ensureBlankLineBeforeFldAnnotations) {
        addBlankLine = true;
      } else if(op.separateFieldsByBlankLines) {
        addBlankLine = true;     // blank lines if requested
      } else if(lastPriority > 0 && orderPriorityGroup != lastPriority) {
        addBlankLine = true;     // blank lines between priority groups
      }

      if(addBlankLine) {
        fieldDeclLines.add("");  // blank lines if requested
      }

      Integer preDeclLen  = null;
      Integer fieldFmtLen = null;
      if(op.verticallyLineUpFields) {
        preDeclLen = getPreDeclLenForGroup(orderPriorityGroup);
      }
      if(op.verticallyLineUpFieldEquals) {
        fieldFmtLen = getFieldNameLenForGroup(orderPriorityGroup);
      }

      String fieldDecl = field.getDecl(preDeclLen, fieldFmtLen);
      fieldDeclLines.add(fieldDecl);

      imports.addAll(field.getAllImportRefs());
      lastPriority = field.getOrderPriorityGroup();
    }

    if(fieldDeclLines.size() > 0) {
      final String fieldsDeclTxt = String.join(Util.nl(), fieldDeclLines);
      return Util.indentLines(getIndentSize(), fieldsDeclTxt);
    }
    return null;
  }

  protected String getClassLine(List<String> imports) {
    String classLine;
    String finalStr = "";
    if(getIsFinaLClass()) {
      finalStr = "final ";
    }

    final List<String> tokens = new ArrayList<String>();
    final LClass parentClass = getParentClass();
    if(parentClass != null) {
      tokens.add("extends " + parentClass.getClassName());
      String importRef = parentClass.getFullName();
      Util.assertValidImportRef(importRef);
      imports.add(importRef);
    }

    final LClass ofInterface = getOfInterface();
    if(ofInterface != null) {
      final String interfaceName = ofInterface.getClassName();
      tokens.add("implements " + interfaceName);
      String importRef = ofInterface.getPkgName() + "." + interfaceName;
      Util.assertValidImportRef(importRef);
      imports.add(importRef);
    }

    String extraInfoStr = "";
    if(tokens.size() > 0) {
      extraInfoStr = String.join(" ", tokens) + " ";
    }

    if(getIsInterface()) {
      classLine = "public " + finalStr + "interface " + getClassName() + " " + extraInfoStr + " {";
    } else {
      classLine = "public " + finalStr + "class " + getClassName() + " " + extraInfoStr + "{";
    }
    return classLine;
  }

  public String getOutPath(final String outDir, final String pkgRoot) {
    final String subDir    = Util.pkg2subDir(getPkgName(), pkgRoot);
    final String classDir  = outDir + subDir;  // subdir starts with '/'
    final String classFile = getClassName() + ".java";
    final String classPath = classDir + "/" + classFile;
    return classPath;
  }

  public String createClassJavaDoc() {
    if(javaDocDescLines.size() == 0 || javaDocAuthor == null || javaDocVersion == null) {
      return null;
    }

    final List<String> fmtLines = new ArrayList<String>();
    for(String descLine : javaDocDescLines) {
      final String fmtLine = " * " + descLine;
      fmtLines.add(fmtLine);
    }
    final String fmtTxt = String.join(Util.nl(), fmtLines);

    final String javaDoc = "/**"                               + Util.nl()
                         + fmtTxt                              + Util.nl()
                         + " *"                                + Util.nl()
                         + " * @author      " + javaDocAuthor  + Util.nl()
                         + " * @version     " + javaDocVersion + Util.nl()
                         + " */";
    return javaDoc;
  }

  public String getImportTxt(final List<String> imports,
                             final List<String> simpleParamTypesNeedingFullRef) {

    final Map<String, Integer> typesNeedingFullRefMap = new HashMap<String, Integer>();
    for(String type : simpleParamTypesNeedingFullRef) {
      typesNeedingFullRefMap.put(type, 1);
    }

    final Map<String, Integer> keepImportsMap = new HashMap<String, Integer>();

    for(String importStr : imports) {
      final List<String> parts = new ArrayList<>();
      parts.addAll(Arrays.asList(importStr.split("\\.")));
      final String lastPkgPart = parts.get(parts.size() - 1);
      parts.remove(parts.size() - 1);
      final String importRefPkg = String.join(".", parts);

      if(importRefPkg.equals(getPkgName())) {
        continue;
      }

      // if the import line doesn't need a full reference, then keep import line for output
      if(typesNeedingFullRefMap.get(lastPkgPart) == null) {
        keepImportsMap.put(importStr, 1);
      }
    }

    //System.err.println("keepImportsMap:\n" + keepImportsMap.toString());

    final List<String> outputImports = new ArrayList<String>();
    outputImports.addAll(keepImportsMap.keySet());

    final List<String> grpOrder = Arrays.asList("java", "javax", "org", "net", "com",
                                                "junit", "junitx");

    final Map<String, Integer> outputtedImports = new HashMap<String, Integer>();
    final List<String> importChunks             = new ArrayList<String>();

    GRP_LOOP:
    for(String grp : grpOrder) {
      final List<String> items = new ArrayList<String>();

      for(String outputImport : outputImports) {
        //System.err.println("Comparing " + outputImport + " against ^" + grp + "\\..*");
        if(outputImport.matches("^" + grp + "\\..*")) {
          //System.err.println("  FOUND MATCH");
          items.add(outputImport);
        }
      }

      if(items.size() > 0) {
        items.sort(String::compareTo);
        final List<String> importLines = new ArrayList<String>();
        for(String item : items) {
          if(item.matches(".*:static$")){
            final String pkg = item.substring(0, item.length() - (":static".length()));
            final String importLine = "import static " + pkg + ";";
            importLines.add(wrapImportIfNeeded(importLine));
          } else {
            final String importLine = "import " + item + ";";
            importLines.add(wrapImportIfNeeded(importLine));
          }
        }
        final String chunk = String.join(Util.nl(), importLines);
        //System.err.println("Add Chunk:\n" + chunk + "]]");
        importChunks.add(chunk);
      }

      for(String item : items) {
        outputtedImports.put(item, 1);
      }
    }

    final List<String> remainderImports = new ArrayList<String>();

    for(String importStr : outputImports) {
      if(outputtedImports.get(importStr) != null) {
        continue;
      }
      remainderImports.add(importStr);
    }

    if(remainderImports.size() > 0) {
      remainderImports.sort(String::compareTo);
      final List<String> importLines = new ArrayList<String>();
      for(String importRef : remainderImports) {
        importLines.add("import " + importRef + ";");
      }
      final String chunk = String.join(Util.nl(), importLines);
      importChunks.add(chunk);
    }

    final String importTxt = String.join(Util.nl() + Util.nl(), importChunks);
    return importTxt;
  }

  public String wrapImportIfNeeded(final String importLine) {
    final List<String> parts = new ArrayList<>();
    parts.addAll(Arrays.asList(importLine.split("\\.")));

    final String importLines
      = Util.wrapJoinTokens("", getMaxLineLen(), "       ", ".", "",
                            Util.JoinTokenStyle.PREPEND, parts, false, true, true);
    return importLines;
  }

  protected List<Method> orderMethods(final List<Method> methods) {
    Comparator<Method> methodCompare =
      new Comparator<Method>() {
        public int compare(final Method a, final Method b) {
          int aPriority = a.getOrderPriorityGroup();
          int bPriority = b.getOrderPriorityGroup();

          if(aPriority == Method.UNDEFINED_ORDER_PRIORITY_MAGIC) {
            aPriority = Method.UNDEFINED_ORDER_PRIORITY_USE;
          }

          if(bPriority == Method.UNDEFINED_ORDER_PRIORITY_MAGIC) {
            bPriority = Method.UNDEFINED_ORDER_PRIORITY_USE;
          }

          if(aPriority < bPriority) {
            return -1;
          } else if(aPriority > bPriority) {
            return 1;
          } else {
            final String aSig = a.getSimpleTypeSigString();
            final String bSig = b.getSimpleTypeSigString();
            return aSig.compareTo(bSig);
          }
        }
      };

    final List<Method> retMethods = methods;
    Collections.sort(retMethods, methodCompare);
    return retMethods;
  }

  protected List<Field> orderFields(final List<Field> fields) {
    Comparator<Field> fieldCompare =
      new Comparator<Field>() {
        public int compare(final Field a, final Field b) {
          int aPriority = a.getOrderPriorityGroup();
          int bPriority = b.getOrderPriorityGroup();

          if(aPriority < bPriority) {
            return -1;
          } else if(aPriority > bPriority) {
            return 1;
          } else {
            final String aSig = a.getFieldName();
            final String bSig = b.getFieldName();
            return aSig.compareTo(bSig);
          }
        }
      };

    final List<Field> retFields = fields;
    Collections.sort(retFields, fieldCompare);
    return retFields;
  }

  protected int getPreDeclLenForGroup(final int orderPriorityGroup) {
    int maxPreDeclLen = 0;

    for(Field field : getFields()) {
      if(field.getOrderPriorityGroup() == orderPriorityGroup) {
        final String preDeclStr = field.getPreDeclString();
        final int    preDeclLen = preDeclStr.length();
        if(preDeclLen > maxPreDeclLen) {
          maxPreDeclLen = preDeclLen;
        }
      }
    }
    return maxPreDeclLen;
  }

  protected int getFieldNameLenForGroup(final int orderPriorityGroup) {
    int maxFieldNameLen = 0;

    for(Field field : getFields()) {
      if(field.getOrderPriorityGroup() == orderPriorityGroup) {
        final String fieldName    = field.getFieldName();
        final int    fieldNameLen = fieldName.length();
        if(fieldNameLen > maxFieldNameLen) {
          maxFieldNameLen = fieldNameLen;
        }
      }
    }
    return maxFieldNameLen;
  }

  public void addMethodsOnInterface(final Method m) {
    // adds methods on interface of this class
    // to implement
  }

}


