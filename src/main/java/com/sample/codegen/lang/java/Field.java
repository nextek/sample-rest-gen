package com.sample.codegen.lang.java;

import java.util.ArrayList;
import java.util.List;

import com.sample.codegen.Util;

public class Field {
  // fields will be outputted in the order of the orderPriorityGroup grouping
  // fields with a lower orderPriorityGroup will appear before those with a
  // larger orderPriorityGroup
  public static int         UNDEFINED_ORDER_PRIORITY_MAGIC = -1;
  public static int         UNDEFINED_ORDER_PRIORITY_USE   = 50;
  public static int         STATIC_ORDER_PRIORITY          = 5;
  
  private String            fieldName;
  private AccessModifier    accessLevel = AccessModifier.PRIVATE;
  private boolean           isStatic;
  private boolean           isFinal;
  private Type              type;
  private String            initializerCode;
  private String            javaDoc;
  private List<String> annotations = new ArrayList<String>();
  private List<String> importRefs  = new ArrayList<String>();

  private int               maxLineLen;
  private int               indentSize;
  private int               orderPriorityGroup = UNDEFINED_ORDER_PRIORITY_MAGIC;

  public Field(final String fieldName, final AccessModifier accessLevel, final int maxLineLen,
               final int indentSize) {
    this.fieldName   = fieldName;
    this.accessLevel = accessLevel;
    this.maxLineLen  = maxLineLen;
    this.indentSize  = indentSize;
  }

  public void setFieldName(final String s) {
    fieldName = s;
  }
  public void setAccessLevel(final AccessModifier l) {
    accessLevel = l;
  }
  public void setIsStatic(final boolean b) {
    isStatic = b;
  }
  public void setIsFinal(final boolean b) {
    isFinal = b;
  }
  public void setType(final Type t) {
    type = t;
  }
  public void setInitializerCode(final String c) {
    initializerCode = c;
  }
  public void setJavaDoc(final String s) {
    javaDoc = s;
  }
  public void addAnnotation(final String a) {
    annotations.add(a);
  }
  public void addImportRef(final String a) {
    Util.assertValidImportRef(a);
    importRefs.add(a);
  }

  public void setMaxLineLen(final int i) {
    maxLineLen = i;
  }
  public void setIndentSize(final int i) {
    indentSize = i;
  }
  public void setOrderPriorityGroup(final int p) {
    orderPriorityGroup = p;
  }

  public String  getFieldName() {
    return fieldName;
  }
  public AccessModifier  getAccessLevel()  {
    return accessLevel;
  }
  public boolean getIsStatic() {
    return isStatic;
  }
  public boolean getIsFinal() {
    return isFinal;
  }
  public Type  getType() {
    return type;
  }
  //public List<String> getImportRefs() {
  //  return importRefs;
  //}
  public List<String> getAllImportRefs() {
    List<String> allImportRefs = new ArrayList<>();
    allImportRefs.addAll(type.getAllImportRefs());
    allImportRefs.addAll(this.importRefs);
    return allImportRefs;
  }

  public int  getOrderPriorityGroup() {
    if(this.orderPriorityGroup == UNDEFINED_ORDER_PRIORITY_MAGIC) {
      if(this.isStatic) {
        return STATIC_ORDER_PRIORITY;
      } else {
        return UNDEFINED_ORDER_PRIORITY_USE;
      }
    } else {
      return orderPriorityGroup;
    }
  }
  public int  getMaxLineLen() {
    return maxLineLen;
  }
  public int  getIndentSize() {
    return indentSize;
  }
  public List<String> getAnnotations() {
    return annotations;
  }

  // the string before the fieldName on a fieldDecl line
  public String getPreDeclString() {
    final List<String> preDeclParts = new ArrayList<String>();
    preDeclParts.add(accessLevel.keyword());
    if(isStatic) {
      preDeclParts.add("static");
    }
    if(isFinal) {
      preDeclParts.add("final");
    }
    preDeclParts.add(this.type.getTypeName());
    final String preDecl    = String.join(" ", preDeclParts);
    return preDecl;
  }

  public String getDecl() {
    return getDecl(null, null);
  }

  // pass typeNameLen of 0 to not use
  public String getDecl(final Integer preDeclLen, final Integer fieldNameFmtLen) {
    final List<String> lines = new ArrayList<String>();
    if(javaDoc != null) {
      lines.add("/** " + javaDoc + " */");
    }
    if(annotations.size() > 0) {
      lines.addAll(annotations);
    }

    final List<String> declParts = new ArrayList<String>();
    final String preDeclStr = getPreDeclString();
    String preDeclFmt = preDeclStr;
    if(preDeclLen != null) {
      preDeclFmt = String.format("%-" + preDeclLen + "s", preDeclStr);
    }
    declParts.add(preDeclFmt);

    String fieldNameFmt = fieldName;
    if(fieldNameFmtLen != null) {
      fieldNameFmt = String.format("%-" + fieldNameFmtLen + "s", fieldName);
    }
    declParts.add(fieldNameFmt);

    String preEquals = String.join(" ", declParts);
    List<String> wrapChunks = new ArrayList<>();
    if(initializerCode != null) {
      wrapChunks.add(" = ");
      wrapChunks.add(initializerCode + ";");
    } else {
      preEquals += ";";
    }

    final String successiveIndent = Util.getRepeatedStr(8, " ");
    String declLine
      = Util.wrapJoinTokens(preEquals, getMaxLineLen(), successiveIndent, "", "",
                            Util.JoinTokenStyle.PREPEND, wrapChunks, false, true, true);
    lines.add(declLine);

    final String retStr = String.join(Util.nl(), lines);
    return retStr;
  }

}


