package com.sample.codegen.lang.java;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sample.codegen.Util;

public class Method {
  // methods will be outputted in the order of the orderPriorityGroup grouping
  // methods with a lower orderPriorityGroup will appear before those with a
  // larger orderPriorityGroup
  public static int         UNDEFINED_ORDER_PRIORITY_MAGIC = -1;
  public static int         UNDEFINED_ORDER_PRIORITY_USE   = 50;
  public static int         CONSTRUCTOR_ORDER_PRIORITY     = 10;
  public static int         FIXED_METHOD_SIG_LEFT_INDENT   = 8;

  private String            methodName;
  private AccessModifier    accessLevel;
  private String            body;
  private Type              returnType = SimpleType.getVoidType();
  private boolean           isStatic   = false;

  private List<Param>  params               = new ArrayList<Param>();
  private List<String> annotations          = new ArrayList<String>();
  private List<String> importRefs           = new ArrayList<String>();
  private List<String> inherentImportRefs   = new ArrayList<String>();

  private String       explicitJavaDoc;
  private List<String> javaDocDescLines   = new ArrayList<String>();
  private String       javaDocReturnDesc;

  private int               orderPriorityGroup = UNDEFINED_ORDER_PRIORITY_MAGIC;

  private int               maxLineLen;
  private int               indentSize;

  private class ExceptionData {
    public String exceptionName;
    public String javaDocDesc;
    ExceptionData(final String exceptionName, final String javaDocDesc) {
      this.exceptionName = exceptionName;
      this.javaDocDesc   = javaDocDesc;
    }
  }

  private List<ExceptionData> exceptions = new ArrayList<ExceptionData>();

  public Method(final Method m) {
    this(m.getMethodName(), m.getAccessLevel(), m.getMaxLineLen(), m.getIndentSize());
    setJavaDocReturnDesc(m.getJavaDocReturnDesc());
    setReturnType(m.getReturnType());
    m.getParams().forEach((p) -> addParam(p));
    m.getJavaDocDescLines().forEach((l) -> addJavaDocDescLine(l));
    m.getImportRefs().forEach((r) -> addImportRef(r));
  }

  public Method(final String methodName, final AccessModifier accessLevel) {
    this.methodName  = methodName;
    this.accessLevel = accessLevel;
  }

  public Method(final String methodName, final AccessModifier accessLevel, final int maxLineLen,
                final int indentSize) {
    this.methodName  = methodName;
    this.accessLevel = accessLevel;
    this.maxLineLen  = maxLineLen;
    this.indentSize  = indentSize;
  }

  public void setMethodName(final String n) {
    methodName = n;
  }
  public void setAccessLevel(final AccessModifier m) {
    accessLevel = m;
  }
  public void setReturnType(final Type t) {
    // setting the return type to null is valid.  this may be done for a constructor which
    // does not have a return type
    returnType = t;
  }
  public void setIsStatic(final boolean v) {
    isStatic = v;
  }
  public void addParam(final Param p) {
    params.add(p);
  }
  public void addAnnotation(final String a) {
    annotations.add(a);
  }
  public void addImportRef(final String a) {
    Util.assertValidImportRef(a);
    importRefs.add(a);
  }
  public void setExplicitJavaDoc(final String a) {
    explicitJavaDoc = a;
  }
  public void addJavaDocDescLine(final String a) {
    javaDocDescLines.add(a);
  }
  public void setJavaDocReturnDesc(final String d) {
    javaDocReturnDesc = d;
  }
  public void setOrderPriorityGroup(final int p) {
    orderPriorityGroup = p;
  }
  public void setMaxLineLen(final int i) {
    maxLineLen = i;
  }
  public void setIndentSize(final int i) {
    indentSize = i;
  }

  public void addImportRefs(final List<String> list) {
    for(String r : list) {
      addImportRef(r);
    }
  }

  public void addException(final String exceptionName, final String javaDocDesc) {
    final ExceptionData d = new ExceptionData(exceptionName, javaDocDesc);
    exceptions.add(d);
  }

  public int getBodyMaxLineLen() {
    return getMaxLineLen() - getIndentSize();
  }

  // the method body should not be given indented
  // e.g. if the method body is one line : return 34;
  // then passed in String should have value "return 34;"
  // the body will then be indented on Method formatting
  public void setBody(final String b) {
    final int longestLineLen = Util.getLongestLineLen(b);
    if(longestLineLen > getBodyMaxLineLen()) {
      //System.err.println(String.format("longestLineLen: %d, bodyMaxLineLen: %d, "
      //                                 + "maxLineLen: %d, indentSize: %d",
      //                                 longestLineLen, bodyMaxLineLen, getMaxLineLen(),
      //                                 getIndentSize()));
      throw new RuntimeException(String.format("Method %s body of '%s' is longer then maxLineLen %d",
                                               getMethodName(), b, getMaxLineLen()));
    }
    body = b;
  }

  public void addAnnotations(final List<String> list) {
    for(String a : list) {
      addAnnotation(a);
    }
  }

  public String         getMethodName() {
    return methodName;
  }
  public AccessModifier getAccessLevel() {
    return accessLevel;
  }
  public String         getBody() {
    return body;
  }
  public Type           getReturnType() {
    return returnType;
  }
  public boolean        getIsStatic() {
    return isStatic;
  }
  public List<Param>    getParams() {
    return params;
  }
  public List<String>   getAnnotations() {
    return annotations;
  }
  public List<String>   getImportRefs() {
    return importRefs;
  }
  public String         getExplicitJavaDoc() {
    return explicitJavaDoc;
  }
  public List<String>   getJavaDocDescLines() {
    return javaDocDescLines;
  }
  public String         getJavaDocReturnDesc() {
    return javaDocReturnDesc;
  }
  public int            getOrderPriorityGroup() {
    return orderPriorityGroup;
  }
  public int            getMaxLineLen() {
    return maxLineLen;
  }
  public int            getIndentSize() {
    return indentSize;
  }

  public List<String> getInherentImportRefs() {
    return inherentImportRefs;
  }

  public int getNumParams() {
    return params.size();
  }

  // returns parameter number of parameter, based at 1
  public int getParamNum(final String paramName) {
    for(int i = 0; i < params.size(); i++) {
      final Param curParam = params.get(i);
      if(curParam.getParamName().equals(paramName)) {
        return i + 1;
      }
    }
    return 0;
  }

  public Param getParamByLabel(final String label) {
    for(Param p : params) {
      if(p.getLabel().equals(label)) {
        return p;
      }
    }
    return null;
  }

  public Param getParamByName(final String name) {
    for(Param p : params) {
      if(p.getParamName().equals(name)) {
        return p;
      }
    }
    return null;
  }

  public List<String> getExceptionNames() {
    final List<String> retList = new ArrayList<String>();
    for(ExceptionData e : exceptions) {
      retList.add(e.exceptionName);
    }
    return retList;
  }

  public String getExceptionJavaDoc(final String exceptionName) {
    for(ExceptionData e : exceptions) {
      if(e.exceptionName.equals(exceptionName)) {
        return e.javaDocDesc;
      }
    }
    return null;
  }

  // return inherent and explicit pkgImports and param pkgs
  public List<String> getInterfaceImportRefs() {
    final List<String> refs = new ArrayList<String>();
    refs.addAll(getInherentImportRefs());

    for(Param param : params) {
      refs.addAll(param.getAllImportRefs());
    }
    Type retType = getReturnType();
    if(retType != null) {
      refs.addAll(retType.getAllImportRefs());
    }

    final Map<String, Integer> importMap = new HashMap<String, Integer>();
    for(String p : refs) {
      importMap.put(p, 1);
    }

    refs.addAll(importMap.keySet());

    return refs;
  }

  // return inherent and explicit pkgImports and param pkgs
  public List<String> getAllImportRefs() {
    final List<String> refs = new ArrayList<String>();
    refs.addAll(getInherentImportRefs());
    refs.addAll(getImportRefs());

    for(Param param : params) {
      refs.addAll(param.getAllImportRefs());
    }
    Type retType = getReturnType();
    if(retType != null) {
      refs.addAll(retType.getAllImportRefs());
    }

    final Map<String, Integer> importMap = new HashMap<String, Integer>();
    for(String p : refs) {
      importMap.put(p, 1);
    }

    refs.addAll(importMap.keySet());

    return refs;
  }

  public String getFullTypeSigString() {
    final List<String> parts = new ArrayList<String>();
    parts.add(getMethodName());

    for(Param p : params) {
      parts.add(p.getFullName());
    }

    final String sigStr = String.join(",", parts);
    return sigStr;
  }

  public String getSimpleTypeSigString() {
    final List<String> parts = new ArrayList<String>();
    parts.add(getMethodName());

    for(Param p : params) {
      parts.add(p.getType().getTypeName());
    }

    final String sigStr = String.join(",", parts);
    return sigStr;
  }

  public List<String> findCommonParamSimpleTypes(final Method b) {
    final Map<String, Integer> typeMap = new HashMap<String, Integer>();

    for(Param thisParam : params) {
      for(Param bParam : b.getParams()) {
        String thisTypeName = thisParam.getType().getTypeName();
        String bTypeName    = bParam.getType().getTypeName();
        
        if(thisTypeName.equals(bTypeName)) {
          typeMap.put(bTypeName, 1);
        }
      }
    }

    final List<String> retList = new ArrayList<String>();
    retList.addAll(typeMap.keySet());
    return retList;
  }

  protected String getFunctionCall(final List<String> args, final String selfName,
                                   final Type returnDeclType, final String returnModifier,
                                   final String returnVariable, final int maxLineLen) {

    if(this.getNumParams() != args.size()){
      throw new IllegalArgumentException("Expected " + this.getNumParams() + " parameters, got "
                                         + args.size());
    }

    final String typeACode = this.getFunctionCallA(args, selfName, returnDeclType,
                                                    returnModifier, returnVariable, maxLineLen);
    final String typeBCode = this.getFunctionCallB(args, selfName, returnDeclType,
                                                    returnModifier, returnVariable, maxLineLen);

    final int typeA_numLines = typeACode.split(Util.nl()).length;
    final int typeB_numLines = typeBCode.split(Util.nl()).length;

    final int longest_lineA = Util.getLongestLineLen(typeACode);
    final int longest_lineB = Util.getLongestLineLen(typeBCode);

    boolean isAOverLen = false;
    boolean isBOverLen = false;

    if(longest_lineA > maxLineLen) {
      isAOverLen = true;
    }
    if(longest_lineB > maxLineLen) {
      isBOverLen = true;
    }

    //System.err.println(String.format("A: numLines: %d, is_overLen: %s, longestLine: %d, "
    //                                 + "maxLineLen: %d", typeA_numLines,
    //                                 String.valueOf(isAOverLen), longest_lineA, maxLineLen));
    //System.err.println(String.format("B: numLines: %d, is_overLen: %s, longestLine: %d",
    //                                 typeB_numLines, String.valueOf(isBOverLen),longest_lineB));

    String retVal = null;
    if(isAOverLen && ! isBOverLen) {
      retVal = typeBCode;
    } else if(! isAOverLen && isBOverLen) {
      retVal = typeACode;
    } else if(isAOverLen && isBOverLen) {
      if(longest_lineA <= longest_lineB) {
        retVal = typeACode;
      } else {
        retVal = typeBCode;
      }
    }
    if(retVal != null) {
      return retVal;
    }

    if(typeA_numLines < typeB_numLines) {
      return typeACode;
    } else {
      return typeBCode;
    }
  }

  protected String getFunctionCallA(final List<String> args, final String selfName,
                                    final Type returnDeclType, final String returnModifier,
                                    final String returnVariable, final int maxLineLen) {
    final ArrayList<String> firstLineParts = new ArrayList<>();

    Type retType = this.getReturnType();
    if(retType != null && ! retType.isVoid()) {
      String returnString;
      if(returnDeclType != null) {
        returnString = returnDeclType.getTypeName();
      } else {
        returnString = this.getReturnType().getTypeName();
      }

      if(returnModifier != null) {
        returnString = returnModifier + " " + returnString;
      }

      if(!returnString.isEmpty()) {
        firstLineParts.add(returnString);
      }

      if(returnVariable != null) {
        firstLineParts.add(returnVariable);
        firstLineParts.add("=");
      }
    }

    firstLineParts.add(selfName + "." + this.getMethodName() + "(");

    final String line = String.join(" ", firstLineParts);
    final int length = line.length();
    final String successiveIndent = Util.getRepeatedStr(length, " ");

    final String code = Util.wrapJoinTokens(line, maxLineLen, successiveIndent, ", ",
                                            ");", Util.JoinTokenStyle.APPEND, args, false, true,
                                            true);
    return code;
  }

  protected String getFunctionCallB(final List<String> args, final String selfName,
                                    final Type returnDeclType, final String returnModifier,
                                    final String returnVariable, final int maxLineLen) {
    final ArrayList<String> firstLineParts = new ArrayList<>();

    String partA = "";
    Type retType = this.getReturnType();
    if(retType != null && ! retType.isVoid()) {
      final ArrayList<String> declarationParts = new ArrayList<>();

      String returnString;
      if(returnDeclType != null) {
        returnString = returnDeclType.getTypeName();
      } else {
        returnString = this.getReturnType().getTypeName();
      }

      if(returnModifier != null) {
        returnString = returnModifier + " " + returnString;
      }

      if(!returnString.isEmpty()) {
        declarationParts.add(returnString);
      }

      if(returnVariable != null){
        declarationParts.add(returnVariable);
      }

      final String line = String.join(" ", declarationParts);
      partA = line + Util.nl();
    }


    firstLineParts.add(selfName + "." + this.getMethodName() + "(");

    final String indentStr = Util.getRepeatedStr(getIndentSize(), " ");
    String line;
    if(partA.length() > 0) {
      line = indentStr + "= " + String.join(" ", firstLineParts);
    } else {
      line = String.join(" ", firstLineParts);
    }
    final int length = line.length();
    final String successiveIndent = Util.getRepeatedStr(length, " ");

    final String partB = Util.wrapJoinTokens(line, maxLineLen, successiveIndent, ", ",
                                             ");", Util.JoinTokenStyle.APPEND, args, false, true,
                                             true);
    final String ret = partA + partB;
    return ret;
  }

  protected String getDeclaration(final List<String> simpleParamTypesNeedingFullRef,
                                  final boolean noFunctionAccess, final String endToken) {
    final String typeACode = getDeclarationTypeA(simpleParamTypesNeedingFullRef, noFunctionAccess,
                                                 endToken);
    final String typeBCode = getDeclarationTypeB(simpleParamTypesNeedingFullRef, noFunctionAccess,
                                                 endToken);
    final String typeCCode = getDeclarationTypeC(simpleParamTypesNeedingFullRef, noFunctionAccess,
                                                 endToken);

    final int longest_lineA = Util.getLongestLineLen(typeACode);
    final int longest_lineB = Util.getLongestLineLen(typeBCode);
    final int longest_lineC = Util.getLongestLineLen(typeCCode);

    boolean isAOverLen = false;
    boolean isBOverLen = false;
    boolean isCOverLen = false;

    if(longest_lineA > this.maxLineLen) {
      isAOverLen = true;
    }
    if(longest_lineB > this.maxLineLen) {
      isBOverLen = true;
    }
    if(longest_lineC > this.maxLineLen) {
      isCOverLen = true;
    }

    List<String> candidates = new ArrayList<>();
    if(! isAOverLen) {
      candidates.add(typeACode);
    }
    if(! isBOverLen) {
      candidates.add(typeBCode);
    }
    if(! isCOverLen) {
      candidates.add(typeCCode);
    }

    if(candidates.size() == 0) {
      // if zore candidates based on max line len, then they're all candidates
      candidates.add(typeACode);
      candidates.add(typeBCode);
      candidates.add(typeCCode);
    }

    String chosenCode = null;
    int curNumLines = 999;
    for(String candidateCode : candidates) {
      int numLines = candidateCode.split(Util.nl()).length;
      if(numLines < curNumLines) {
        chosenCode = candidateCode;
      }
    }

    if(chosenCode == null) {
      // if still haven't picked one, then choose the first one
      chosenCode = typeACode;
    }

    return chosenCode;
  }

  protected String getDeclarationBase(final List<String> simpleParamTypesNeedingFullRef,
                                      final boolean noFunctionAccess, final String endToken) {
    final int workMaxLineLen = getMaxLineLen() - 3;  // leave a fudge factor

    final Map<String, Boolean> needingFullRefMap = new HashMap<String, Boolean>();
    for(String typeName : simpleParamTypesNeedingFullRef) {
      needingFullRefMap.put(typeName, true);
    }

    final List<String> firstLineParts = new ArrayList<String>();
    if(! noFunctionAccess) {
      firstLineParts.add(getAccessLevel().keyword());
    }
    if(this.isStatic){
      firstLineParts.add("static");
    }
    if(getReturnType() != null) {
      firstLineParts.add(getReturnType().getTypeName());
    }
    firstLineParts.add(getMethodName() + "(");

    final String firstLinePrefix = String.join(" ", firstLineParts);
    final int    firstLineLen    = firstLinePrefix.length();

    final List<String> paramChunks = new ArrayList<String>();
    int paramCnt = getParams().size();

    for(Param param : getParams()) {
      boolean doesNeedFullRef = true;
      if(needingFullRefMap.get(param.getType().getTypeName()) == null) {
        doesNeedFullRef = false;
      }
      String paramStr = param.getParamStr(doesNeedFullRef);
      paramChunks.add(paramStr);
    }

    String useEndToken;
    if(this.exceptions.size() > 0) {
      useEndToken = ")";
    } else {
      useEndToken = ")" + endToken;
    }

    String successiveIndent = Util.getRepeatedStr(firstLineLen, " ");
    if(Util.doesFirstTokenWrap(firstLinePrefix, workMaxLineLen, successiveIndent,
                               ", ", useEndToken, Util.JoinTokenStyle.APPEND,
                               paramChunks, false, true, true)) {
      // our first parameter is too long so we have to indent at a fixed len from left
      successiveIndent = Util.getRepeatedStr(FIXED_METHOD_SIG_LEFT_INDENT, " ");
    }

    final String code = Util.wrapJoinTokens(firstLinePrefix, workMaxLineLen, successiveIndent,
                                            ", ", useEndToken, Util.JoinTokenStyle.APPEND,
                                            paramChunks, false, true, true);
    return code;
  }

  protected String getDeclarationTypeA(final List<String> simpleParamTypesNeedingFullRef,
                                       final boolean noFunctionAccess, final String endToken) {
    String code = getDeclarationBase(simpleParamTypesNeedingFullRef, noFunctionAccess, endToken);
    final String finalCode = addExceptionOnFuncNewLine(code, endToken);
    return finalCode;
  }

  protected String getDeclarationTypeB(final List<String> simpleParamTypesNeedingFullRef,
                                       final boolean noFunctionAccess, final String endToken) {
    String code = getDeclarationBase(simpleParamTypesNeedingFullRef, noFunctionAccess, endToken);
    final String finalCode = addExceptionOnFuncSigAppendLast(code, endToken, false);
    return finalCode;
  }

  protected String getDeclarationTypeC(final List<String> simpleParamTypesNeedingFullRef,
                                       final boolean noFunctionAccess, final String endToken) {
    String code = getDeclarationBase(simpleParamTypesNeedingFullRef, noFunctionAccess, endToken);
    final String finalCode = addExceptionOnFuncSigAppendLast(code, endToken, true);
    return finalCode;
  }

  public String getInterfaceDeclaration(final List<String> simpleParamTypesNeedingFullRef) {
    final String funcDecl = getDeclaration(simpleParamTypesNeedingFullRef, true, ";");
    String javaDoc = getExplicitJavaDoc();
    if(javaDoc == null) {
      javaDoc = createMethodJavaDoc();
    }

    String code = "";
    if(javaDoc != null) {
      code += javaDoc + Util.nl();
    }

    code += funcDecl;

    return code;
  }

  public String getDefinitionCode(final List<String> simpleParamTypesNeedingFullRef,
                                  final boolean noFunctionAccess) {
    final int indentSize = getIndentSize();
    final String indent = Util.getRepeatedStr(indentSize, " ");

    final String funcDecl = getDeclaration(simpleParamTypesNeedingFullRef, noFunctionAccess,
                                           " {");
    String javaDoc = getExplicitJavaDoc();
    if(javaDoc == null) {
      javaDoc = createMethodJavaDoc();
    }

    String code = "";
    if(javaDoc != null) {
      code += javaDoc + Util.nl();
    }

    if(annotations.size() > 0) {
      final String annotationsTxt = String.join(Util.nl(), annotations);
      code += annotationsTxt + Util.nl();
    }

    code += funcDecl + Util.nl();

    final String body = getBody();
    if(body == null) {
      throw new RuntimeException("no function body defined for func: " + getFullTypeSigString());
    }
    if(body.length() > 0) {
      final int bodyIndentSize = indentSize;
      final String indentedBody = Util.indentLines(bodyIndentSize, body);
      code += indentedBody + Util.nl();
    }

    code += "}" + Util.nl();
    return code;
  }

  String createMethodJavaDoc() {
    final String javaDocReturnDesc = getJavaDocReturnDesc();

    Type retType = this.getReturnType();
    if(retType != null && ! retType.isVoid()) {
      if(javaDocReturnDesc == null) {
        throw new RuntimeException("No javadoc return description provided on method "
                                   + getMethodName() + " for return type "
                                   + returnType + ".");
      }
    }

    final List<String> descLinesFmt = new ArrayList<String>();
    if(getJavaDocDescLines().size() == 0) {
      throw new RuntimeException("No javadoc description line on method " + getMethodName());
    }
    for(String descLine : getJavaDocDescLines()) {
      descLinesFmt.add(" * " + descLine);
    }
    final String descLinesTxt = String.join(Util.nl(), descLinesFmt);

    int maxFieldLen = 0;
    for(Param p : getParams()) {
      final int paramLen = p.getParamName().length();
      if(paramLen > maxFieldLen) {
        maxFieldLen = paramLen;
      }
    }

    String retLine = "";
    if(javaDocReturnDesc != null && javaDocReturnDesc.length() > 0) {
      retLine = String.format(" * @return %s%s",
                              Util.getRepeatedStr(maxFieldLen + 1, " "),
                              javaDocReturnDesc);
      retLine = retLine;
    }

    final List<String> paramLines = new ArrayList<String>();
    for(Param p : getParams()) {
      String paramDesc = p.getJavaDocParamDesc();
      if(paramDesc == null) {
        paramDesc = "This is the " + p.getParamName() + " field.";
      }
      final String fieldNameFmt = String.format("%-" + maxFieldLen + "s", p.getParamName());
      final String javaDocLine  = String.format(" * @param %s  %s", fieldNameFmt, paramDesc);
      if(javaDocLine.length() > getMaxLineLen()) {
        String javaDocL1 = String.format(" * @param %s", fieldNameFmt);
        paramLines.add(javaDocL1);
        String javaDocL2 = String.format(" *        %s", paramDesc);
        paramLines.add(javaDocL2);
      } else {
        paramLines.add(javaDocLine);
      }
    }

    final String paramTxt = String.join(Util.nl(), paramLines);

    final List<String> javaDocParts = new ArrayList<String>();
    if(descLinesTxt.length() > 0) {
      javaDocParts.add(descLinesTxt);
    }

    if(paramTxt.length() > 0 || retLine.length() > 0) {
      if(javaDocParts.size() > 0) {
        javaDocParts.add(" *");  // empty line
      }
    }

    if(paramTxt.length() > 0) {
      javaDocParts.add(paramTxt);
    }

    if(retLine.length() > 0) {
      javaDocParts.add(retLine);
    }

    final String javaDoc =  "/**"                              + Util.nl()
                          + String.join(Util.nl(), javaDocParts) + Util.nl()
                          + " */";
    return javaDoc;
    //Util.indentLines(final int indentSize, final String txt) {
    //final String finalJavaDoc = javaDoc;
  }

  protected String addExceptionOnFuncNewLine(final String code, String endToken) {
    if(this.exceptions.size() == 0) {
      return code;
    }

    final List<String> exceptionNames = getExceptionNames();

    final String indent       = Util.getRepeatedStr(getIndentSize() * 2, " ");
    final String firstLinePre = indent + "throws ";
    final int preLen          = firstLinePre.length();
    final String successiveIndent = Util.getRepeatedStr(preLen, " ");

    final String exceptionTxt
      = Util.wrapJoinTokens(firstLinePre, getMaxLineLen(), successiveIndent, ", ", endToken,
                            Util.JoinTokenStyle.APPEND, exceptionNames, false, true, true);

    final String retCode = code + Util.nl() + exceptionTxt;
    return retCode;
  }

  protected String addExceptionOnFuncSigAppendLast(final String code, String endToken,
                                                   boolean indentFixed) {
    if(this.exceptions.size() == 0) {
      return code;
    }

    final List<String> exceptionNames = getExceptionNames();
    List<String> tokens = new ArrayList<>();
    for(int i = 0; i < exceptionNames.size(); i++) {
      String exception = exceptionNames.get(i);
      boolean isFirst = (i == 0);
      if(isFirst) {
        exception = " throws " + exception;
      }
      tokens.add(exception);
    }

    final List<String> codeLines = Arrays.asList(code.split(Util.nl()));
    if(codeLines.size() == 0) {
      throw new RuntimeException("unexpected, no code lines");
    }

    final String indent   = Util.getRepeatedStr(getIndentSize(), " ");
    final int numLines    = codeLines.size();
    final String lastLine = codeLines.get(numLines - 1);
    String allButLast = "";
    if(numLines >= 2) {
      final List<String> subList = codeLines.subList(0, numLines - 1);
      allButLast = String.join(Util.nl(), subList);
    }

    final String firstLinePre = lastLine;
    String successiveIndent = null;
    if(indentFixed) {
      successiveIndent = Util.getRepeatedStr(FIXED_METHOD_SIG_LEFT_INDENT, " ");
    } else {
      successiveIndent = Util.getRepeatedStr(getIndentSize() * 2, " ");
    }

    final String exceptionTxt
      = Util.wrapJoinTokens(firstLinePre, getMaxLineLen(), successiveIndent, ", ", endToken,
                            Util.JoinTokenStyle.APPEND, tokens, false, true, true);

    List<String> parts = new ArrayList<>();
    if(allButLast.length() > 0) {
      parts.add(allButLast);
    }
    parts.add(exceptionTxt);

    final String retCode = String.join(Util.nl(), parts);
    return retCode;
  }

}



