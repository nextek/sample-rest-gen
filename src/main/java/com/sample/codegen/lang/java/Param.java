package com.sample.codegen.lang.java;

import java.util.ArrayList;
import java.util.List;

import com.sample.codegen.Util;

public class Param {
  private String  paramName;
  private Type    type;
  private boolean isStatic = false;
  private boolean isFinal  = false;
  private String  fullName;  // i.e. importRef
  private String  javaDocParamDesc;
  private String  label;
  private List<String> importRefs  = new ArrayList<String>();
  private List<String> annotations = new ArrayList<String>();

  public Param(final String paramName, final Type type) {
    this.paramName = paramName;
    this.type      = type;
    if(type == null) {
      throw new RuntimeException("can't pass null type for Param");
    }
  }

  public Param(final String paramName, final String typeName, final String fullName) {
    this.paramName = paramName;
    this.type      = new SimpleType(typeName, fullName);
    if(typeName == null) {
      throw new RuntimeException("can't pass null typeName for Param");
    }
  }

  public void setParamName(final String n) {
    paramName = n;
  }
  public void setIsStatic(final boolean b) {
    isStatic  = b;
  }
  public void setIsFinal(final boolean b) {
    isFinal   = b;
  }
  public void setJavaDocParamDesc(final String d) {
    javaDocParamDesc = d;
  }
  public void setLabel(final String l) {
    label = l;
  }
  public void addImportRef(final String a) {
    Util.assertValidImportRef(a);
    importRefs.add(a);
  }
  public void addImportRefs(final List<String> list) {
    for(String r : list) {
      addImportRef(r);
    }
  }

  public void addAnnotation(final String a) {
    annotations.add(a);
  }
  public void addAnnotations(final List<String> list) {
    for(String a : list) {
      addAnnotation(a);
    }
  }

  public String getParamName() {
    return paramName;
  }
  public Type getType() {
    return type;
  }
  public String getFullName() {
    return type.getFullName();
  }
  public String getLabel() {
    return label;
  }
  public String getJavaDocParamDesc() {
    return javaDocParamDesc;
  }

  public List<String> getAllImportRefs() {
    final List<String> ret = new ArrayList<String>();
    ret.addAll(this.importRefs);
    ret.addAll(type.getAllImportRefs());
    return ret;
  }

  public String getParamStr(final boolean useFullType) {
    final List<String> parts = new ArrayList<String>();

    parts.addAll(annotations);
    if(isFinal) {
      parts.add("final");
    }
    String useType = type.getTypeName();
    if(useFullType) {
      useType = type.getFullName();
    }
    parts.add(useType);
    parts.add(paramName);

    final String retStr = String.join(" ", parts);
    return retStr;
  }

}


