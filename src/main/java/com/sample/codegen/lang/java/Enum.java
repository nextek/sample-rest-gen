package com.sample.codegen.lang.java;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sample.codegen.Util;

public class Enum extends LClass implements com.sample.codegen.lang.Enum {
  private String              name;
  private List<String>        values;
  private Map<String, String> value2constant = new HashMap<>();

  public Enum(String name, List<String> values){
    super(name);
    this.name = name;
    this.values = values;

    for(String value : values){
      String constant = value.toUpperCase().replaceAll("-", "_");
      value2constant.put(value, constant);
    }

    prepare();
  }

  public Enum(String name) {
    super(name);
    this.name = name;
    this.values = new ArrayList<>();

    prepare();
  }

  public String getName() {
    return name;
  }

  public List<String> getValues() {
    return values;
  }

  public String getConstantFromValue(String value) {
    return value2constant.get(value);
  }

  private void prepare(){
    Field field = new Field("value", AccessModifier.PRIVATE,
                               this.getMethodMaxLineLen(), this.getIndentSize());
    field.setType(SimpleType.getStringType());
    this.addField(field);

    Method constructor = new Method(this.getClassName(), AccessModifier.NONE,
                                       this.getMethodMaxLineLen(), this.getIndentSize());
    Param p = new Param("value", "String", null);
    p.setIsFinal(true);
    constructor.addParam(p);
    constructor.setBody("this.value = value;");
    constructor.setReturnType(null);

    Method getter = new Method("getValue", AccessModifier.PUBLIC, this.getMethodMaxLineLen(),
                                  this.getIndentSize());
    getter.setBody("return this.value;");
    getter.setReturnType(SimpleType.getStringType());
    getter.setJavaDocReturnDesc("String representation of " + this.getClassName());

    this.addMethod(constructor);
    this.addMethod(getter);
  }

  // This is stolen from LClass and simplified for enums
  @Override()
  protected String getClassLine(List<String> imports) {
    return "public enum " + this.getClassName() + " {";
  }

  @Override()
  protected String getFieldsText(GenerationOptions op, List<String> imports) {
    StringBuilder stringBuilder = new StringBuilder();

    List<String> parts = new ArrayList<>();
    for(String value : values){
      String constant = this.getConstantFromValue(value);
      parts.add("/**");
      parts.add(" * Enum value for '" + value + "'.");
      parts.add(" */");
      parts.add(constant + "(\"" + value + "\"),");
    }
    String enumValuesText = String.join(Util.nl(), parts) + ";";
    enumValuesText = Util.indentLines(getIndentSize(), enumValuesText);
    stringBuilder.append(enumValuesText.substring(0, enumValuesText.length() - 2) + ";");
    stringBuilder.append(Util.nl());
    stringBuilder.append(Util.nl());

    stringBuilder.append(super.getFieldsText(op, imports));
    return stringBuilder.toString();
  }

}
