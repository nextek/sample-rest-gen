package com.sample.codegen.lang.java;

import java.util.ArrayList;
import java.util.List;

import com.sample.codegen.Util;

abstract public class Type {
  private List<String> importRefs  = new ArrayList<String>();

  public void addImportRef(final String a) {
    Util.assertValidImportRef(a);
    importRefs.add(a);
  }
  public void addImportRefs(final List<String> list) {
    for(String r : list) {
      addImportRef(r);
    }
  }
  public List<String> getImportRefs() {
    return importRefs;
  }
  
  abstract public List<String> getAllImportRefs();
  abstract public String getTypeName();
  abstract public String getFullName(); // e.g. java.util.List<com.sample.soa.model.Guest>
  abstract public boolean isVoid();
  abstract public boolean isString();
  abstract public boolean isComposite();
}

