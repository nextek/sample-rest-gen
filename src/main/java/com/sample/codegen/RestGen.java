package com.sample.codegen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.sample.codegen.config.Configuration;
import com.sample.codegen.config.Configuration.HysterixInfo;
import com.sample.codegen.restgen.ApiGen;
import com.sample.codegen.restgen.EnumGen;
import com.sample.codegen.restgen.ExternalGen;
import com.sample.codegen.restgen.ModelGen;
import com.sample.codegen.restgen.NamingUtil;
import com.sample.codegen.restgen.PackageInfoGen;
import com.sample.codegen.restgen.ServiceGen;
import com.sample.codegen.restgen.StaticGen;
import com.sample.codegen.restgen.TestGen;
import com.sample.codegen.restgen.ValidateGen;

import io.swagger.models.Info;
import io.swagger.models.Path;
import io.swagger.models.Swagger;

public class RestGen {
  static private final String EXTERNAL_DEP_FIELD = "x-external-dependencies";
  private Configuration config;

  private Swagger     swagger;
  private ModelGen    modelGen;
  private ValidateGen validateGen;
  private ApiGen      apiGen;
  private StaticGen   staticGen;
  private ServiceGen  serviceGen;
  private ExternalGen externalGen;
  private TestGen     testGen;

  public RestGen(final Swagger swagger, final Configuration configuration) {
    this.swagger = swagger;
    this.config  = configuration;

    updateConfig();

    EnumGen.getInstance().init(swagger, configuration);
    setupNamingUtil();

    modelGen    = new ModelGen(swagger, configuration);
    serviceGen  = new ServiceGen(swagger, configuration);
    validateGen = new ValidateGen(swagger, configuration, modelGen);
    apiGen      = new ApiGen(swagger, configuration, modelGen, serviceGen, validateGen);
    externalGen = new ExternalGen();
    staticGen   = new StaticGen(swagger, configuration);
    
    if(this.config.isDoGenerateTestCode()) {
      testGen = new TestGen(swagger, configuration, apiGen, modelGen, serviceGen, validateGen);
    }
  }

  private void loadHysterixDependencies() {
    final Info info = this.swagger.getInfo();
    if(info != null) {
      final Map<String, Object> vendorExt = info.getVendorExtensions();
      if(vendorExt != null) {
        Object o = vendorExt.get(EXTERNAL_DEP_FIELD);
        if(o != null) {
          if(! o.getClass().getName().equals("java.util.ArrayList")) {
            throw new RuntimeException(EXTERNAL_DEP_FIELD + " is expecting an array");
          }

          ArrayList list = (ArrayList)o;
          List<HysterixInfo> hysterixDependencies = new ArrayList<>();
          for(Object s : list) {
            String itemType = s.getClass().getName();
            if(! itemType.equals("com.fasterxml.jackson.databind.node.ObjectNode")) {
              throw new RuntimeException(EXTERNAL_DEP_FIELD +" is expecting list items as hashes");
            }
            ObjectNode node = (ObjectNode)s;

            Iterator<String> fieldNames = node.fieldNames();
            String commandName = null;
            String resourceName = null;
            while(fieldNames.hasNext()){
              String fieldName = fieldNames.next();
              JsonNode jsonNodeValue = node.get(fieldName);
              String value = jsonNodeValue.asText();
              if(fieldName.equals("command")) {
                commandName = value;
              } else if(fieldName.equals("resource")) {
                resourceName = value;
              }
            }
            if(commandName == null) {
              throw new RuntimeException(EXTERNAL_DEP_FIELD +" is expecting field of name");
            }
            if(resourceName == null) {
              throw new RuntimeException(EXTERNAL_DEP_FIELD +" is expecting field of resource");
            }
            HysterixInfo hystInfo = new HysterixInfo(commandName, resourceName);
            hysterixDependencies.add(hystInfo);
          }

          this.config.setHysterixDependencies(hysterixDependencies);
          this.config.setIsComposite(true);
        }
      }
    }
  }

  protected void updateConfig() {
    String svcName = config.getServiceName();
    if(svcName == null || svcName.length() == 0) {
      svcName = findServiceNameInSwagger();
      if(svcName == null) {
        throw new RuntimeException("can't find service name in swagger");
      }
      config.setServiceName(svcName);
    }

    final Info info = this.swagger.getInfo();
    String swaggerVersion = null;
    if(info != null) {
      swaggerVersion = info.getVersion();
    }
    if(swaggerVersion == null) {
      throw new RuntimeException("Info.version field is not defined but is required to "
                               + "be defined in the swagger file");
    }
    config.setSwaggerVersion(swaggerVersion);

    loadHysterixDependencies();
  }

  public Swagger     getSwagger()     {
    return swagger;
  }
  public ModelGen    getModelGen()    {
    return modelGen;
  }
  public ValidateGen getValidateGen() {
    return validateGen;
  }
  public ApiGen      getApiGen()      {
    return apiGen;
  }
  public ServiceGen  getServiceGen()  {
    return serviceGen;
  }
  public StaticGen   getStaticGen()   {
    return staticGen;
  }

  private void setupNamingUtil() {
    if(this.swagger == null) {
      throw new RuntimeException("Failed to parse your swagger file. Please check your swagger "
                               + "file for correctnes");
    }
    this.swagger.getPaths().forEach((s, path) -> {
        NamingUtil.registerPath(s);
      });
  }

  public void generate() {
    String svcSubdirName = config.getServiceName().toLowerCase();
    String svcPkgToken   = config.getSvcPkgToken();
    final String serviceDir = String.format("%s/%s-svc", config.getOutdir(), svcSubdirName);
    final String srcSvcDir  = serviceDir + "/src/main/java/com/sample/soa/" + svcPkgToken;
    final String testSvcDir = serviceDir + "/src/test/java/com/sample/soa/" + svcPkgToken;
    final String pkgRoot    = "com.sample.soa." + svcPkgToken;

    makeEmptyDirs(serviceDir);

    Util.writeGeneratedFile(serviceDir + "/rest-gen-config.txt", config.toString());

    getModelGen().generate(srcSvcDir, pkgRoot);
    getValidateGen().generate(srcSvcDir, pkgRoot);
    getApiGen().generate(srcSvcDir, pkgRoot);
    getServiceGen().generate(srcSvcDir, pkgRoot);
    getStaticGen().generate(serviceDir);
    EnumGen.getInstance().generate(srcSvcDir, pkgRoot);
    this.externalGen.generate(srcSvcDir, pkgRoot);
    PackageInfoGen.getInstance().generate(srcSvcDir);
    if(this.config.isDoGenerateTestCode()) {
      testGen.generate(testSvcDir, pkgRoot);
    }
  }

  protected void makeEmptyDirs(String serviceDir) {
    Util.makeDirs(serviceDir + "/src/main/resources");
    Util.makeDirs(serviceDir + "/src/test/java");
    Util.makeDirs(serviceDir + "/src/test/resources");
  }

  protected String findServiceNameInSwagger() {
    final String basePath = getSwagger().getBasePath();

    if(basePath != null) {
      final String[] partsRaw = basePath.split("/");
      List<String> parts = Arrays.asList(partsRaw);

      if(parts.size() > 0) {
        parts = parts.subList(1, parts.size());  // remove initial empty part
      }
      if(parts.size() > 0) {
        String wantPart = null;
        PARTS_LOOP_A:
        for(String part : parts) {
          if(part.matches("(^api.*|^.*SNAPSHOT*)")) {
            continue PARTS_LOOP_A;
          }
          wantPart = part;
          break PARTS_LOOP_A;
        }
        if(wantPart != null) {
          return wantPart;
        }
      }
    }

    final List<String> paths = new ArrayList<String>();
    final Map<String, Path> pathsMap = swagger.getPaths();
    paths.addAll(pathsMap.keySet());

    String commonPart = null;
    PATH_LOOP:
    for(String path: paths) {
      final String[] partsRaw  = path.split("/");
      List<String> parts = Arrays.asList(partsRaw);

      if(parts.size() > 0) {
        parts = parts.subList(1, parts.size());  // remove initial empty part
      }
      if(parts.size() == 0) {
        continue PATH_LOOP;
      }

      String wantPart = null;
      PARTS_LOOP_B:
      for(String part : parts) {
        if(part.matches("(^api.*|^.*SNAPSHOT*)")) {
          continue PARTS_LOOP_B;
        }
        wantPart = part;
        break PARTS_LOOP_B;
      }
      if(wantPart != null) {
        return wantPart;
      }

      if(commonPart == null) {
        commonPart = wantPart;
      } else if(! commonPart.equals(wantPart)) {
        throw new RuntimeException("finding different initial path urls, can't find service name");
      }
    }

    return commonPart;
  }

}
