package com.sample.codegen.config;

import java.util.ArrayList;
import java.util.List;

public class Configuration {
  public static class HysterixInfo {
    private String commandName;
    private String resource;

    public HysterixInfo(String commandName, String resource) {
      this.commandName = commandName;
      this.resource    = resource;
    }
    public String getCommandName() {
      return commandName;
    }
    public String getResource() {
      return resource;
    }
  }

  private static Configuration configSingletonObj;

  private String   cmdLine;
  private String   swagger;
  private String   outdir;
  private String   serviceName;
  private String   svcClassName;
  private String   svcPkgToken;
  private String   nexusUrl;
  private String   ppBasePath;
  private String   ppGroupIdPath;
  private String   ppArtifactId;
  private String   parentPomVersion;
  private Database database;
  private boolean  isComposite;
  private List<HysterixInfo> hysterixDependencies = new ArrayList<>();

  private boolean checkstylePom;
  private String checkstylePath;

  private boolean doDebug;

  private boolean doGenerateTestCode;

  // Extra values that serve as defaults
  private static final String PROGRAM_NAME = "sample_rest_gen";

  private String swaggerVersion;
  private String releaseVersion;
  private boolean maskVersions;

  public Configuration(final String cmdLine, final String swagger, final String outdir,
                       final String serviceName,
                       final String svcClassName, final String svcPkgToken,
                       final String databaseStr, final boolean doTest,
                       final boolean doDebug, final String nexusUrl, final String ppBasePath,
                       final String ppGroupIdPath, final String ppArtifactId,
                       final String parentPomVersion, final String releaseVersion,
                       final boolean maskVersions) {
    this.cmdLine            = cmdLine;
    this.swagger            = swagger;
    this.outdir             = outdir;
    this.serviceName        = serviceName;
    this.svcClassName       = svcClassName;
    this.svcPkgToken        = svcPkgToken;
    this.database           = Database.fromString(databaseStr);
    this.checkstylePom      = checkstylePom;
    this.checkstylePath     = checkstylePath;
    this.doGenerateTestCode = doTest;
    this.doDebug            = doDebug;
    this.nexusUrl           = nexusUrl;
    this.ppBasePath         = ppBasePath;
    this.ppGroupIdPath      = ppGroupIdPath;
    this.ppArtifactId       = ppArtifactId;
    this.parentPomVersion   = parentPomVersion;
    this.releaseVersion     = releaseVersion;
    this.maskVersions       = maskVersions;
  }

  public static Configuration getConfig() {
    return configSingletonObj;
  }

  public static void registerConfig(final Configuration c) {
    configSingletonObj = c;
  }

  public String getSwagger() {
    return swagger;
  }

  public String getOutdir() {
    return outdir;
  }

  public String getServiceName() {
    return serviceName;
  }

  public String getSvcClassName() {
    return svcClassName;
  }
  
  public String getSvcPkgToken() {
    return svcPkgToken;
  }

  public Database getDatabase() {
    return database;
  }

  public boolean isComposite() {
    return isComposite;
  }

  public List<HysterixInfo> getHysterixDependencies() {
    return hysterixDependencies;
  }

  public boolean getMaskVersions() {
    return maskVersions;
  }

  public void setServiceName(final String serviceName) {
    this.serviceName = serviceName;
  }

  public void setSwaggerVersion(final String swaggerVersion) {
    this.swaggerVersion = swaggerVersion;
  }

  public boolean isCheckstylePom() {
    return checkstylePom;
  }

  public String getCheckstylePath() {
    return checkstylePath;
  }

  public boolean isDoDebug() {
    return doDebug;
  }

  public String getNexusUrl() {
    return nexusUrl;
  }

  public String getPPBasePath() {
    return ppBasePath;
  }

  public String getPPGroupIdPath() {
    return ppGroupIdPath;
  }

  public String getPPArtifactId() {
    return ppArtifactId;
  }

  public String getParentPomVersion() {
    return parentPomVersion;
  }

  public String getJavaDocAuthor() {
    String relVersionFmt = null;
    if(this.maskVersions) {
      relVersionFmt = "***";
    } else if(this.releaseVersion == null) {
      relVersionFmt = "undetermined release version";
    } else {
      relVersionFmt = "v" + this.releaseVersion;
    }

    String ret = PROGRAM_NAME + " " + relVersionFmt;
    return ret;
  }

  public String getJavaDocVersion() {
    String swaggerVersionFmt = null;
    if(this.maskVersions) {
      swaggerVersionFmt = "***";
    } else if(this.swaggerVersion == null) {
      swaggerVersionFmt = "undetermined swagger version";
    } else {
      if(this.swaggerVersion.matches("^v.*")) {
        swaggerVersionFmt = this.swaggerVersion;
      } else {
        swaggerVersionFmt = "v" + this.swaggerVersion;
      }
    }
    return swaggerVersionFmt;
  }

  public boolean isDoGenerateTestCode() {
    return doGenerateTestCode;
  }

  public void setIsComposite(boolean v) {
    isComposite = v;
  }

  public void setHysterixDependencies(List<HysterixInfo> l) {
    hysterixDependencies = l;
  }

  public String getSwaggerVersion() {
    return swaggerVersion;
  }

  public String getReleaseVersion() {
    return releaseVersion;
  }

  @Override()
  public String toString() {
    return "Configuration{"
      + "cmdLine=" + quoteString(cmdLine)
      + ", swagger=" + quoteString(swagger)
      + ", outdir=" + quoteString(outdir)
      + ", serviceName=" + quoteString(serviceName)
      + ", checkstylePom=" + checkstylePom
      + ", checkstylePath=" + quoteString(checkstylePath)
      + ", doDebug=" + doDebug
      + ", nexusUrl=" + quoteString(nexusUrl)
      + ", ppBasePath=" + quoteString(ppBasePath)
      + ", ppGroupIdPath=" + quoteString(ppGroupIdPath)
      + ", ppArtifactId=" + quoteString(ppArtifactId)
      + ", parentPomVersion=" + quoteString(parentPomVersion)
      + ", isComposite=" + isComposite
      + ", database=" + (database != null ? quoteString(database.toString()) : "null")
      + ", swaggerVersion=" + quoteString(swaggerVersion)
      + ", releaseVersion=" + quoteString(releaseVersion)
      + ", maskVersions=" + maskVersions
      + ", doGenerateTestCode=" + doGenerateTestCode
      + ", svcPkgToken=" + quoteString(svcPkgToken)
      + ", svcClassName=" + quoteString(svcClassName)
      + '}';
  }

  private String quoteString(String v) {
    if(v == null) {
      return v;
    }
    return "'" + v + "'";
  }
  
  public enum Database {
    COUCHBASE,
    MARIADB;

    public static Database fromString(final String text) {
      if(text == null) {
        return null;
      }
      for(Database d : Database.values()) {
        if (d.name().equalsIgnoreCase(text)) {
          return d;
        }
      }
      return null;
    }
    public String toString() {
      return name();
    }
  }
}

