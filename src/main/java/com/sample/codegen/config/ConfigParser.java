package com.sample.codegen.config;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.sample.codegen.Util;
import com.sample.codegen.config.HelpException;
import com.sample.codegen.restgen.staticgen.StaticHelper;

public class ConfigParser {
  private static final String DEFAULT_NEXUS_URL       = "http://localhost:8081/nexus/content";
  // PP stands for parent pom
  private static final String DEFAULT_PP_BASE_PATH    = "repositories/bharath";
  private static final String DEFAULT_PP_GROUPID_PATH = "com/sample/maven";
  private static final String DEFAULT_PP_ARTIFACT_ID  = "DropWizardGlobals";
  private CommandLineParser parser = new DefaultParser();
  private Options options = new Options();

  public ConfigParser(){
    options.addOption(Option.builder("s")
                      .longOpt("swagger")
                      .desc("where <file> is a yaml file")
                      .argName("file")
                      .hasArg()
                      .required(true)
                      .build());
    options.addOption(Option.builder("o")
                      .longOpt("outdir")
                      .desc("writes java model code to <dir>")
                      .argName("dir")
                      .hasArg()
                      .required(true)
                      .build());
    options.addOption(Option.builder("n")
                      .longOpt("service_name")
                      .desc("what to name the service (e.g. stays) if not given will "
                            + "attempted to be auto-determined from swagger")
                      .hasArg()
                      .build());
    options.addOption(Option.builder("d")
                      .longOpt("database")
                      .desc("supported options: couchbase or mariadb")
                      .hasArg()
                      .build());
    // ------------------------------------------
    // // Not using checkstyle currently
    //options.addOption(Option.builder("c")
    //                          .longOpt("checkstyle_pom")
    //                          .desc("generate pom.xml with the checkstyle plugin")
    //                          .build());
    //options.addOption(Option.builder()
    //                          .longOpt("checkstyle_path")
    //                          .desc("where <path> is the checkstyle xml file is located if not "
    //                                + "passed will default to ~/dev-config/dmx-checkstyle.xml")
    //                          .argName("path")
    //                          .hasArg()
    //                          .build());
    // ------------------------------------------
    options.addOption(Option.builder()
                      .longOpt("parentPomVersion")
                      .desc("Parent pom version to use. Only pass if no connection to nexus.")
                      .hasArg()
                      .build());
    options.addOption(Option.builder("t")
                      .longOpt("test")
                      .desc("generate project with generic test code")
                      .build());
    options.addOption(Option.builder("X")
                      .longOpt("debug")
                      .desc("will write debug info to .dbg file")
                      .build());
    options.addOption(Option.builder("m")
                      .longOpt("maskversions")
                      .desc("mask versions outputted in code")
                      .build());
    options.addOption(Option.builder("h")
                      .longOpt("help")
                      .desc("usage displayed")
                      .build());
  }

  public Configuration parse(final String[] args) throws HelpException, ParseException {
    List<String> argsList = Arrays.asList(args);
    String cmdLine = String.join(" ", argsList);
    // Finding help in a manual way because CommandLineParser.parse does not support just parsing
    // and not throwing missing option exceptions.  I want to mark options as required, but I
    // also want to show help usage when requested without an error message.
    if( argsList.contains("--help") || argsList.contains("-h")) {
      throw new HelpException("--help requested");
    }

    CommandLine optionalLine = this.parser.parse(this.options, args, false);
    if(optionalLine.hasOption("h")) {
      throw new ParseException("--help requested");
    }

    final CommandLine requiredLine = this.parser.parse(this.options, args, false);

    String swagger          = "";
    String outdir           = "";
    String database         = "";
    String serviceName      = "";
    String nexus            = "";
    String parentPomVersion = "";

    if(requiredLine.hasOption("s")) {
      swagger = requiredLine.getOptionValue("s");
    } else {
      throw new ParseException("Missing --swagger option");
    }

    if(requiredLine.hasOption("o")) {
      outdir = requiredLine.getOptionValue("o");
    } else {
      throw new ParseException("Missing --outdir option");
    }

    if(optionalLine.hasOption("d")) {
      database = optionalLine.getOptionValue("d");
    }

    if(optionalLine.hasOption("n")) {
      serviceName = optionalLine.getOptionValue("n");
    }

    String genNexusUrl      = System.getenv("GEN_NEXUS_URL");
    String genPPBasePath    = System.getenv("GEN_PP_BASE_PATH");
    String genPPGroupIdPath = System.getenv("GEN_PP_GROUPID_PATH");
    String genPPArtifactId  = System.getenv("GEN_PP_ARTIFACT_ID");

    String nexusUrl = null;
    if(genNexusUrl != null) {
      nexusUrl = genNexusUrl;
    } else{
      nexusUrl = this.DEFAULT_NEXUS_URL;
    }

    String ppBasePath = null;
    if(genPPBasePath != null) {
      ppBasePath = genPPBasePath;
    } else{
      ppBasePath = this.DEFAULT_PP_BASE_PATH;
    }

    String ppGroupIdPath = null;
    if(genPPGroupIdPath != null) {
      ppGroupIdPath = genPPGroupIdPath;
    } else {
      ppGroupIdPath = this.DEFAULT_PP_GROUPID_PATH;
    }

    String ppArtifactId = null;
    if(genPPGroupIdPath != null) {
      ppArtifactId = genPPArtifactId;
    } else{
      ppArtifactId = this.DEFAULT_PP_ARTIFACT_ID;
    }

    if(optionalLine.hasOption("parentPomVersion")) {
      parentPomVersion = optionalLine.getOptionValue("parentPomVersion");
    } else {
      parentPomVersion = StaticHelper.getParentVersion(nexusUrl, ppBasePath, ppGroupIdPath,
                                                       ppArtifactId);
    }

    boolean maskVersions = optionalLine.hasOption("m");

    boolean doDebug = optionalLine.hasOption("X");
    boolean doTest  = optionalLine.hasOption("t");

    //------------------------------------
    //            VALIDATION
    //------------------------------------
    //handle tilde
    if(swagger.startsWith("~")){
      swagger = System.getProperty("user.home") + swagger.substring(1);
    }
    if(outdir.startsWith("~")){
      outdir = System.getProperty("user.home") + outdir.substring(1);
    }

    //swagger must exist
    if(! new File(swagger).exists()){
      throw new ParseException("Swagger file '" + swagger + "' does not exist.");
    }
    //replace('-', '_');
    final String svcPkgToken  = serviceName.toLowerCase().replaceAll("[^a-zA-Z_]", "");
    final String svcClassName = Util.ucFirst(svcPkgToken);

    final String releaseVersion = getManifestReleaseVersion();

    return new Configuration(cmdLine, swagger, outdir, serviceName, svcClassName, svcPkgToken,
                             database, doTest, doDebug, nexusUrl, ppBasePath, ppGroupIdPath,
                             genPPArtifactId, parentPomVersion, releaseVersion, maskVersions);
  }

  public String getManifestReleaseVersion() {
    String retVersion = null;
    try {
      Enumeration<URL> resources
        = getClass().getClassLoader().getResources("META-INF/MANIFEST.MF");
      URL mainJarResource = null;

      RESOURCE_LOOP:
      while (resources.hasMoreElements()) {
        URL resource = resources.nextElement();
        String manifestFile = resource.getFile();
        if(manifestFile.matches(".*\\!.*?\\!.*")) {
          // we only want top level resources
          continue RESOURCE_LOOP;
        }
        if(manifestFile.matches(".*sample-rest-gen.*\\.jar.*")) {
          mainJarResource = resource;
        }
      }

      if(mainJarResource != null) {
        Manifest manifest = new Manifest(mainJarResource.openStream());
        Attributes attr = manifest.getMainAttributes();
        retVersion = attr.getValue("ImplementationVersion");
      }
    } catch (IOException E) {
      throw new RuntimeException("manifest exception");
    }

    return retVersion;
  }

  public void showHelp() {
    final String pgmDesc = "generates java REST code from swagger yaml file.";
    String executable = getExecutable();
    executable = executable.replace(".jar", ".one-jar.jar");
    String cmdSyntax = executable + " [options]";

    final HelpFormatter formatter = new HelpFormatter();

    Options requiredOpts = new Options();
    List<String> helpLines;
    Collection<Option> allOptions = this.options.getOptions();
    Options requiredOptions = new Options();
    Options optionalOptions = new Options();
    for(Option o : allOptions) {
      if(o.isRequired()) {
        requiredOptions.addOption(o);
      } else {
        optionalOptions.addOption(o);
      }
    }

    System.err.println("Usage: " + cmdSyntax);
    System.err.println("       " + pgmDesc);
    formatter.setOptionComparator(null);  // output options in order defined
    formatter.setSyntaxPrefix("");
    String requiredHeader = "=== REQUIRED OPTIONS ===============================";
    formatter.printHelp(" ", requiredHeader, requiredOptions, Util.nl());

    String optionalHeader = "=== OPTIONAL OPTIONS ===============================";
    formatter.printHelp(" ", optionalHeader, optionalOptions, "");

    System.err.println("");
    String envVarHeader = "=== OPTIONAL ENV VARIABLES ===========================";
    System.err.println(envVarHeader);
    System.err.println("  * GEN_NEXUS_URL       : default = " + DEFAULT_NEXUS_URL);
    System.err.println("  * GEN_PP_BASE_PATH    : default = " + DEFAULT_PP_BASE_PATH);
    System.err.println("  * GEN_PP_GROUPID_PATH : default = " + DEFAULT_PP_GROUPID_PATH);
    System.err.println("  * GEN_PP_ARTIFACT_ID  : default = " + DEFAULT_PP_ARTIFACT_ID);
    System.err.println("    note: PP stands for Parent Pom");
  }

  private String getExecutable() {
    return (new File(ConfigParser.class.getProtectionDomain().getCodeSource().getLocation()
                    .getPath())).getName();
  }
}
