package com.sample.codegen;

import java.io.File;

import org.apache.commons.cli.ParseException;

import com.sample.codegen.config.ConfigParser;
import com.sample.codegen.config.Configuration;
import com.sample.codegen.config.HelpException;

import io.swagger.models.Swagger;
import io.swagger.parser.SwaggerParser;

public class SamplemssGenerator {

  public SamplemssGenerator() {
  }

  public void processSwagger(final Swagger swagger, final Configuration configuration) {
    final RestGen restGen = new RestGen(swagger, configuration);
    restGen.generate();
  }

  public static void main(final String[] args){
    final ConfigParser parser = new ConfigParser();
    final Configuration config;
    try {
      config = parser.parse(args);
      Configuration.registerConfig(config);
    } catch(HelpException e) {
      System.err.println(e.getMessage());
      System.err.println();
      parser.showHelp();
      return;
    } catch (ParseException e) {
      System.err.println("Error: " + e.getMessage());
      System.err.println();
      parser.showHelp();
      return;
    }

    Swagger swagger;

    try {
      final SwaggerParser swaggerParser = new SwaggerParser();
      swagger = swaggerParser.read(config.getSwagger());
      if(swagger == null) {
        throw new Exception("Failed to parse swagger for undetermined reason.");
      }
    }catch(Exception e){
      System.err.println("Failed to parse swagger at '" + config.getSwagger() + "'.");
      System.err.println(e.getMessage());
      return;
    }

    final SamplemssGenerator runner = new SamplemssGenerator();
    runner.processSwagger(swagger, config);
  }
}
