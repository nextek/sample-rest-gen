package com.sample.codegen;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import com.sample.codegen.lang.java.SimpleType;
import com.sample.codegen.lang.java.Type;

import io.swagger.models.HttpMethod;
import io.swagger.models.Swagger;

public class Util {

  public static String lcFirst(final String input) {
    if(input == null) {
      return null;
    } else if(input.length() > 0) {
      return input.substring(0, 1).toLowerCase() + input.substring(1).toLowerCase();
    } else {
      return input;
    }
  }

  public static String lcFirstOnly(final String input) {
    if(input == null){
      return null;
    } else if(input.length() > 0) {
      return input.substring(0, 1).toLowerCase() + input.substring(1);
    } else {
      return input;
    }
  }

  public static String ucFirst(final String input) {
    if(input == null) {
      return null;
    } else if(input.length() > 0) {
      return input.substring(0, 1).toUpperCase() + input.substring(1).toLowerCase();
    } else {
      return input;
    }
  }

  public static String ucFirstOnly(final String input) {
    if(input == null) {
      return null;
    } else if(input.length() > 0) {
      return input.substring(0, 1).toUpperCase() + input.substring(1);
    } else {
      return input;
    }
  }

  public static String constantizeVar(final String varName) {
    String regex = "([a-z])([A-Z]+)";
    String replacement = "$1_$2";
    String retStr = varName.replaceAll(regex, replacement).toUpperCase();
    return retStr;
  }
  
  public static String pluralizeNoun(final String noun) {
    if(noun.matches(".*[^s]s$")) {
      // already pluralized
      return noun;
    }

    String returnStr;
    if(noun.matches(".*(ch|x|s)$")) {
      returnStr = noun + "es";
    } else {
      returnStr = noun + "s";
    }
    return returnStr;
  }

  public static String pkg2subDir(final String pkg, final String rootPkg) {
    if(! pkg.contains(rootPkg)) {
      throw new RuntimeException("couldn't find rootPkg " + rootPkg + " in pkg " + pkg);
    }

    final String pkgSub  = pkg.replaceFirst("^.*" + rootPkg, "");
    final String dirSub  = pkgSub.replace('.', '/');
    return dirSub;
  }

  public static void writeGeneratedFile(final String path, final String txt) {
    final String dir = new File(path).getParent();
    new File(dir).mkdirs();

    System.out.println("Writing file : " + path);
    writeFile(path, txt);
  }

  public static void writeFile(final String path, final String txt) {
    PrintWriter out;
    try {
      out = new PrintWriter(path);
    } catch(FileNotFoundException e) {
      throw new RuntimeException(e);
    }
    out.println(txt);
    out.close();
  }

  public static void makeDirs(final String path){
    new File(path).mkdirs();
  }

  public static Type getJavaType(final String swaggerType, final String swaggerFormat) {
    Type javaType = null;
    String format = (swaggerFormat == null) ? "" : swaggerFormat;

    switch(swaggerType) {
    case "string":
      javaType = SimpleType.getStringType();
      break;
    case "boolean":
      javaType = SimpleType.getBooleanBoxType();
      break;
    default:
      javaType = null;
      break;
    }
    
    if(javaType != null) {
      return javaType;
    }

    if(swaggerType.equals("integer") && swaggerFormat == null) {
      // if they forget to specify a format, default to int32
      format = "int32";
    }

    switch (format) {
    case "int32":
      javaType = SimpleType.getIntegerBoxType();
      break;
    case "int64":
      javaType = SimpleType.getLongBoxType();
      break;
    case "float":
      javaType = SimpleType.getFloatBoxType();
      break;
    case "double":
      javaType = SimpleType.getDoubleBoxType();
      break;
    case "string":
      javaType = SimpleType.getStringType();
      break;
    case "byte":
      javaType = SimpleType.getByteBoxType();
      break;
    case "binary":
      javaType = SimpleType.getStringType();
      break;
    case "boolean":
      javaType = SimpleType.getBooleanBoxType();
      break;
    case "date":
      javaType = SimpleType.getStringType();
      break;
    case "date-time":
      javaType = SimpleType.getStringType();
      break;
    case "time":
      javaType = SimpleType.getStringType();
      break;
    case "password":
      javaType = SimpleType.getStringType();
      break;
    default:
      javaType = null;
      break;
    }

    return javaType;
  }

  public static String pkg2resourceSubdir(final String pkg, final String node) {
    final String[] parts        = pkg.split("\\.");
    final List<String> retParts = new ArrayList<String>();

    // System.err.println("pkg2res - pkg: " + pkg);
    // System.err.println("pkg2res - node: " + node);

    boolean on = false;
    String past = "";
    for(String part : parts) {
      // System.err.println("part (" + part + ")");
      if(on) {
        // System.err.println("   on is set, adding " + part);
        retParts.add(part);
      }

      past += part;
      if(past.equals(node)) {
        on = true;
      }
      past += ".";
    }

    final String subdir = String.join("/", retParts);
    // System.err.println("ret: " + subdir);
    return subdir;
  }

  public static int getLongestLineLen(final String txt) {
    int max = 0;
    final String[] lines = txt.split(nl());
    for(String line : lines) {
      if(line.length() > max) {
        max = line.length();
      }
    }
    return max;
  }

  public static boolean isHttpMethodReadOnly(final HttpMethod method) {
    switch(method){
    case GET:
    case OPTIONS:
    case HEAD:
      return true;
    case PATCH:
    case PUT:
    case POST:
    case DELETE:
      return false;
    default:
      throw new IllegalArgumentException("Invalid Http method '" + method + "'");
    }
  }

  public enum JoinTokenStyle {
    PREPEND, APPEND;
  }

  public static String getRepeatedStr(final int indentSize, final String indentChar) {
    final String indentStr = new String(new char[indentSize]).replace("\0", indentChar);
    return indentStr;
  }

  public static List<String> indentLines(final int indentSize, final List<String> lines) {
    final String indent = Util.getRepeatedStr(indentSize, " ");
    final List<String> indentedLines = new ArrayList<String>();
    for(String line  : lines) {
      if(line.length() > 0) {
        indentedLines.add(indent + line);
      } else {
        indentedLines.add(line);
      }
    }
    return indentedLines;
  }
  
  public static String indentLines(final int indentSize, final String txt) {
    if(txt == null) {
      return null;
    }
    final String indent = Util.getRepeatedStr(indentSize, " ");
    final List<String> indentedLines = new ArrayList<String>();
    for(String line  : txt.trim().split(nl())) {
      if(line.length() > 0) {
        indentedLines.add(indent + line);
      } else {
        indentedLines.add(line);
      }
    }
    final String indentedTxt = String.join(nl(), indentedLines);
    return indentedTxt;
  }

  public static List<String> getConditionalGroupedLines(int indentSize, int maxLineLen,
                                                        String oper, List<String> tokens,
                                                        String finalVar) {
    CondGroupResult result = getConditionalGroupedLines(indentSize, maxLineLen,
                                                        oper, tokens, 1, finalVar);
    return result.retLines;
  }
  
  static public class CondGroupResult {
    public List<String> retLines;
    public int numVarsUsed;
  }
  
  public static CondGroupResult getConditionalGroupedLines(int indentSize, int maxLineLen,
                                                           String oper, List<String> tokens,
                                                           int finalVarInst, String wantFinalVar) {
    int MAX_GROUP_SIZE = 3;
    int NUM_GROUPS     = 3;
    String varPrefix = "v";
    String indentStr = getRepeatedStr(indentSize, " ");

    if(tokens.size() <= MAX_GROUP_SIZE) {
      List<String> varsUsed = new ArrayList<>();
      List<String> useTokens = new ArrayList<>();;
      for(int i = 0; i < tokens.size(); i++) {
        boolean isLast = (i == (tokens.size() - 1));
        String curToken = tokens.get(i);
        if(isLast) {
          curToken = curToken + ";";
        }
        useTokens.add(curToken);
      }
      String var = varPrefix + finalVarInst;
      if(wantFinalVar != null) {
        var = wantFinalVar;
      }
      String declLine = indentStr + "final boolean " + var;
      String succLinePrefix  = getRepeatedStr(declLine.length(), " ");
      String firstLinePrefix = declLine + " = ";
      if(tokens.size() > 1) {
        firstLinePrefix += " ";
      }
      CondGroupResult result = new CondGroupResult();
      result.retLines = wrapJoinTokensLn(firstLinePrefix, maxLineLen, succLinePrefix, oper,
                                         "", JoinTokenStyle.PREPEND, useTokens, false,
                                         true, false);
      result.numVarsUsed = 1;
      return result;
    }

    List<List<String>> groups = new ArrayList<>();
    for(int i = 0; i < NUM_GROUPS; i++) {
      groups.add(new ArrayList<>());
    }

    List<String> retLines = new ArrayList<>();
    int itemsPerGroup = (tokens.size() + 2) / NUM_GROUPS;
    for(int i = 0; i < tokens.size(); i++) {
      int groupNumToUse = (i / 3);
      if(groupNumToUse > 2) {
        groupNumToUse = 2;
      }
      List<String> group = groups.get(groupNumToUse);
      group.add(tokens.get(i));
    }

    List<String> varsUsed = new ArrayList<>();
    int numVarsUsed = 0;
    int curVarInst = finalVarInst + 1;
    for(List<String> group : groups) {
      if(group.size() == 0) {
        continue;
      }
      CondGroupResult res = getConditionalGroupedLines(indentSize, maxLineLen, oper, group,
                                                       curVarInst, null);
      varsUsed.add(varPrefix + curVarInst);
      retLines.addAll(res.retLines);
      int thisCallVarsUsed = res.numVarsUsed;
      numVarsUsed += thisCallVarsUsed;
      curVarInst += thisCallVarsUsed;
    }
    String finalVar;
    if(wantFinalVar != null) {
      finalVar = wantFinalVar;
    } else {
      finalVar = varPrefix + finalVarInst;
    }
    String lastLine = indentStr + "final boolean " + finalVar + " = "
                    + String.join(oper, varsUsed) + ";";
    retLines.add(lastLine);
      
    CondGroupResult result = new CondGroupResult();
    result.retLines    = retLines;
    result.numVarsUsed = numVarsUsed;
    return result;
  }

  public static boolean doesFirstTokenWrap(final String firstLinePrefix,
                                           final int maxLineLen,
                                           final String successiveLinePrefix,
                                           final String joinToken,
                                           final String finalAppendStr,
                                           final JoinTokenStyle joinTokenStyle,
                                           final List<String> tokens,
                                           final boolean forceFinalOnNewline,
                                           final boolean trimTrailingWS,
                                           final boolean concatAppendStrOnLastToken) {
    List<String> retLines = wrapJoinTokensLn(firstLinePrefix,
                                             maxLineLen,
                                             successiveLinePrefix,
                                             joinToken,
                                             finalAppendStr,
                                             joinTokenStyle,
                                             tokens,
                                             forceFinalOnNewline,
                                             trimTrailingWS,
                                             concatAppendStrOnLastToken);
    String firstLine = retLines.get(0);
    if(firstLine.matches(".*" + Pattern.quote(firstLinePrefix) + "$")) {
      return true;
    } else {
      return false;
    }
  }

  public static List<String> wrapJoinTokensLn(final String firstLinePrefix,
                                              final int maxLineLen,
                                              final String successiveLinePrefix,
                                              final String joinToken,
                                              final String finalAppendStr,
                                              final JoinTokenStyle joinTokenStyle,
                                              final List<String> tokens,
                                              final boolean forceFinalOnNewline,
                                              final boolean trimTrailingWS,
                                              final boolean concatAppendStrOnLastToken) {
    final List<String> adjTokens = new ArrayList<String>();
    final int numTokens = tokens.size();
    String useFinalAppendStr = finalAppendStr;

    for(int i = 0; i < numTokens; i++) {
      final String token = tokens.get(i);
      final boolean isFirst = (i == 0);
      final boolean isLast  = (i == (numTokens - 1));

      String adjToken = null;

      if(joinTokenStyle == JoinTokenStyle.PREPEND) {
        if(isFirst) {
          adjToken = token;
        } else {
          adjToken = joinToken + token;
        }
      } else if(joinTokenStyle == JoinTokenStyle.APPEND) {
        if(! isLast) {
          adjToken = token + joinToken;
        } else {
          adjToken = token;
        }
      }
      if(isLast && concatAppendStrOnLastToken) {
        adjToken += finalAppendStr;
        useFinalAppendStr = null;
      }

      adjTokens.add(adjToken);
    }

    if(useFinalAppendStr != null && ! forceFinalOnNewline) {
      adjTokens.add(useFinalAppendStr);
    }

    final List<String> retLines = new ArrayList<String>();
    boolean isFirstLine = true;

    while(adjTokens.size() > 0) {
      String prefixStr;
      if(isFirstLine) {
        prefixStr = firstLinePrefix;
      } else {
        prefixStr = successiveLinePrefix;
      }

      String lastTstLine = prefixStr;
      int numTokensUsed  = 0;

      //System.err.println("adjTokens: " + String.join("|", adjTokens));

      TEST_TOKENS_LOOP:
      for(int tokenCnt = 0; tokenCnt <= adjTokens.size(); tokenCnt++) {
        String tstLine;

        if(tokenCnt == 0) {
          tstLine = prefixStr;
        } else {
          final int lastIndex = tokenCnt - 1;
          final List<String> tokenGrp = adjTokens.subList(0, lastIndex + 1);
          tstLine = prefixStr + String.join("", tokenGrp);
        }

        if(trimTrailingWS) {
          tstLine = tstLine.replaceFirst("\\s+$", "");
        }

        //System.err.format("tokenCnt: %d, tstLine: [%s]\n", tokenCnt, tstLine);

        if(tokenCnt == 1 && ! isFirstLine) {
          if(tstLine.length() > maxLineLen) {
            // if the first token fails to fit in a line, keep it no matter what
            lastTstLine   = tstLine;
            numTokensUsed = tokenCnt;
            break TEST_TOKENS_LOOP;
          }
        }
        //System.err.println("tstLine.length: " + tstLine.length() + ", max: " + maxLineLen);
        if(tstLine.length() > maxLineLen) {
          //System.err.println("  tstLine longer then maxLineLen");
          break TEST_TOKENS_LOOP;
        }

        lastTstLine   = tstLine;
        numTokensUsed = tokenCnt;
      }

      //System.err.println("using chosen tstLine: [" + lastTstLine + ")");
      //System.err.println("using numTokensUsed: [" + numTokensUsed);
      retLines.add(lastTstLine);
      for(int i = 0; i < numTokensUsed; i++) {
        adjTokens.remove(0);
      }
      isFirstLine = false;
    }

    if(useFinalAppendStr != null && forceFinalOnNewline) {
      final String finalLine = successiveLinePrefix + useFinalAppendStr;
      retLines.add(finalLine);
    }
    return retLines;
  }
    
  public static String wrapJoinTokens(final String firstLinePrefix,        final int maxLineLen,
                                      final String successiveLinePrefix,   final String joinToken,
                                      final String finalAppendStr,
                                      final JoinTokenStyle joinTokenStyle,
                                      final List<String> tokens,
                                      final boolean forceFinalOnNewline,
                                      final boolean trimTrailingWS,
                                      final boolean concatAppendStrOnLastToken) {
    List<String> lines = wrapJoinTokensLn(firstLinePrefix, maxLineLen, successiveLinePrefix,
                                          joinToken, finalAppendStr, joinTokenStyle, tokens,
                                          forceFinalOnNewline, trimTrailingWS,
                                          concatAppendStrOnLastToken);
    final String retStr = String.join(nl(), lines);
    return retStr;
  }

  public static String getLastResource(final String pathString) {
    final String[] pathParts = pathString.split("/");

    PARTS_LOOP:
    for(int i = pathParts.length - 1; i >= 0; i--) {
      final String part = pathParts[i];
      if(part.length() == 0) {
        continue PARTS_LOOP;
      }
      if(part.matches(".*[{}].*")) {
        continue PARTS_LOOP;
      }
      return part;
    }

    return null;
  }

  public static String getLastPathPart(final String pathString) {
    final String[] pathParts = pathString.split("/");

    PARTS_LOOP:
    for(int i = pathParts.length - 1; i >= 0; i--) {
      final String part = pathParts[i];
      if(part.length() == 0) {
        continue PARTS_LOOP;
      }
      if(part.matches(".*[{}].*")) {
        String tok = part.replaceAll("[{}]", "");
        return tok;
      }
      return part;
    }

    return null;
  }
  
  public static String abbreviateIdentifier(final String input){
    final String[] chunks = input.split("(?=\\p{Upper})");

    final String[] finalChunks = new String[chunks.length];

    for(int i = 0; i < chunks.length; i++){
      final String current = chunks[i];

      if(current.length() < 4){
        finalChunks[i] = current;
      } else if(!current.substring(3, 4).matches("[aeiou]")){
        if(current.length() == 5){
          finalChunks[i] = current;
        }else{
          finalChunks[i] = current.substring(0, 4);
        }
      } else if(!current.substring(2, 3).matches("[aeiou]")) {
        finalChunks[i] = current.substring(0, 3);
      } else if(!current.substring(1, 2).matches("[aeiou]")) {
        finalChunks[i] = current.substring(0, 2);
      } else if(!current.substring(0, 1).matches("[aeiou]")) {
        finalChunks[i] = current.substring(0, 1);
      } else {
        finalChunks[i] = current;
      }
    }

    return String.join("", finalChunks);
  }

  public static List<String> getPathPartsList(final String path) {
    final List<String> parts = new LinkedList<String>();
    parts.addAll(Arrays.asList(path.split("/")));

    if(parts.get(0).length() == 0) {
      parts.remove(0);  // remove the empty part
    }

    return parts;
  }

  public static String[] getPathPartGroups(final String path) {
    final List<String> parts = new LinkedList<String>();
    parts.addAll(Arrays.asList(path.split("/")));

    if(parts.get(0).length() == 0) {
      parts.remove(0);  // remove the empty part
    }
    final String basePart = parts.get(0);
    parts.remove(0);  // remove the base part

    final String basePath     = "/" + basePart;
    final String remainingPath = String.join("/", parts);
    final String[] returnVals = new String[2];
    returnVals[0] = basePath;
    returnVals[1] = remainingPath;
    return returnVals;
  }
  
  public static String getBasePath(final String pathString) {
    final String[] pathParts = pathString.split("/");
    List<String> useParts = new ArrayList<>();
    
    PARTS_LOOP:
    for(int i = 0 ; i < pathParts.length; i++) {
      boolean isLast    = (i == pathParts.length - 1);
      final String part = pathParts[i];
      if(part.length() == 0) {
        continue PARTS_LOOP;
      }
      if(! (part.matches(".*[{}].*") && isLast)) {
        useParts.add(part);
      }
    }
    String basePath = "/" + String.join("/", useParts);
    return basePath;
  }
  
  int getPathPartsCount(final String path) {
    final List<String> parts = new LinkedList<String>();
    parts.addAll(Arrays.asList(path.split("/")));

    int cnt = 0;
    for(String part : parts) {
      if(part.length() == 0) {
        continue;
      }
      cnt++;
    }
    return cnt;
  }
  
  public static String nl() {
    return System.getProperty("line.separator");
  }

  public static List<String> getCustomValidations(Map<String, Object> vendorExtensions) {
    List<String> customValidations = new ArrayList<>();

    Object val = vendorExtensions.get("x-customvalidator");
    if(val == null) {
      return customValidations;
    }

    if(val instanceof String) {
      customValidations.add((String)val);
    } else if(val instanceof List) {
      List<String> result = (List<String>)val;
      customValidations.addAll(result);
    }

    return customValidations;
  }

  public static String lookupCustomValidatorAnnotation(String customValidator) {
    String ret = null;
    switch(customValidator) {
    case "email":
      ret = "AssertValidEmail";
      break;
    default:
    }
    return ret;
  }

  public static String lookupCustomValidatorImportRef(String customValidator) {
    String ret = null;
    switch(customValidator) {
    case "email":
      ret = "com.sample.soa.constraints.AssertValidEmail";
      break;
    default:
    }
    return ret;
  }

  public static void assertValidImportRef(String importRef) {
    if(importRef.matches(".*<.*")) {
      throw new RuntimeException("Attempting to add invalid less then sign for import ref: "
                                 + importRef);
    }
    if("import".equals(importRef.substring(0, 6))) {
      throw new RuntimeException("don't pass import on importRef lines: " + importRef);
    }
    if(importRef.endsWith(";")) {
      throw new RuntimeException("don't pass semi-colon on importRef lines: " + importRef);
    }
  }

  public static void assertValidImportRefs(List<String> importRefs) {
    for(String importRef : importRefs) {
      assertValidImportRef(importRef);
    }
  }
}


