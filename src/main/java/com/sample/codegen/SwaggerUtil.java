package com.sample.codegen;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.sample.codegen.restgen.ParamCat;

import io.swagger.models.ArrayModel;
import io.swagger.models.HttpMethod;
import io.swagger.models.Model;
import io.swagger.models.ModelImpl;
import io.swagger.models.Operation;
import io.swagger.models.RefModel;
import io.swagger.models.ComposedModel;
import io.swagger.models.Path;
import io.swagger.models.Response;
import io.swagger.models.Swagger;
import io.swagger.models.parameters.BodyParameter;
import io.swagger.models.parameters.Parameter;
import io.swagger.models.properties.ArrayProperty;
import io.swagger.models.properties.BooleanProperty;
import io.swagger.models.properties.DoubleProperty;
import io.swagger.models.properties.FloatProperty;
import io.swagger.models.properties.IntegerProperty;
import io.swagger.models.properties.LongProperty;
import io.swagger.models.properties.MapProperty;
import io.swagger.models.properties.Property;
import io.swagger.models.properties.RefProperty;
import io.swagger.models.properties.StringProperty;

public class SwaggerUtil {
  public static class ResponseC {
    public static String getResponseSchema(Response response) {
      Property schema = response.getSchema();

      if(schema == null) {
        return null;
      }

      if(schema instanceof ArrayProperty) {
        final ArrayProperty arrayProp = (ArrayProperty) schema;
        schema = arrayProp.getItems();
      }

      String schemaName = null;
      if(schema instanceof RefProperty) {
        final RefProperty refProp = (RefProperty) schema;
        schemaName = refProp.getSimpleRef();
      }
      return schemaName;
    }
  }

  public static class OperationC {
    public static boolean hasParamsOfType(final Operation operation,
                                          final String type) {
      final List<Parameter> ret = getParamsOfType(operation, type);
      return (ret.size() > 0);
    }

    public static boolean hasParamsNotOfType(final Operation operation,
                                             final String type) {
      final List<Parameter> ret = getParamsNotOfType(operation, type);
      return (ret.size() > 0);
    }

    public static List<Parameter> getParamsOfType(final Operation operation,
                                                  final String type) {
      Class c;
      try {
        c = Class.forName("io.swagger.models.parameters." + type);
      } catch(ClassNotFoundException e) {
        throw new RuntimeException(e);
      }

      final List<Parameter> params = operation.getParameters();
      final List<Parameter> retList = new ArrayList<Parameter>();

      for(Parameter param : params) {
        if(c.isInstance(param)) {
          retList.add(param);
        }
      }
      return retList;
    }

    public static List<Parameter> getParamsNotOfType(final Operation operation,
                                                     final String type) {
      Class c;
      try {
        c = Class.forName("io.swagger.models.parameters." + type);
      } catch(ClassNotFoundException e) {
        throw new RuntimeException(e);
      }

      final List<Parameter> params = operation.getParameters();
      final List<Parameter> retList = new ArrayList<Parameter>();

      for(Parameter param : params) {
        if(! c.isInstance(param)) {
          retList.add(param);
        }
      }
      return retList;
    }

    public static List<Integer> getResponseCodes(final Operation operation) {
      final List<Integer> retList = new ArrayList<Integer>();

      final Map<String, Response> responsesMap = operation.getResponses();
      for(Map.Entry<String, Response> entryC : responsesMap.entrySet()) {
        final String retCode    = entryC.getKey();
        final Response response = entryC.getValue();
        if(response == null) {
          continue;
        }
        final Property schema   = response.getSchema();
        int retCodeInt = 0;
        try {
          retCodeInt = Integer.parseInt(retCode);
        } catch(NumberFormatException e) {
          retCodeInt = 0;
        }
        if(retCodeInt > 0) {
          retList.add(retCodeInt);
        }
      }
      return retList;
    }

    public static boolean hasResponseCode(final Operation operation,
                                          int code) {
      List<Integer> responseCodes = getResponseCodes(operation);
      for(Integer responseCode : responseCodes) {
        if(code == responseCode) {
          return true;
        }
      }
      return false;
    }
    
    public static Response getResponse(final Operation operation,
                                       final int wantRetCode) {
      final Map<String, Response> responsesMap = operation.getResponses();
      for(Map.Entry<String, Response> entryC : responsesMap.entrySet()) {
        final String retCode    = entryC.getKey();
        final Response response = entryC.getValue();
        if(response == null) {
          continue;
        }
        final Property schema   = response.getSchema();
        int retCodeInt = 0;
        try {
          retCodeInt = Integer.parseInt(retCode);
        } catch(NumberFormatException e) {
          retCodeInt = 0;
        }
        if(wantRetCode == retCodeInt) {
          return response;
        }
      }

      return null;
    }

    public static String getInputBodySchema(final Operation operation) {
      String schemaName = null;
      final List<Parameter> bodyParams = OperationC.getParamsOfType(operation, "BodyParameter");
      if(bodyParams.size() == 0) {
        return null;
      }

      final BodyParameter bodyParam = (BodyParameter) bodyParams.get(0);
      Model schema = bodyParam.getSchema();
      if(schema instanceof ArrayModel) {
        final ArrayModel arrayModel = (ArrayModel) schema;
        Property prop = arrayModel.getItems();
        if(prop instanceof RefProperty) {
          final RefProperty refProp = (RefProperty) prop;
          schemaName = refProp.getSimpleRef();
        }
      } else if(schema instanceof RefModel) {
        final RefModel refSchema = (RefModel) schema;
        schemaName = refSchema.getSimpleRef();
      } else if(schema instanceof ModelImpl) {
        return null;
      } else {
        throw new RuntimeException("unsupported schema type" + schema.getClass().getName());
      }
      return schemaName;
    }

    public static boolean doesHaveArrayResponse(final Operation operation) {
      final Response response = OperationC.getResponse(operation, 200);
      if(response == null) {
        return false;
      }

      final Property schema = response.getSchema();

      if(schema != null && (schema instanceof ArrayProperty)) {
        final ArrayProperty arrayProp = (ArrayProperty) schema;
        final Property itemsProp     = arrayProp.getItems();

        if(itemsProp instanceof RefProperty) {
          final RefProperty refProp = (RefProperty) itemsProp;
          final String refSchema    = refProp.getSimpleRef();
          if(refSchema != null) {
            return true;
          }
        }
      }

      return false;
    }

    public static List<String> getProduces(final Operation operation, final Swagger swagger) {
      List<String> operProduces    = operation.getProduces();
      List<String> swaggerProduces = swagger.getProduces();

      if(operProduces != null) {
        return operProduces;
      } else if(swaggerProduces != null) {
        return swaggerProduces;
      } else {
        List<String> emptyList = new ArrayList<>();
        return emptyList;
      }
    }

    public static List<String> getConsumes(final Operation operation, final Swagger swagger) {
      List<String> operConsumes    = operation.getConsumes();
      List<String> swaggerConsumes = swagger.getConsumes();

      if(operConsumes != null) {
        return operConsumes;
      } else if(swaggerConsumes != null) {
        return swaggerConsumes;
      } else {
        List<String> emptyList = new ArrayList<>();
        return emptyList;
      }
    }
  }  // end of class Operation

  public static class ModelC {
    public static List<String> getReferencedSchemas(final Model model,
                                                    final io.swagger.models.Swagger swagger) {
      final List<String> childSchemaNames = new ArrayList<String>();
      Map<String, Property> propertiesMap = model.getProperties();
      //System.err.println("getReferencedSchemas, model type: " + model.getClass().getName());
      
      if(model instanceof ComposedModel) {
        ComposedModel compModel = (ComposedModel)model;
        List<Model> allOfList = compModel.getAllOf();
        //System.err.println("propertiesMap: " + model.getProperties());

        for(Model allOfModel : allOfList) {
          String refStr = allOfModel.getReference();
          //System.err.println("  composed- ref: " + refStr);
          if(refStr != null) {
            RefModel refModel = new RefModel(refStr);
            String simpleRef = refModel.getSimpleRef();
            childSchemaNames.add(simpleRef);
            //System.err.println("    simpleRef: " + simpleRef);
          }

          //System.err.println("          - propertiesMap: " + allOfModel.getProperties());
          if(allOfModel.getProperties() != null) {
            propertiesMap = allOfModel.getProperties();
          }
        }
      }

      if(propertiesMap != null) {
        PROP_LOOP:
        for(String propName : propertiesMap.keySet()) {
          Property prop = propertiesMap.get(propName);
          //System.err.println("   propName: " + propName);
          if(prop instanceof ArrayProperty) {
            final ArrayProperty arrayProp = (ArrayProperty) prop;
            prop = arrayProp.getItems();
          }
          if(prop instanceof MapProperty) {
            final MapProperty mapProp = (MapProperty) prop;
            prop = mapProp.getAdditionalProperties();
          }

          if(prop instanceof RefProperty) {
            final RefProperty refProp = (RefProperty) prop;
            final String schemaName   = refProp.getSimpleRef();
            childSchemaNames.add(schemaName);
          } else {
            continue PROP_LOOP;
          }
        }
      }

      List<String> grandChildSchemaNames = new ArrayList<>();
      for(String childSchemaName : childSchemaNames) {
        //System.err.println(" * childSchema: " + childSchemaName);
        Model childModel = SwaggerUtil.SwaggerC.getModel(swagger, childSchemaName);
        if(childModel != null) {
          List<String> curSchemas = getReferencedSchemas(childModel, swagger);
          //for(String grandCh : curSchemas) {
          //  System.err.println("   => grandChildSchema: " + grandCh);
          //}
          grandChildSchemaNames.addAll(curSchemas);
        }
      }

      List<String> retSchemaNames = new ArrayList<>();
      retSchemaNames.addAll(childSchemaNames);
      retSchemaNames.addAll(grandChildSchemaNames);
      
      return retSchemaNames;
    }
  }  // end of class Model

  public static class PropertyC {
    public static String getDefault(final io.swagger.models.properties.Property prop) {
      String defaultVal = null;
      if(prop instanceof BooleanProperty) {
        Boolean b = ((BooleanProperty)prop).getDefault();
        if(b != null) {
          defaultVal = String.valueOf(b);
        }
      } else if(prop instanceof StringProperty) {
        StringProperty strProp = (StringProperty)prop;
        String def = strProp.getDefault();
        if(def != null) {
          if(strProp.getEnum() != null) {
            defaultVal = def;
          } else {
            // we don't put the enum type defaults in quotes because they need to get
            // converted to the enum constant anyways
            defaultVal = "\"" + def + "\"";
          }
        }
      } else if(prop instanceof IntegerProperty) {
        Integer i = ((IntegerProperty)prop).getDefault();
        if(i != null) {
          defaultVal = String.valueOf(i);
        }
      } else if(prop instanceof LongProperty) {
        Long l = ((LongProperty)prop).getDefault();
        if(l != null) {
          defaultVal = String.valueOf(l);
        }
      } else if(prop instanceof FloatProperty) {
        Float f = ((FloatProperty)prop).getDefault();
        if(f != null) {
          defaultVal = String.valueOf(f);
        }
      } else if(prop instanceof DoubleProperty) {
        Double d = ((DoubleProperty)prop).getDefault();
        if(d != null) {
          defaultVal = String.valueOf(d);
        }
      }

      return defaultVal;
    }
  }  // end of class Property

  public static class SwaggerC {
    public static Model getModel(final Swagger swagger, final String modelName) {
      final Map<String, Model> definitions = swagger.getDefinitions();
      return definitions.get(modelName);
    }

    public static Path getPath(final Swagger swagger, final String requestPathString) {
      final Map<String, Path> pathsMap = swagger.getPaths();
      String matchingGetPathString = null;

      PATH_LOOP:
      for(Map.Entry<String, Path> entry : pathsMap.entrySet()) {
        final String pathString = entry.getKey();
        final Path   path       = entry.getValue();
        if(requestPathString.equals(pathString)) {
          return path;
        }
      }

      return null;
    }

    public static Operation getOperation(final Swagger swagger, final String requestPathString,
                                         HttpMethod requestHttpMethod) {
      final Map<String, Path> pathsMap = swagger.getPaths();
      String matchingGetPathString = null;

      PATH_LOOP:
      for(Map.Entry<String, Path> entry : pathsMap.entrySet()) {
        final String pathString = entry.getKey();
        final Path   path       = entry.getValue();
        if(! requestPathString.equals(pathString)) {
          continue;
        }

        final Map<HttpMethod, io.swagger.models.Operation> operationMap = path.getOperationMap();

        OPER_LOOP:
        for(Map.Entry<HttpMethod, io.swagger.models.Operation> entryB : operationMap.entrySet()) {
          final HttpMethod method = entryB.getKey();
          final io.swagger.models.Operation operation = entryB.getValue();
          if(method == requestHttpMethod) {
            return operation;
          }
        }
      }
      return null;
    }

    /**
     * Returns a candidate for a matching GET Location header path for a post.
     *
     * @param swagger          The swagger object.
     * @param postPathString   The path string for the post.
     * @param postInputSchema  The schema of the input type on the post.
     * @param level            0 : return GET path that matches post.
     *                         1 : return GET path that has one (tmpl) token more then post.
     * @param matchingTypes    If true then the GET must return same schema type as
                               the postInputSchema.
     * @return                 A path string for the corresponding found GET.
     */
    private static String getGetPathCandidate(final Swagger swagger,
                                              final String postPathString,
                                              final String postInputSchema,
                                              final int level,
                                              final boolean matchingTypes) {
      final Map<String, Path> pathsMap = swagger.getPaths();
      String matchingGetPathString = null;

      PATH_LOOP:
      for(Map.Entry<String, Path> entry : pathsMap.entrySet()) {
        final String pathString = entry.getKey();
        final Path   path       = entry.getValue();

        //System.err.println("Comparing pathString (" + pathString + "), postPathString: "
        //                   + postPathString + ")");

        if(! pathString.matches("^\\Q" + postPathString + "\\E.*")) {
          continue PATH_LOOP;
        }

        //System.err.println("Found path match, pathString: " + pathString
        //                   + ", postPathString: " + postPathString);
        
        final String subdirPath = pathString.replace(postPathString, "");
        final String[] subdirChunksA    = subdirPath.split("/");
        final List<String> subdirChunks = new ArrayList<String>();
        for(String subdirChunk : subdirChunksA) {
          if(subdirChunk.length() > 0) {
            subdirChunks.add(subdirChunk);
          }
        }

        int subdirChunkSize = subdirChunks.size();
        if(subdirChunkSize != level) {
          //System.err.println("   subDirChunkSize is " + subdirChunkSize + ", continuing loop");
          continue PATH_LOOP;
        }
        if(subdirChunkSize == 1) {
          String lastToken = subdirChunks.get(0);
          if(! lastToken.matches("^[{].*[}]$")) {
            // if the path is one token past the postPath we want that last extra path token to
            // be a templatized token
            continue PATH_LOOP;
          }
        }

        final Map<HttpMethod, io.swagger.models.Operation> operationMap = path.getOperationMap();

        OPER_LOOP:
        for(Map.Entry<HttpMethod, io.swagger.models.Operation> entryB : operationMap.entrySet()) {
          final HttpMethod method = entryB.getKey();
          final String methodName = method.name();
          final io.swagger.models.Operation operation = entryB.getValue();
          //System.err.println("     considering method: " + methodName);

          if(method != HttpMethod.GET) {
            continue OPER_LOOP;
          }
          if(SwaggerUtil.OperationC.doesHaveArrayResponse(operation)) {
            continue OPER_LOOP;
          }
          Response resp   = OperationC.getResponse(operation, 200);
          String schemaName = ResponseC.getResponseSchema(resp);

          // this must be our matching get
          matchingGetPathString = pathString;
        }
      }

      //System.err.println("returning: " + matchingGetPathString);
      return matchingGetPathString;
    }

    public static String getCorrespondingGetPathForPost(final Swagger swagger,
                                                        final String postPathString) {
      final Map<String, Path> pathsMap = swagger.getPaths();
      String matchingGetPathString = null;

      Operation postOperation = getOperation(swagger, postPathString, HttpMethod.POST);
      String postInputSchema = OperationC.getInputBodySchema(postOperation);

      String getPathStrA = getGetPathCandidate(swagger, postPathString, postInputSchema, 1, true);
      if(getPathStrA != null) {
        //System.err.println("A: found GET path: " + getPathStrA);
        return getPathStrA;
      }

      String getPathStrB = getGetPathCandidate(swagger, postPathString, postInputSchema, 0, true);
      if(getPathStrB != null) {
        //System.err.println("B: found GET path: " + getPathStrB);
        return getPathStrB;
      }

      String getPathStrC = getGetPathCandidate(swagger, postPathString, postInputSchema, 1, false);
      if(getPathStrC != null) {
        //System.err.println("C: found GET path: " + getPathStrC);
        return getPathStrC;
      }

      String getPathStrD = getGetPathCandidate(swagger, postPathString, postInputSchema, 0, false);
      if(getPathStrD != null) {
        //System.err.println("D: found GET path: " + getPathStrD);
        return getPathStrD;
      }

      return null;
    }
  }  // end of class Swagger
}

