package com.sample.codegen.lang.java;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

import com.sample.codegen.lang.java.Param;

public class TestParam {

  String message = "Hello World";
  Param  p = new Param("var", "int", null);

  @Test
  public void testPrintMessage() {
    String paramStr = p.getParamStr(false);
    assertEquals(paramStr, "int var");
  }
}
