package com.sample.codegen.lang.java;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

import com.sample.codegen.Util;
import com.sample.codegen.lang.java.AccessModifier;
import com.sample.codegen.lang.java.Method;
import com.sample.codegen.lang.java.Param;
import com.sample.codegen.lang.java.SimpleType;

public class TestMethod {

  @Test
  public void testA() {
    Method a = new Method("runA", AccessModifier.PROTECTED, 100, 2);
    a.setReturnType(SimpleType.getStringType());
    a.addJavaDocDescLine("Test a method.");
    a.setJavaDocReturnDesc("Returns a result string.");
    a.addException("Exception", "possible exception");
    a.setBody("// todo");

    List<String> l = new ArrayList<>();
    String methDef = a.getDefinitionCode(l, false);
    String wantStr = "/**" + Util.nl() +
                     " * Test a method." + Util.nl() +
                     " *" + Util.nl() +
                     " * @return  Returns a result string." + Util.nl() +
                     " */" + Util.nl() +
                     "protected String runA() throws Exception {" + Util.nl() +
                     "  // todo"              + Util.nl() +
                     "}"                      + Util.nl();
    //assertEquals("one", Util.nl() + methDef);
    assertEquals(wantStr, methDef);

    String interfDef = a.getInterfaceDeclaration(l);
    String wantStrB = "/**" + Util.nl() +
                      " * Test a method." + Util.nl() +
                      " *" + Util.nl() +
                      " * @return  Returns a result string." + Util.nl() +
                      " */" + Util.nl() +
                      "String runA() throws Exception;";
    assertEquals(wantStrB, interfDef);
  }

  @Test
  public void testB() {
    Method a = new Method("runB", AccessModifier.PROTECTED, 100, 2);
    a.setReturnType(SimpleType.getStringType());
    a.addJavaDocDescLine("Test a method.");
    a.setJavaDocReturnDesc("Returns a result string.");
    a.addException("Exception", "possible exception");

    final Param paramA = new Param("averylongparametertotesthowfuncwrapsA", "String", null);
    a.addParam(paramA);

    final Param paramB = new Param("averylongparametertotesthowfuncwrapsB", "String", null);
    a.addParam(paramB);

    a.setBody("// todo");
    List<String> l = new ArrayList<>();
    String methDef = a.getDefinitionCode(l, false);

    String jdocBlock = "/**" + Util.nl() +
      " * Test a method." + Util.nl() +
      " *" + Util.nl() +
      " * @param averylongparametertotesthowfuncwrapsA" + Util.nl() +
      " *        This is the averylongparametertotesthowfuncwrapsA field." + Util.nl() +
      " * @param averylongparametertotesthowfuncwrapsB" + Util.nl() +
      " *        This is the averylongparametertotesthowfuncwrapsB field." + Util.nl() +
      " * @return                                       Returns a result string." + Util.nl() +
      " */";

    String wantStr = jdocBlock + Util.nl() +
      "protected String runB(String averylongparametertotesthowfuncwrapsA," + Util.nl() +
        "                      String averylongparametertotesthowfuncwrapsB) throws Exception {" + Util.nl() +
        "  // todo"                                                          + Util.nl() +
        "}"                                                                  + Util.nl();

    //assertEquals("one", Util.nl() + methDef);
    assertEquals(wantStr, methDef);

    String interfDef = a.getInterfaceDeclaration(l);
    String wantStrB = jdocBlock + Util.nl() +
        "String runB(String averylongparametertotesthowfuncwrapsA," + Util.nl() +
        "            String averylongparametertotesthowfuncwrapsB) throws Exception;";
    assertEquals(wantStrB, interfDef);
  }

  @Test
  public void testC() {
    Method a = new Method("runC", AccessModifier.PROTECTED, 75, 2);
    a.setReturnType(SimpleType.getStringType());
    a.addException("Exception", "possible exception");
    a.addJavaDocDescLine("Test a method.");
    a.setJavaDocReturnDesc("Returns a result string.");

    final Param paramA = new Param("averylongparametertotesthowfuncwrapsC", "String", null);
    a.addParam(paramA);

    final Param paramB = new Param("averylongparametertotesthowfuncwrapsD", "String", null);
    a.addParam(paramB);

    a.setBody("// todo");
    List<String> l = new ArrayList<>();
    String methDef = a.getDefinitionCode(l, false);

    String jdocBlock = "/**" + Util.nl() +
      " * Test a method." + Util.nl() +
      " *" + Util.nl() +
      " * @param averylongparametertotesthowfuncwrapsC" + Util.nl() +
      " *        This is the averylongparametertotesthowfuncwrapsC field." + Util.nl() +
      " * @param averylongparametertotesthowfuncwrapsD" + Util.nl() +
      " *        This is the averylongparametertotesthowfuncwrapsD field." + Util.nl() +
      " * @return                                       Returns a result string." + Util.nl() +
      " */";

    String wantStr = jdocBlock + Util.nl() +
        "protected String runC(String averylongparametertotesthowfuncwrapsC," + Util.nl() +
        "                      String averylongparametertotesthowfuncwrapsD)" + Util.nl() +
        "         throws Exception {"                                        + Util.nl() +
        "  // todo"                                                          + Util.nl() +
        "}"                                                                  + Util.nl();

    //assertEquals("one", Util.nl() + methDef);
    assertEquals(wantStr, methDef);

    String interfDef = a.getInterfaceDeclaration(l);
    String wantStrB = jdocBlock + Util.nl() +
        "String runC(String averylongparametertotesthowfuncwrapsC," + Util.nl() +
        "            String averylongparametertotesthowfuncwrapsD) throws Exception;";
    assertEquals(wantStrB, interfDef);
  }

  @Test
  public void testD() {
    Method a = new Method("runD", AccessModifier.PROTECTED, 60, 2);
    a.setReturnType(SimpleType.getStringType());
    a.addException("Exception", "possible exception");
    a.addJavaDocDescLine("Test a method.");
    a.setJavaDocReturnDesc("Returns a result string.");

    final Param paramA = new Param("averylongparametertotesthowfuncwrapsE", "String", null);
    a.addParam(paramA);

    final Param paramB = new Param("averylongparametertotesthowfuncwrapsF", "String", null);
    a.addParam(paramB);

    a.setBody("// todo");
    List<String> l = new ArrayList<>();
    String methDef = a.getDefinitionCode(l, false);

    String jdocBlock =
      "/**" + Util.nl() +
      " * Test a method." + Util.nl() +
      " *" + Util.nl() +
      " * @param averylongparametertotesthowfuncwrapsE" + Util.nl() +
      " *        This is the averylongparametertotesthowfuncwrapsE field." + Util.nl() +
      " * @param averylongparametertotesthowfuncwrapsF" + Util.nl() +
      " *        This is the averylongparametertotesthowfuncwrapsF field." + Util.nl() +
      " * @return                                       Returns a result string." + Util.nl() +
      " */";

    String wantStr
      = jdocBlock + Util.nl() +
        "protected String runD(" + Util.nl() +
        "        String averylongparametertotesthowfuncwrapsE," + Util.nl() +
        "        String averylongparametertotesthowfuncwrapsF)" + Util.nl() +
        "         throws Exception {"                            + Util.nl() +
        "  // todo"                                               + Util.nl() +
        "}"                                                       + Util.nl();

    //assertEquals("one", Util.nl() + methDef);
    assertEquals(wantStr, methDef);

    String interfDef = a.getInterfaceDeclaration(l);
    String wantStrB = jdocBlock + Util.nl() +
        "String runD(String averylongparametertotesthowfuncwrapsE," + Util.nl() +
        "            String averylongparametertotesthowfuncwrapsF)" + Util.nl() +
        "         throws Exception;";
    //assertEquals("one", Util.nl() + interfDef);
    assertEquals(wantStrB, interfDef);
  }

  @Test
  public void testE() {
    Method a = new Method("update", AccessModifier.PUBLIC, 98, 2);
    a.addJavaDocDescLine("Test a method.");
    a.setReturnType(SimpleType.getVoidType());

    final Param paramA = new Param("putMembershipsTravelaccountsParams",
                                   "PutMembershipsTravelaccountsParams", null);
    paramA.setIsFinal(true);
    a.addParam(paramA);

    final Param paramB = new Param("guestTravelAccountsRequest",
                                   "GuestTravelAccountsRequest", null);
    paramB.setIsFinal(true);
    a.addParam(paramB);

    a.setBody("// todo");
    List<String> l = new ArrayList<>();
    String methDef = a.getDefinitionCode(l, false);

    String jdocBlock =
      "/**" + Util.nl() +
      " * Test a method." + Util.nl() +
      " *" + Util.nl() +
      " * @param putMembershipsTravelaccountsParams" + Util.nl() +
      " *        This is the putMembershipsTravelaccountsParams field." + Util.nl() +
      " * @param guestTravelAccountsRequest          This is the guestTravelAccountsRequest field."
      + Util.nl() +
      " */";

    String wantStr = jdocBlock + Util.nl() +
      "public void update(final PutMembershipsTravelaccountsParams putMembershipsTravelaccountsParams," + Util.nl() +
      "                   final GuestTravelAccountsRequest guestTravelAccountsRequest) {" + Util.nl() +
        "  // todo"          + Util.nl() +
        "}"                  + Util.nl();

    //assertEquals("one", Util.nl() + methDef + "END");
    assertEquals(wantStr, methDef);
  }
}

