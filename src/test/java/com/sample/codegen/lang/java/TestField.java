package com.sample.codegen.lang.java;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

import com.sample.codegen.lang.java.AccessModifier;
import com.sample.codegen.lang.java.Field;
import com.sample.codegen.lang.java.SimpleType;

public class TestField {

  @Test
  public void testA() {
    Field f = new Field("var", AccessModifier.PRIVATE, 100, 2);
    f.setType(SimpleType.getStringType());
    String declStr = f.getDecl();
    assertEquals(declStr, "private String var;");
  }
}
