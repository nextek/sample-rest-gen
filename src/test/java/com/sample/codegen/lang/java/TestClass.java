package com.sample.codegen.lang.java;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

import com.sample.codegen.lang.java.AccessModifier;
import com.sample.codegen.lang.java.Field;
import com.sample.codegen.lang.java.LClass;
import com.sample.codegen.lang.java.Method;
import com.sample.codegen.lang.java.Param;
import com.sample.codegen.lang.java.SimpleType;

public class TestClass {

  @Test
  public void testA() {
    LClass a = new LClass("One");
    a.setPkgName("com.sample.test");
    LClass.GenerationOptions options = new LClass.GenerationOptions();
    String txt = a.getClassTxt(options);
    //System.err.println("txt:\n" + txt);

    assertEquals("one", "one");
  }

  @Test
  public void testB() {
    LClass a = new LClass("Two");
    a.setPkgName("com.sample.test");
    a.addImportRef("com.sample.Util.Helper");
    a.addImportRef("java.util.List");
    a.addImportRef("javax.ws.rs.core.Context");
    
    Field f = a.createField("fldA", AccessModifier.PRIVATE);
    f.setType(SimpleType.getStringType());
    f.setInitializerCode("\"hello there\"");
    a.addField(f);

    Method m = a.createMethod("methA", AccessModifier.PUBLIC);
    m.addParam(new Param("color", "String", null));
    m.addParam(new Param("longFieldNameA", "int", null));
    m.addParam(new Param("longFieldNameB", "int", null));
    m.addParam(new Param("longFieldNameC", "int", null));
    m.addParam(new Param("longFieldNameD", "int", null));
    m.addParam(new Param("longFieldNameE", "int", null));
    m.setBody("int x = 5;");
    //m.setExplicitJavaDoc("// a good method");
    m.addJavaDocDescLine("Returns the isAccessible field.");
    m.setJavaDocReturnDesc("The isAccessible field.");
    a.addMethod(m);

    LClass.GenerationOptions options = new LClass.GenerationOptions();
    String txt = a.getClassTxt(options);
    //System.err.println("txtB:\n" + txt);

    assertEquals("one", "one");
  }

  @Test
  public void testC() {
    LClass a = new LClass("Three");
    a.setPkgName("com.sample.test");

    Field f = a.createField("fldA", AccessModifier.PRIVATE);
    f.setType(SimpleType.getStringType());
    f.setInitializerCode("\"hello there\"");
    f.setIsStatic(true);
    f.setIsFinal(true);
    a.addField(f);

    Field fB = a.createField("longFieldNameB", AccessModifier.PRIVATE);
    fB.setType(SimpleType.getStringType());
    fB.setInitializerCode("\"here\"");
    fB.setIsStatic(true);
    fB.setIsFinal(true);
    a.addField(fB);

    final LClass.GenerationOptions options = new LClass.GenerationOptions();
    options.ensureBlankLineBeforeFldAnnotations = true;
    options.verticallyLineUpFieldEquals         = true;
    final String classTxt = a.getClassTxt(options);

    //System.err.println("txtC:\n" + classTxt);

    assertEquals("one", "one");
  }

}

