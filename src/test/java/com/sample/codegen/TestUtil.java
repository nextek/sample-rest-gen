package com.sample.codegen;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import com.sample.codegen.Util;

public class TestUtil {

  @Test
  public void test1a() {
    ArrayList<String> tokens = new ArrayList<String>();

    String out = Util.wrapJoinTokens("  myfunc(", 40, "xyz", " + ", ");",
                                     Util.JoinTokenStyle.PREPEND, tokens, false, false, true);
    
    assertEquals("  myfunc();", out);
  }

  @Test
  public void test1b() {
    ArrayList<String> tokens = new ArrayList<String>();

    String out = Util.wrapJoinTokens("  myfunc(", 40, "xyz", " + ", ");",
                                     Util.JoinTokenStyle.APPEND, tokens, false, false, true);
    
    assertEquals("  myfunc();", out);
  }

  @Test
  public void test2a() {
    ArrayList<String> tokens = new ArrayList<String>();
    tokens.add("abc");

    String out = Util.wrapJoinTokens("  myfunc(", 40, "xyz", " + ", ");",
                                     Util.JoinTokenStyle.PREPEND, tokens, false, false, true);
    
    assertEquals("  myfunc(abc);", out);
  }

  @Test
  public void test2b() {
    ArrayList<String> tokens = new ArrayList<String>();
    tokens.add("abc");

    String out = Util.wrapJoinTokens("  myfunc(", 40, "xyz", " + ", ");",
                                     Util.JoinTokenStyle.APPEND, tokens, false, false, true);
    
    assertEquals("  myfunc(abc);", out);
  }

  @Test
  public void test3a() {
    ArrayList<String> tokens = new ArrayList<String>();
    tokens.add("abc");
    tokens.add("def");
    
    String out = Util.wrapJoinTokens("  myfunc(", 15, "  ", "+", ");",
                                     Util.JoinTokenStyle.PREPEND, tokens, false, false, true);
    
    assertEquals("  myfunc(abc" + Util.nl() + "  +def);", out);
  }

  @Test
  public void test3b() {
    ArrayList<String> tokens = new ArrayList<String>();
    tokens.add("abc");
    tokens.add("def");
    
    String out = Util.wrapJoinTokens("  myfunc(", 15, "  ", "+", ");",
                                     Util.JoinTokenStyle.APPEND, tokens, false, false, true);
    
    assertEquals("  myfunc(abc+" + Util.nl() + "  def);", out);
  }

  @Test
  public void test4a() {
    ArrayList<String> tokens = new ArrayList<String>();
    tokens.add("abc");
    tokens.add("def");
    tokens.add("ghi");
    tokens.add("jkl");
    
    String out = Util.wrapJoinTokens("A(", 9, " ", "+", ");",
                                     Util.JoinTokenStyle.PREPEND, tokens, false, false, false);
    
    assertEquals("A(abc+def" + Util.nl() + " +ghi+jkl" + Util.nl() + " );", out);
  }

  @Test
  public void test4b() {
    ArrayList<String> tokens = new ArrayList<String>();
    tokens.add("abc");
    tokens.add("def");
    tokens.add("ghi");
    tokens.add("jkl");
    
    String out = Util.wrapJoinTokens("A(", 9, " ", "+", ");",
                                     Util.JoinTokenStyle.APPEND, tokens, false, false, true);

    assertEquals("A(abc+" + Util.nl() + " def+ghi+" + Util.nl() + " jkl);", out);
  }

  @Test
  public void test5a() {
    ArrayList<String> tokens = new ArrayList<String>();
    tokens.add("abc");
    tokens.add("def");
    tokens.add("ghi");
    tokens.add("jkl");
    
    String out = Util.wrapJoinTokens("A(", 10, " ", ", ", ");",
                                     Util.JoinTokenStyle.PREPEND, tokens, false, true, true);

    assertEquals("A(abc, def" + Util.nl() + " , ghi" + Util.nl() + " , jkl);", out);
  }

  @Test
  public void test5b() {
    ArrayList<String> tokens = new ArrayList<String>();
    tokens.add("abc");
    tokens.add("def");
    tokens.add("ghi");
    tokens.add("jkl");
    
    String out = Util.wrapJoinTokens("A(", 10, " ", ", ", ");",
                                     Util.JoinTokenStyle.APPEND, tokens, false, true, true);

    assertEquals("A(abc," + Util.nl() + " def, ghi," + Util.nl() + " jkl);", out);
  }

  @Test
  public void test6a() {
    ArrayList<String> tokens = new ArrayList<String>();
    tokens.add("abcdefghijkl");
    
    String out = Util.wrapJoinTokens("  myfunc(", 15, "  ",", ", ");",
                                     Util.JoinTokenStyle.PREPEND, tokens, false, false, false);

    assertEquals("  myfunc(" + Util.nl() + "  abcdefghijkl" + Util.nl() + "  );", out);
  }

  @Test
  public void test6b() {
    ArrayList<String> tokens = new ArrayList<String>();
    tokens.add("abcdefghijkl");
    
    String out = Util.wrapJoinTokens("  myfunc(", 15, "  ",",", ");",
                                     Util.JoinTokenStyle.APPEND, tokens, false, false, false);

    assertEquals("  myfunc(" + Util.nl() + "  abcdefghijkl" + Util.nl() + "  );", out);
  }

  //@Test
  public void testCondGroup1() {

    List<String> tokens = new ArrayList<>();
    for(int i = 1; i <= 10; i++) {
      tokens.add("conditionalVar" + i);
    }
    
    List<String> ret = Util.getConditionalGroupedLines(2, 40, " && ", tokens, "finalVar");
    String out = String.join("\n", ret);

    String wantStr = "  final boolean v2 =  conditionalVar1" + Util.nl() +
                     "                   && conditionalVar2" + Util.nl() +
                     "                   && conditionalVar3;" + Util.nl() +
                     "  final boolean v3 =  conditionalVar4" + Util.nl() +
                     "                   && conditionalVar5" + Util.nl() +
                     "                   && conditionalVar6;" + Util.nl() +
                     "  final boolean v5 =  conditionalVar7" + Util.nl() +
                     "                   && conditionalVar8" + Util.nl() +
                     "                   && conditionalVar9;" + Util.nl() +
                     "  final boolean v6 = conditionalVar10;" + Util.nl() +
                     "  final boolean v4 = v5 && v6;" + Util.nl() +
                     "  final boolean finalVar = v2 && v3 && v4;";

    assertEquals(wantStr, out);
  }

  //@Test
  public void testCondGroup2() {
    List<String> tokens = new ArrayList<>();
    for(int i = 1; i <= 1; i++) {
      tokens.add("conditionalVar" + i);
    }
    
    List<String> ret = Util.getConditionalGroupedLines(2, 50, " && ", tokens, "lastVar");
    String out = String.join("\n", ret);
    String wantStr = "  final boolean lastVar = conditionalVar1;";

    assertEquals(wantStr, out);
  }

  //@Test
  public void testCondGroup3() {
    List<String> tokens = new ArrayList<>();
    for(int i = 1; i <= 2; i++) {
      tokens.add("conditionalVar" + i);
    }
    
    List<String> ret = Util.getConditionalGroupedLines(2, 50, " && ", tokens, "lastVar");
    String out = String.join("\n", ret);
    String wantStr = "  final boolean lastVar =  conditionalVar1" + Util.nl() +
                     "                        && conditionalVar2;";
    assertEquals(wantStr, out);
  }

}
